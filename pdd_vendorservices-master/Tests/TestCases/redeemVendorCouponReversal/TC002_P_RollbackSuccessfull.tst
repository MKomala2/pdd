<?xml version="1.0" encoding="UTF-8"?>
<TestCase name="TC002_P_RollbackSuccessfull" version="5">

<meta>
   <create author="MMelagi1" buildNumber="10.3.0.297" date="08/02/2019" host="LP-5CD7521B4F" version="10.3.0"/>
   <lastEdited author="MKomala2" buildNumber="10.3.0.297" date="07/23/2020" host="LP-5CD7355fj7" version="10.3.0"/>
</meta>

<id>6CB973E1C5BF11EA89E18AB111CBFB47</id>
<IsInProject>true</IsInProject>
<sig>ZWQ9NSZ0Y3Y9NSZsaXNhdj0xMC4zLjAgKDEwLjMuMC4yOTcpJm5vZGVzPS03NDE4MjM1ODE=</sig>
<subprocess>false</subprocess>

<initState>
</initState>

<resultState>
</resultState>

<deletedProps>
</deletedProps>

      <Companion type="com.itko.lisa.test.EventSubprocessCompanion">
<Events>
    <Parameter>
    <key>Step error</key>
    <value/>
    </Parameter>
    <Parameter>
    <key>Abort</key>
    <value/>
    </Parameter>
    <Parameter>
    <key>Cycle failed</key>
    <value/>
    </Parameter>
</Events>
<EventComponents>
    <Parameter>
    <key>Event ID</key>
    <value>EVENT_ID</value>
    </Parameter>
    <Parameter>
    <key>Long Description</key>
    <value>LONG_DESCRIPTION</value>
    </Parameter>
    <Parameter>
    <key>Short Description</key>
    <value>SHORT_DESCRIPTION</value>
    </Parameter>
    <Parameter>
    <key>Step Name</key>
    <value>STEP_NAME</value>
    </Parameter>
    <Parameter>
    <key>Test Name</key>
    <value>TEST_NAME</value>
    </Parameter>
    <Parameter>
    <key>Timestamp</key>
    <value>TIMESTAMP</value>
    </Parameter>
</EventComponents>
<CompanionSubprocess>
<Subprocess>{{LISA_RELATIVE_PROJ_ROOT}}/Tests/Subprocesses/FailReportGenerator.tst</Subprocess>
<fullyParseProps>false</fullyParseProps>
<sendCommonState>false</sendCommonState>
<getCommonState>false</getCommonState>
<onAbort>abort</onAbort>
<Parameters>
    <Parameter>
    <key>Test_Execution_Status</key>
    <value>{{Test_Execution_Status}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>TestCase_Description</key>
    <value>{{TestCase_Description}}</value>
    <name>Referenced 1st in ReportGenerator</name>
    </Parameter>
    <Parameter>
    <key>STEPNAME</key>
    <value>{{STEPNAME}}</value>
    <name>Referenced 1st in ReportGenerator</name>
    </Parameter>
    <Parameter>
    <key>LISA_STEP</key>
    <value>{{LISA_LAST_STEP}}</value>
    <name>Referenced 1st in ReportGenerator</name>
    </Parameter>
    <Parameter>
    <key>FailureDescription</key>
    <value>{{FailureDescription}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>Operation</key>
    <value>{{Operation}}</value>
    <name>Referenced 1st in ReportGenerator</name>
    </Parameter>
    <Parameter>
    <key>Comments</key>
    <value>{{Comments}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>TC_Id</key>
    <value>{{TC_Id}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
</Parameters>
<SaveProps>
</SaveProps>
</CompanionSubprocess>
      </Companion>

    <Node log="" name="ReadScenarios" next="Environment_Selection" quiet="true" think="0" type="com.itko.lisa.test.NoTransNode" uid="6CB973E2C5BF11EA89E18AB111CBFB47" useFilters="true" version="1"> 


      <!-- Filters -->
      <Filter type="com.itko.lisa.test.TimeStampFilter">
        <valueToFilterKey>lisa.Do-Nothing Step.rsp</valueToFilterKey>
<pre>true</pre>
<post>false</post>
<datePattern>yyyy-MM-dd'T'HH:mm:ssXXX</datePattern>
<preprop>timestamp</preprop>
<postprop/>
<Offset>0</Offset>
      </Filter>


      <!-- Data Sets -->
<readrec>Read Scenarios</readrec>

      <!-- Assertions -->
<CheckResult assertTrue="false" name="CustomListener" type="com.itko.lisa.test.AssertByScript">
<log>Assertion name: CustomListener checks for: false  is of type: Assert by Script Execution.</log>
<then>continue</then>
<valueToAssertKey/>
        <script>import com.itko.lisa.test.*;&#xd;
import java.text.*;&#xd;
&#xd;
testExec.setStateValue("AssertMessage",""); &#xd;
TestEventListener custom_listener = new com.itko.lisa.test.TestEventListener()&#xd;
{&#xd;
public void testEvent(com.itko.lisa.test.TestEvent testevent)&#xd;
    {&#xd;
if(testevent.getEventId() == com.itko.lisa.test.TestEvent.EVENT_TRANSFAILED)&#xd;
        { &#xd;
testExec.setStateValue("AssertMessage",testevent.getLongDesc()); &#xd;
}&#xd;
}&#xd;
};&#xd;
&#xd;
testExec.setStateValue("Custom_Listener",custom_listener); &#xd;
testExec.getTestEventMgr().addListener(custom_listener,com.itko.lisa.test.TestEvent.noFilter); &#xd;
return true;&#xd;
</script>
        <language>BeanShell</language>
        <copyprops>TestExecPropsAndSystemProps</copyprops>
</CheckResult>

<CheckResult assertTrue="false" name="Check if test case execution required." type="com.itko.lisa.test.CheckResultPropRegEx">
<log>Assertion name: Ensure Property Matches Expression checks for: false is of type: Property Value Expression.</log>
<then>ReadScenarios</then>
<valueToAssertKey/>
        <prop>TC_Execute</prop>
        <param>YES</param>
</CheckResult>

    </Node>


    <Node log="" name="Environment_Selection" next="TimeStamp" quiet="true" think="0H" type="com.itko.lisa.utils.ExecSubProcessNode" uid="6CB973E3C5BF11EA89E18AB111CBFB47" useFilters="true" version="1"> 


      <!-- Assertions -->
<CheckResult assertTrue="false" name="Scripted Assertion" type="com.itko.lisa.test.AssertByScript">
<log>Assertion name: Scripted Assertion checks for: true is of type: Assert by Script Execution.</log>
<then>Subprocess ReportGenerator</then>
<valueToAssertKey/>
        <script>import com.itko.lisa.test.TestEvent;&#xd;
import java.text.*;&#xd;
&#xd;
if(testExec.getStateValue("AssertMessage").equals(""))&#xd;
{&#xd;
testExec.setStateValue("Test_Execution_Status", "Passed");&#xd;
testExec.setStateValue("Timing Log Location", testExec.getStateValue("Log Location"));&#xd;
testExec.setStateValue("Unique BODID", testExec.getStateValue("BODID"));&#xd;
if(testExec.getStateValue("Test_Script_Status")!= "Failed")&#xd;
{&#xd;
testExec.setStateValue("Test_Script_Status", "Passed");&#xd;
 }   &#xd;
return true;&#xd;
}&#xd;
else&#xd;
{&#xd;
testExec.setStateValue("Test_Execution_Status", "Failed");&#xd;
testExec.setStateValue("Timing Log Location", testExec.getStateValue("Log Location"));&#xd;
testExec.setStateValue("Unique BODID", testExec.getStateValue("BODID"));  &#xd;
testExec.setStateValue("Test_Script_Status", "Failed");&#xd;
testExec.raiseEvent(TestEvent.EVENT_TRANSFAILED,"Failure","Failure_Scripted_Assertion");&#xd;
return false;&#xd;
}</script>
        <language>BeanShell</language>
        <copyprops>TestExecPropsAndSystemProps</copyprops>
</CheckResult>

<Subprocess>{{LISA_RELATIVE_PROJ_ROOT}}/Tests/Subprocesses/Environment_Selection.tst</Subprocess>
<fullyParseProps>false</fullyParseProps>
<sendCommonState>false</sendCommonState>
<getCommonState>false</getCommonState>
<onAbort>continue</onAbort>
<Parameters>
    <Parameter>
    <key>EnvironmentTested</key>
    <value>{{EnvironmentTested}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
</Parameters>
<SaveProps>
<save>BW_Host</save>
<save>CE_Host</save>
</SaveProps>
    </Node>


    <Node log="" name="TimeStamp" next="Load Request" quiet="true" think="0H" type="com.itko.lisa.utils.ExecSubProcessNode" uid="6CB973E4C5BF11EA89E18AB111CBFB47" useFilters="true" version="1"> 

<Subprocess>{{LISA_RELATIVE_PROJ_ROOT}}/Tests/Subprocesses/TimeStamp.tst</Subprocess>
<fullyParseProps>false</fullyParseProps>
<sendCommonState>false</sendCommonState>
<getCommonState>false</getCommonState>
<onAbort>continue</onAbort>
<Parameters>
</Parameters>
<SaveProps>
<save>DATE</save>
<save>TIMESTAMP</save>
</SaveProps>
    </Node>


    <Node log="" name="Load Request" next="Parse Request" quiet="true" think="500-1S" type="com.itko.lisa.test.FileNode" uid="6CB973E5C5BF11EA89E18AB111CBFB47" useFilters="true" version="1"> 


      <!-- Assertions -->
<CheckResult assertTrue="false" name="Scripted Assertion" type="com.itko.lisa.test.AssertByScript">
<log>Assertion name: Scripted Assertion checks for: true is of type: Assert by Script Execution.</log>
<then>Subprocess ReportGenerator</then>
<valueToAssertKey/>
        <script>import com.itko.lisa.test.TestEvent;&#xd;
import java.text.*;&#xd;
&#xd;
if(testExec.getStateValue("AssertMessage").equals(""))&#xd;
{&#xd;
testExec.setStateValue("Test_Execution_Status", "Passed");&#xd;
testExec.setStateValue("Timing Log Location", testExec.getStateValue("Log Location"));&#xd;
testExec.setStateValue("Unique BODID", testExec.getStateValue("BODID"));&#xd;
if(testExec.getStateValue("Test_Script_Status")!= "Failed")&#xd;
{&#xd;
testExec.setStateValue("Test_Script_Status", "Passed");&#xd;
 }   &#xd;
return true;&#xd;
}&#xd;
else&#xd;
{&#xd;
testExec.setStateValue("Test_Execution_Status", "Failed");&#xd;
testExec.setStateValue("Timing Log Location", testExec.getStateValue("Log Location"));&#xd;
testExec.setStateValue("Unique BODID", testExec.getStateValue("BODID"));  &#xd;
testExec.setStateValue("Test_Script_Status", "Failed");&#xd;
testExec.raiseEvent(TestEvent.EVENT_TRANSFAILED,"Failure","Failure_Scripted_Assertion");&#xd;
return false;&#xd;
}</script>
        <language>BeanShell</language>
        <copyprops>TestExecPropsAndSystemProps</copyprops>
</CheckResult>

<Loc>{{LISA_RELATIVE_PROJ_ROOT}}/Data/Requests/{{RequestFile}}</Loc>
<charset>DEFAULT</charset>
<PropKey>Request</PropKey>
<onFail>continue</onFail>
    </Node>


    <Node log="" name="Parse Request" next="Send BW Request" quiet="true" think="500-1S" type="com.itko.lisa.utils.ParseTextContentNode" uid="6CB973E6C5BF11EA89E18AB111CBFB47" useFilters="true" version="1"> 


      <!-- Assertions -->
<CheckResult assertTrue="true" name="Check CE Execution Mode  flag" type="com.itko.lisa.test.CheckResultPropRegEx">
<log>Assertion name: Check CE flag checks for: true is of type: Property Value Expression.</log>
<then>Send CE Request</then>
<valueToAssertKey/>
        <prop>ExecutionMode</prop>
        <param>CE</param>
</CheckResult>

<CheckResult assertTrue="false" name="Scripted Assertion" type="com.itko.lisa.test.AssertByScript">
<log>Assertion name: Scripted Assertion checks for: true is of type: Assert by Script Execution.</log>
<then>Subprocess ReportGenerator</then>
<valueToAssertKey/>
        <script>import com.itko.lisa.test.TestEvent;&#xd;
import java.text.*;&#xd;
&#xd;
if(testExec.getStateValue("AssertMessage").equals(""))&#xd;
{&#xd;
testExec.setStateValue("Test_Execution_Status", "Passed");&#xd;
testExec.setStateValue("Timing Log Location", testExec.getStateValue("Log Location"));&#xd;
testExec.setStateValue("Unique BODID", testExec.getStateValue("BODID"));&#xd;
if(testExec.getStateValue("Test_Script_Status")!= "Failed")&#xd;
{&#xd;
testExec.setStateValue("Test_Script_Status", "Passed");&#xd;
 }   &#xd;
return true;&#xd;
}&#xd;
else&#xd;
{&#xd;
testExec.setStateValue("Test_Execution_Status", "Failed");&#xd;
testExec.setStateValue("Timing Log Location", testExec.getStateValue("Log Location"));&#xd;
testExec.setStateValue("Unique BODID", testExec.getStateValue("BODID"));  &#xd;
testExec.setStateValue("Test_Script_Status", "Failed");&#xd;
testExec.raiseEvent(TestEvent.EVENT_TRANSFAILED,"Failure","Failure_Scripted_Assertion");&#xd;
return false;&#xd;
}</script>
        <language>BeanShell</language>
        <copyprops>TestExecPropsAndSystemProps</copyprops>
</CheckResult>

<text>e3tSZXF1ZXN0fX0=</text>
<propKey>Request</propKey>
    </Node>


    <Node log="" name="Send BW Request" next="Send CE Request" quiet="false" think="500-1S" type="com.itko.lisa.ws.nx.NxWSStep" uid="6CB973E7C5BF11EA89E18AB111CBFB47" useFilters="true" version="1"> 


      <!-- Filters -->
      <Filter type="com.itko.lisa.test.FilterSaveResponse">
        <valueToFilterKey>lisa.Send BW Request.rsp</valueToFilterKey>
      <prop>BW_Response</prop>
      </Filter>

      <Filter type="com.itko.lisa.test.FilterSavePropToFile">
        <valueToFilterKey>lisa.Send BW Request.rsp</valueToFilterKey>
      <file>{{LOCDIR}}/Data/OutputFiles/{{TC_Id}}_{{TestCase_Description}}_BW.txt</file>
      <append>false</append>
      </Filter>

      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.Send BW Request.rsp</valueToFilterKey>
<prop>BW_ConfirmationCode</prop>
<xpathq>/*[local-name()='Envelope']/*[local-name()='Body']/*[local-name()='redeemVendorCouponResponse']/*[local-name()='confirmationInfo']/*[local-name()='code']/text()</xpathq>
      </Filter>

      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.Send BW Request.rsp</valueToFilterKey>
<prop>BW_CorrelationId</prop>
<xpathq>/*[local-name()='Envelope']/*[local-name()='Body']/*[local-name()='Fault']/*[local-name()='detail']/*[local-name()='errorInfo']/*[local-name()='correlationId']/text()</xpathq>
      </Filter>

      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.Send BW Request.rsp</valueToFilterKey>
<prop>BW_errorCode</prop>
<xpathq>/*[local-name()='Envelope']/*[local-name()='Body']/*[local-name()='Fault']/*[local-name()='detail']/*[local-name()='errorInfo']/*[local-name()='errorCode']/text()</xpathq>
      </Filter>

      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.Send BW Request.rsp</valueToFilterKey>
<prop>BW_errorMessage</prop>
<xpathq>/*[local-name()='Envelope']/*[local-name()='Body']/*[local-name()='Fault']/*[local-name()='detail']/*[local-name()='errorInfo']/*[local-name()='errorMessage']/text()</xpathq>
      </Filter>

      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.Send BW Request.rsp</valueToFilterKey>
<prop>BW_faultcode</prop>
<xpathq>/*[local-name()='Envelope']/*[local-name()='Body']/*[local-name()='Fault']/*[local-name()='faultcode']/text()</xpathq>
      </Filter>

      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.Send BW Request.rsp</valueToFilterKey>
<prop>BW_faultstring</prop>
<xpathq>/*[local-name()='Envelope']/*[local-name()='Body']/*[local-name()='Fault']/*[local-name()='faultstring']/text()</xpathq>
      </Filter>


      <!-- Assertions -->
<CheckResult assertTrue="false" name="Check ConfirmationCode" type="com.itko.lisa.test.AssertByScript">
<log>Assertion name: Check ConfirmationCode checks for: false is of type: Assert by Script Execution.</log>
<then>continue</then>
<valueToAssertKey/>
        <script>String confirmationCode = testExec.getStateValue("BW_ConfirmationCode");&#xd;
if(!confirmationCode.equals("")){&#xd;
 testExec.setStateValue("BW_Code","100");&#xd;
}&#xd;
else{&#xd;
testExec.setStateValue("BW_Code","501");&#xd;
}&#xd;
&#xd;
return true;</script>
        <language>BeanShell</language>
        <copyprops>TestExecPropsAndSystemProps</copyprops>
</CheckResult>

<CheckResult assertTrue="true" name="Check BW Execution Mode  flag" type="com.itko.lisa.test.CheckResultPropRegEx">
<log>Assertion name: Check CE flag checks for: true is of type: Property Value Expression.</log>
<then>Validate Params</then>
<valueToAssertKey/>
        <prop>ExecutionMode</prop>
        <param>BW</param>
</CheckResult>

<CheckResult assertTrue="false" name="Scripted Assertion" type="com.itko.lisa.test.AssertByScript">
<log>Assertion name: Scripted Assertion checks for: true is of type: Assert by Script Execution.</log>
<then>Subprocess ReportGenerator</then>
<valueToAssertKey/>
        <script>import com.itko.lisa.test.TestEvent;&#xd;
import java.text.*;&#xd;
&#xd;
if(testExec.getStateValue("AssertMessage").equals(""))&#xd;
{&#xd;
testExec.setStateValue("Test_Execution_Status", "Passed");&#xd;
testExec.setStateValue("Timing Log Location", testExec.getStateValue("Log Location"));&#xd;
testExec.setStateValue("Unique BODID", testExec.getStateValue("BODID"));&#xd;
if(testExec.getStateValue("Test_Script_Status")!= "Failed")&#xd;
{&#xd;
testExec.setStateValue("Test_Script_Status", "Passed");&#xd;
 }   &#xd;
return true;&#xd;
}&#xd;
else&#xd;
{&#xd;
testExec.setStateValue("Test_Execution_Status", "Failed");&#xd;
testExec.setStateValue("Timing Log Location", testExec.getStateValue("Log Location"));&#xd;
testExec.setStateValue("Unique BODID", testExec.getStateValue("BODID"));  &#xd;
testExec.setStateValue("Test_Script_Status", "Failed");&#xd;
testExec.raiseEvent(TestEvent.EVENT_TRANSFAILED,"Failure","Failure_Scripted_Assertion");&#xd;
return false;&#xd;
}</script>
        <language>BeanShell</language>
        <copyprops>TestExecPropsAndSystemProps</copyprops>
</CheckResult>

<wsdl>{{LISA_RELATIVE_PROJ_URL}}/Data/WSDL/rpxVendorServices.wsdl</wsdl>
<endpoint>https://{{BW_Host}}/RevenueManagement/VendorServices/V1</endpoint>
<targetNamespace>http://www.tmobile.com/schema/rpx/vendorServices</targetNamespace>
<service>vendorServices</service>
<port>vendorServicesPort</port>
<operation>redeemVendorCouponReversal</operation>
<onError>continue (quiet)</onError>
<maintainSession>true</maintainSession>
<clearSession>false</clearSession>
<request itko_enc="base64">e3tSZXF1ZXN0fX0=</request>
<style>document</style>
<use>literal</use>
<soapAction>urn:redeemVendorCouponReversal</soapAction>
<sslInfo>
<ssl-keystore-password-enc>lfcb4fd72ffe6dc8432df226ca35f0d832241c85698a3a5ab0d6a57326dfecb83</ssl-keystore-password-enc>
<ssl-key-password-enc>ld3c1ad016f3e09743827c5b9f050890c1bb015b745c502592376674039490c8c</ssl-key-password-enc>
</sslInfo>
<wsiInfo>
<wsi-check-wsdl>false</wsi-check-wsdl>
<wsi-check-msg>false</wsi-check-msg>
<wsi-display-type>onlyFailed</wsi-display-type>
<wsi-on-fail>continue</wsi-on-fail>
</wsiInfo>
<transport>
<soapVersionUri>1.1</soapVersionUri>
<httpVersionUri>1.1</httpVersionUri>
<webMethodUri>POST</webMethodUri>
<mtom>false</mtom>
<dontSendRequest>false</dontSendRequest>
<fault-as-error>true</fault-as-error>
<callTimeout>30000</callTimeout>
</transport>
<uddiActive>false</uddiActive>
<uddi-result>
<uddi-selection>
</uddi-selection>
</uddi-result>
<wss4jInfo>
<version>5</version>
<wssecurity>
<isSend>true</isSend>
<must>false</must>
</wssecurity>
<wsi-bsp>false</wsi-bsp>
</wss4jInfo>
<wsaInfo>
<useAddressing>false</useAddressing>
<must>false</must>
<to/>
<toOverride>false</toOverride>
<toDefault>true</toDefault>
<from/>
<fromOverride>false</fromOverride>
<fromDefault>true</fromDefault>
<action/>
<actionOverride>false</actionOverride>
<actionDefault>true</actionDefault>
<msgid/>
<msgidOverride>false</msgidOverride>
<msgidDefault>true</msgidDefault>
<replyTo/>
<replyToOverride>false</replyToOverride>
<faultTo/>
<faulttToOverride>false</faulttToOverride>
<addressingVersion>http://www.w3.org/2005/08/addressing</addressingVersion>
</wsaInfo>
<customHTTPHeaderInfo>
</customHTTPHeaderInfo>
<attachments>
<attachment-type>MIME</attachment-type>
</attachments>
<mtom-paths>
</mtom-paths>
    </Node>


    <Node log="" name="Send CE Request" next="Compare_BW_CE" quiet="false" think="500-1S" type="com.itko.lisa.ws.nx.NxWSStep" uid="6CB973E8C5BF11EA89E18AB111CBFB47" useFilters="true" version="1"> 


      <!-- Filters -->
      <Filter type="com.itko.lisa.test.FilterSavePropToFile">
        <valueToFilterKey>lisa.Send CE Request.rsp</valueToFilterKey>
      <file>{{LOCDIR}}/Data/OutputFiles/{{TC_Id}}_{{TestCase_Description}}_CE.txt</file>
      <append>false</append>
      </Filter>

      <Filter type="com.itko.lisa.test.FilterSaveResponse">
        <valueToFilterKey>lisa.Send CE Request.rsp</valueToFilterKey>
      <prop>CE_Response</prop>
      </Filter>

      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.Send CE Request.rsp</valueToFilterKey>
<prop>CE_CorrelationId</prop>
<xpathq>/*[local-name()='Envelope']/*[local-name()='Body']/*[local-name()='rechargeAccountResponse']/*[local-name()='correlationId']/text()</xpathq>
      </Filter>

      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.Send CE Request.rsp</valueToFilterKey>
<prop>CE_ConfirmationCode</prop>
<xpathq>/*[local-name()='Envelope']/*[local-name()='Body']/*[local-name()='rechargeAccountResponse']/*[local-name()='confirmationInfo']/*[local-name()='code']/text()</xpathq>
      </Filter>

      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.Send BW Request.rsp</valueToFilterKey>
<prop>BW_errorCode</prop>
<xpathq>/*[local-name()='Envelope']/*[local-name()='Body']/*[local-name()='Fault']/*[local-name()='detail']/*[local-name()='errorInfo']/*[local-name()='errorCode']/text()</xpathq>
      </Filter>

      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.Send BW Request.rsp</valueToFilterKey>
<prop>BW_errorMessage</prop>
<xpathq>/*[local-name()='Envelope']/*[local-name()='Body']/*[local-name()='Fault']/*[local-name()='detail']/*[local-name()='errorInfo']/*[local-name()='errorMessage']/text()</xpathq>
      </Filter>

      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.Send BW Request.rsp</valueToFilterKey>
<prop>BW_faultcode</prop>
<xpathq>/*[local-name()='Envelope']/*[local-name()='Body']/*[local-name()='Fault']/*[local-name()='faultcode']/text()</xpathq>
      </Filter>

      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.Send BW Request.rsp</valueToFilterKey>
<prop>BW_faultstring</prop>
<xpathq>/*[local-name()='Envelope']/*[local-name()='Body']/*[local-name()='Fault']/*[local-name()='faultstring']/text()</xpathq>
      </Filter>


      <!-- Assertions -->
<CheckResult assertTrue="false" name="Check ConfirmationCode" type="com.itko.lisa.test.AssertByScript">
<log>Assertion name: Check ConfirmationCode checks for: true is of type: Assert by Script Execution.</log>
<then>continue</then>
<valueToAssertKey/>
        <script>// This script should return a boolean result indicating the assertion is true or false&#xd;
&#xd;
String confirmationCode = testExec.getStateValue("CE_ConfirmationCode");&#xd;
if(!confirmationCode.equals("")){&#xd;
 testExec.setStateValue("CE_Code","100");&#xd;
}&#xd;
else{&#xd;
testExec.setStateValue("CE_Code","501");&#xd;
}&#xd;
&#xd;
return true;</script>
        <language>BeanShell</language>
        <copyprops>TestExecPropsAndSystemProps</copyprops>
</CheckResult>

<CheckResult assertTrue="true" name="Check CE Execution Mode  flag" type="com.itko.lisa.test.CheckResultPropRegEx">
<log>Assertion name: Check CE flag checks for: true is of type: Property Value Expression.</log>
<then>Validate Params</then>
<valueToAssertKey/>
        <prop>ExecutionMode</prop>
        <param>CE</param>
</CheckResult>

<CheckResult assertTrue="false" name="Scripted Assertion" type="com.itko.lisa.test.AssertByScript">
<log>Assertion name: Scripted Assertion checks for: true is of type: Assert by Script Execution.</log>
<then>Subprocess ReportGenerator</then>
<valueToAssertKey/>
        <script>import com.itko.lisa.test.TestEvent;&#xd;
import java.text.*;&#xd;
&#xd;
if(testExec.getStateValue("AssertMessage").equals(""))&#xd;
{&#xd;
testExec.setStateValue("Test_Execution_Status", "Passed");&#xd;
testExec.setStateValue("Timing Log Location", testExec.getStateValue("Log Location"));&#xd;
testExec.setStateValue("Unique BODID", testExec.getStateValue("BODID"));&#xd;
if(testExec.getStateValue("Test_Script_Status")!= "Failed")&#xd;
{&#xd;
testExec.setStateValue("Test_Script_Status", "Passed");&#xd;
 }   &#xd;
return true;&#xd;
}&#xd;
else&#xd;
{&#xd;
testExec.setStateValue("Test_Execution_Status", "Failed");&#xd;
testExec.setStateValue("Timing Log Location", testExec.getStateValue("Log Location"));&#xd;
testExec.setStateValue("Unique BODID", testExec.getStateValue("BODID"));  &#xd;
testExec.setStateValue("Test_Script_Status", "Failed");&#xd;
testExec.raiseEvent(TestEvent.EVENT_TRANSFAILED,"Failure","Failure_Scripted_Assertion");&#xd;
return false;&#xd;
}</script>
        <language>BeanShell</language>
        <copyprops>TestExecPropsAndSystemProps</copyprops>
</CheckResult>

<wsdl>{{LISA_RELATIVE_PROJ_URL}}/Data/WSDL/rpxVendorServices.wsdl</wsdl>
<endpoint>https://{{CE_Host}}/RevenueManagement/VendorServices/V1</endpoint>
<targetNamespace>http://www.tmobile.com/schema/rpx/vendorServices</targetNamespace>
<service>vendorServices</service>
<port>vendorServicesPort</port>
<operation>redeemVendorCouponReversal</operation>
<onError>continue (quiet)</onError>
<maintainSession>true</maintainSession>
<clearSession>false</clearSession>
<request itko_enc="base64">e3tSZXF1ZXN0fX0=</request>
<style>document</style>
<use>literal</use>
<soapAction>urn:redeemVendorCouponReversal</soapAction>
<sslInfo>
<ssl-keystore-password-enc>l1867a2beacdf1c7c053f9acb663856c3fb0010028d7931bdecf4ff86bd7323cb</ssl-keystore-password-enc>
<ssl-key-password-enc>le5aaca1ba658ee84bf4d71f5cddcca5e797896836436271054be4943efe643a8</ssl-key-password-enc>
</sslInfo>
<wsiInfo>
<wsi-check-wsdl>false</wsi-check-wsdl>
<wsi-check-msg>false</wsi-check-msg>
<wsi-display-type>onlyFailed</wsi-display-type>
<wsi-on-fail>continue</wsi-on-fail>
</wsiInfo>
<transport>
<soapVersionUri>1.1</soapVersionUri>
<httpVersionUri>1.1</httpVersionUri>
<webMethodUri>POST</webMethodUri>
<mtom>false</mtom>
<dontSendRequest>false</dontSendRequest>
<fault-as-error>true</fault-as-error>
<callTimeout>30000</callTimeout>
</transport>
<uddiActive>false</uddiActive>
<uddi-result>
<uddi-selection>
</uddi-selection>
</uddi-result>
<wss4jInfo>
<version>5</version>
<wssecurity>
<isSend>true</isSend>
<must>false</must>
</wssecurity>
<wsi-bsp>false</wsi-bsp>
</wss4jInfo>
<wsaInfo>
<useAddressing>false</useAddressing>
<must>false</must>
<to/>
<toOverride>false</toOverride>
<toDefault>true</toDefault>
<from/>
<fromOverride>false</fromOverride>
<fromDefault>true</fromDefault>
<action/>
<actionOverride>false</actionOverride>
<actionDefault>true</actionDefault>
<msgid/>
<msgidOverride>false</msgidOverride>
<msgidDefault>true</msgidDefault>
<replyTo/>
<replyToOverride>false</replyToOverride>
<faultTo/>
<faulttToOverride>false</faulttToOverride>
<addressingVersion>http://www.w3.org/2005/08/addressing</addressingVersion>
</wsaInfo>
<customHTTPHeaderInfo>
</customHTTPHeaderInfo>
<attachments>
<attachment-type>MIME</attachment-type>
</attachments>
<mtom-paths>
</mtom-paths>
    </Node>


    <Node log="" name="Compare_BW_CE" next="Validate Params" quiet="true" think="0H" type="com.itko.lisa.utils.ExecSubProcessNode" uid="6CB973E9C5BF11EA89E18AB111CBFB47" useFilters="true" version="1"> 


      <!-- Assertions -->
<CheckResult assertTrue="false" name="check for Test_Execution_Status" type="com.itko.lisa.test.CheckResultPropRegEx">
<log>Assertion name: check for TestStatus checks for: false is of type: Property Value Expression.</log>
<then>continue</then>
<valueToAssertKey/>
        <prop>Test_Execution_Status</prop>
        <param>Passed</param>
</CheckResult>

<CheckResult assertTrue="false" name="Scripted Assertion" type="com.itko.lisa.test.AssertByScript">
<log>Assertion name: Scripted Assertion checks for: true is of type: Assert by Script Execution.</log>
<then>Subprocess ReportGenerator</then>
<valueToAssertKey/>
        <script>import com.itko.lisa.test.TestEvent;&#xd;
import java.text.*;&#xd;
&#xd;
if(testExec.getStateValue("AssertMessage").equals(""))&#xd;
{&#xd;
testExec.setStateValue("Test_Execution_Status", "Passed");&#xd;
testExec.setStateValue("Timing Log Location", testExec.getStateValue("Log Location"));&#xd;
testExec.setStateValue("Unique BODID", testExec.getStateValue("BODID"));&#xd;
if(testExec.getStateValue("Test_Script_Status")!= "Failed")&#xd;
{&#xd;
testExec.setStateValue("Test_Script_Status", "Passed");&#xd;
 }   &#xd;
return true;&#xd;
}&#xd;
else&#xd;
{&#xd;
testExec.setStateValue("Test_Execution_Status", "Failed");&#xd;
testExec.setStateValue("Timing Log Location", testExec.getStateValue("Log Location"));&#xd;
testExec.setStateValue("Unique BODID", testExec.getStateValue("BODID"));  &#xd;
testExec.setStateValue("Test_Script_Status", "Failed");&#xd;
testExec.raiseEvent(TestEvent.EVENT_TRANSFAILED,"Failure","Failure_Scripted_Assertion");&#xd;
return false;&#xd;
}</script>
        <language>BeanShell</language>
        <copyprops>TestExecPropsAndSystemProps</copyprops>
</CheckResult>

<Subprocess>{{LISA_RELATIVE_PROJ_ROOT}}/Tests/Subprocesses/Compare_BW_CE.tst</Subprocess>
<fullyParseProps>false</fullyParseProps>
<sendCommonState>false</sendCommonState>
<getCommonState>false</getCommonState>
<onAbort>continue</onAbort>
<Parameters>
    <Parameter>
    <key>CE_Response</key>
    <value>{{CE_Response}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>BW_Response</key>
    <value>{{BW_Response}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>TC_Id</key>
    <value>{{TC_Id}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>TestCase_Description</key>
    <value>{{TestCase_Description}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>Operation</key>
    <value>{{Operation}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>CE_Code</key>
    <value>{{CE_Code}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>BW_Code</key>
    <value>{{BW_Code}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>IgnoreAttributes</key>
    <value>providerId,keyValue</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
</Parameters>
<SaveProps>
<save>FailureDescription</save>
<save>Test_Execution_Status</save>
</SaveProps>
    </Node>


    <Node log="" name="Validate Params" next="Subprocess ReportGenerator" quiet="false" think="500-1S" type="com.itko.lisa.test.UserScriptNode" uid="6CB973EAC5BF11EA89E18AB111CBFB47" useFilters="true" version="1"> 


      <!-- Assertions -->
<CheckResult assertTrue="true" name="Any Exception Then Fail" type="com.itko.lisa.dynexec.CheckInvocationEx">
<log>Assertion name: Any Exception Then Fail checks for: true is of type: Assert on Invocation Exception.</log>
<then>continue</then>
<valueToAssertKey/>
        <param>.*</param>
</CheckResult>

<CheckResult assertTrue="false" name="check for Test_Execution_Status" type="com.itko.lisa.test.CheckResultPropRegEx">
<log>Assertion name: check for TestStatus checks for: false is of type: Property Value Expression.</log>
<then>continue</then>
<valueToAssertKey/>
        <prop>Test_Execution_Status</prop>
        <param>Passed</param>
</CheckResult>

<CheckResult assertTrue="false" name="Scripted Assertion" type="com.itko.lisa.test.AssertByScript">
<log>Assertion name: Scripted Assertion checks for: true is of type: Assert by Script Execution.</log>
<then>Subprocess ReportGenerator</then>
<valueToAssertKey/>
        <script>import com.itko.lisa.test.TestEvent;&#xd;
import java.text.*;&#xd;
&#xd;
if(testExec.getStateValue("AssertMessage").equals(""))&#xd;
{&#xd;
testExec.setStateValue("Test_Execution_Status", "Passed");&#xd;
testExec.setStateValue("Timing Log Location", testExec.getStateValue("Log Location"));&#xd;
testExec.setStateValue("Unique BODID", testExec.getStateValue("BODID"));&#xd;
if(testExec.getStateValue("Test_Script_Status")!= "Failed")&#xd;
{&#xd;
testExec.setStateValue("Test_Script_Status", "Passed");&#xd;
 }   &#xd;
return true;&#xd;
}&#xd;
else&#xd;
{&#xd;
testExec.setStateValue("Test_Execution_Status", "Failed");&#xd;
testExec.setStateValue("Timing Log Location", testExec.getStateValue("Log Location"));&#xd;
testExec.setStateValue("Unique BODID", testExec.getStateValue("BODID"));  &#xd;
testExec.setStateValue("Test_Script_Status", "Failed");&#xd;
testExec.raiseEvent(TestEvent.EVENT_TRANSFAILED,"Failure","Failure_Scripted_Assertion");&#xd;
return false;&#xd;
}</script>
        <language>BeanShell</language>
        <copyprops>TestExecPropsAndSystemProps</copyprops>
</CheckResult>

<onerror>continue</onerror>
<language>BeanShell</language>
<copyProps>TestExecProps</copyProps>
<script>String errorMsg="";&#xd;
int count = 0;&#xd;
String executionMode = testExec.getStateValue("ExecutionMode");&#xd;
String expCorrelationId = testExec.getStateValue("correlationId");&#xd;
String expRechargeAmountInCents = testExec.getStateValue("rechargeAmountInCents");&#xd;
String expMSISDN = testExec.getStateValue("msisdn");&#xd;
&#xd;
String confrimationCode="";&#xd;
String actCorrelationId = "";&#xd;
String actRechargeAmountInCents = "";&#xd;
String actRechargePromotedAmtInCents = "";&#xd;
String actMSISDN = "";&#xd;
String actPrepaidSubscriberId = "";&#xd;
String subscriberType = "";&#xd;
String totalBalInCents = "";&#xd;
String balExpiryDate = "";&#xd;
String periodicChargeStatus = "";&#xd;
if(executionMode.equals("BW")){&#xd;
confrimationCode = testExec.getStateValue("BW_ConfirmationCode");&#xd;
actCorrelationId = testExec.getStateValue("BW_CorrelationId");&#xd;
actRechargeAmountInCents = testExec.getStateValue("BW_RechargeAmtInCents");&#xd;
actRechargePromotedAmtInCents = testExec.getStateValue("BW_RechargePromotedAmtInCents");&#xd;
actMSISDN = testExec.getStateValue("BW_MSISDN");&#xd;
actPrepaidSubscriberId = testExec.getStateValue("BW_PrepaidSubscriberId");&#xd;
subscriberType = testExec.getStateValue("BW_SubscriberType");&#xd;
totalBalInCents = testExec.getStateValue("BW_TotalBalInCents");&#xd;
balExpiryDate = testExec.getStateValue("BW_BalExpiryDate");&#xd;
periodicChargeStatus = testExec.getStateValue("BW_PeriodicChargeStatus");&#xd;
}&#xd;
else {&#xd;
confrimationCode = testExec.getStateValue("CE_ConfirmationCode");&#xd;
actCorrelationId = testExec.getStateValue("CE_CorrelationId");&#xd;
actRechargeAmountInCents = testExec.getStateValue("CE_RechargeAmtInCents");&#xd;
actRechargePromotedAmtInCents = testExec.getStateValue("CE_RechargePromotedAmtInCents");&#xd;
actMSISDN = testExec.getStateValue("CE_MSISDN");&#xd;
actPrepaidSubscriberId = testExec.getStateValue("CE_PrepaidSubscriberId");&#xd;
subscriberType = testExec.getStateValue("CE_SubscriberType");&#xd;
totalBalInCents = testExec.getStateValue("CE_TotalBalInCents");&#xd;
balExpiryDate = testExec.getStateValue("CE_BalExpiryDate");&#xd;
periodicChargeStatus = testExec.getStateValue("CE_PeriodicChargeStatus");&#xd;
}&#xd;
&#xd;
&#xd;
//Validation&#xd;
&#xd;
if(confrimationCode.equals("")){&#xd;
 errorMsg += "Confirmation Code is not returned.Actual Confirmation Code:"+confrimationCode+".\r\n";&#xd;
 count++;&#xd;
}&#xd;
else{&#xd;
 if(!expCorrelationId.equals(actCorrelationId)){&#xd;
  errorMsg += "CorrelationId - Expected:"+expCorrelationId+" Actual:"+actCorrelationId+".\r\n";&#xd;
  count++;&#xd;
 }&#xd;
 if(!expRechargeAmountInCents.equals(actRechargeAmountInCents)){&#xd;
  errorMsg += "RechargeAmountInCents - Expected:"+expRechargeAmountInCents+" Actual:"+actRechargeAmountInCents+".\r\n";&#xd;
  count++;&#xd;
 }&#xd;
 if(!expRechargeAmountInCents.equals(actRechargePromotedAmtInCents)){&#xd;
  errorMsg += "RechargePromotedAmountInCents - Expected:"+expRechargeAmountInCents+" Actual:"+actRechargePromotedAmtInCents+".\r\n";&#xd;
  count++;&#xd;
 }&#xd;
 if(!expMSISDN.equals(actMSISDN)){&#xd;
  errorMsg += "MSISDN - Expected:"+expMSISDN+" Actual:"+actMSISDN+".\r\n";&#xd;
  count++;&#xd;
 }&#xd;
 if(!expMSISDN.equals(actPrepaidSubscriberId)){&#xd;
  errorMsg += "Prepaid SubscriberID - Expected:"+expMSISDN+" Actual:"+actPrepaidSubscriberId+".\r\n";&#xd;
  count++;&#xd;
 }&#xd;
 if(!subscriberType.equals("Prepaid")){&#xd;
  errorMsg += "SubscriberType - Expected:Prepaid Actual:"+subscriberType+".\r\n";&#xd;
  count++;&#xd;
 }&#xd;
 if(totalBalInCents.equals("")){&#xd;
  errorMsg += "TotalBalanceInCents returned null.\r\n";&#xd;
  count++;&#xd;
 }&#xd;
 if(balExpiryDate.equals("")){&#xd;
  errorMsg += "BalanceExpiryDate returned null.\r\n";&#xd;
  count++;&#xd;
 }&#xd;
 if(!periodicChargeStatus.equals("paid")){&#xd;
  errorMsg += "PeriodicChargeStatus - Expected:paid Actual:"+periodicChargeStatus+".\r\n";&#xd;
  count++;&#xd;
 }&#xd;
}&#xd;
&#xd;
if(count&gt;0){&#xd;
    testExec.setStateValue("Test_Execution_Status", "Failed");&#xd;
    testExec.setStateValue("FailureDescription", errorMsg);&#xd;
}&#xd;
else{&#xd;
    testExec.setStateValue("Test_Execution_Status", "Passed");&#xd;
    testExec.setStateValue("FailureDescription", "");&#xd;
}</script>
    </Node>


    <Node log="" name="Subprocess ReportGenerator" next="ReadScenarios" quiet="true" think="0H" type="com.itko.lisa.utils.ExecSubProcessNode" uid="6CB973EBC5BF11EA89E18AB111CBFB47" useFilters="true" version="1"> 

<Subprocess>{{LISA_RELATIVE_PROJ_ROOT}}/Tests/Subprocesses/ReportGenerator.tst</Subprocess>
<fullyParseProps>false</fullyParseProps>
<sendCommonState>false</sendCommonState>
<getCommonState>false</getCommonState>
<onAbort>continue</onAbort>
<Parameters>
    <Parameter>
    <key>Test_Execution_Status</key>
    <value>{{Test_Execution_Status}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>TestCase_Description</key>
    <value>{{TestCase_Description}}</value>
    <name>Referenced 1st in ReportGenerator</name>
    </Parameter>
    <Parameter>
    <key>STEPNAME</key>
    <value>{{LISA_LAST_STEP}}</value>
    <name>Referenced 1st in ReportGenerator</name>
    </Parameter>
    <Parameter>
    <key>FailureDescription</key>
    <value>{{FailureDescription}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>ExecutionMode</key>
    <value>{{ExecutionMode}}</value>
    <name>Referenced 1st in ReportGenerator</name>
    </Parameter>
    <Parameter>
    <key>Comments</key>
    <value>MSISDN : {{MSISDN}} &lt;BR&gt; IMEI : {{imei}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>TC_Id</key>
    <value>{{TC_Id}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>Operation</key>
    <value>{{Operation}}</value>
    <name>Referenced 1st in Subprocess Historical_update</name>
    </Parameter>
    <Parameter>
    <key>EAR</key>
    <value>{{EAR}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
</Parameters>
<SaveProps>
</SaveProps>
    </Node>


    <Node log="" name="end" next="fail" quiet="true" think="0h" type="com.itko.lisa.test.NormalEnd" uid="6CB973ECC5BF11EA89E18AB111CBFB47" useFilters="true" version="1"> 

    </Node>


    <Node log="" name="fail" next="abort" quiet="true" think="0h" type="com.itko.lisa.test.Abend" uid="6CB973EDC5BF11EA89E18AB111CBFB47" useFilters="true" version="1"> 

    </Node>


    <Node log="" name="abort" next="" quiet="true" think="0h" type="com.itko.lisa.test.AbortStep" uid="6CB973EEC5BF11EA89E18AB111CBFB47" useFilters="true" version="1"> 

    </Node>


    <DataSet atend="end" local="false" maxItemsToFetch="100" name="Read Scenarios" random="false" type="custom.dataset.ExcelDataFile">
<sample>rO0ABXNyABFqYXZhLnV0aWwuSGFzaE1hcAUH2sHDFmDRAwACRgAKbG9hZEZhY3RvckkACXRocmVzaG9sZHhwP0AAAAAAABh3CAAAACAAAAASdAAKVENfRXhlY3V0ZXQAA1lFU3QACnBvc0JhdGNoSWR0AAEwdAAQcG9zVHJhbnNhY3Rpb25JZHQADDAwMDAxNDYzMzAwMnQACmVtcGxveWVlSWR0AAg4MDU2NjEwMXQACnRlcm1pbmFsSWR0AAY1NjYxMDF0AAh1c2VyTmFtZXQAEFBBWVNQT1RfRVBJTl9QQ1J0AAdzdG9yZUlkdAAMVk9YWFgwMDAwMDAwdAATZGVhbGVyVHJhbnNhY3Rpb25JZHEAfgAHdAAVUmVhZCBTY2VuYXJpb3NfUm93TnVtdAABMXQADWNoYW5uZWxEZXRhaWx0AARlYXB5dAALcmVxdWVzdE5hbWV0ABFnZXRDb3Vwb25SZXZlcnNhbHQACHBhc3N3b3JkdAAKUGFzc0B3b3JkMXQABVRDX0lkdAAFVEMwMDF0AAptZXJjaGFudElkdAAGNDIwMjI0dAALY2hhbm5lbE5hbWV0AAZWRU5ET1J0AAtSZXF1ZXN0RmlsZXEAfgAWdAAUVGVzdENhc2VfRGVzY3JpcHRpb250ABpQX1JldmVyc2VPcmlnaW5hbEdldGNvdXBvbnQADWRlYWxlckJhdGNoSWR0AAEweA==</sample>
    <location>C:/Users/mahaboob_s/Desktop/T-Mobile/U2_Project/Sprint 15/pdd_vendorservices-master/Data/InputFiles/DataSheet.xlsx</location>
    <sheetname>RedeemVendorCouponReversal</sheetname>
    <sRow>2</sRow>
    <eRow>3</eRow>
    </DataSet>

</TestCase>