<?xml version="1.0" encoding="UTF-8"?>
<TestCase name="HTML_Report" version="5">

<meta>
   <create author="spothed1" buildNumber="8.2.0.244" date="08/08/2017" host="LNAR-HXWTYF2" version="8.2.0"/>
   <lastEdited author="MKomala2" buildNumber="10.3.0.297" date="07/07/2020" host="LP-5CD7355fj7" version="10.3.0"/>
</meta>

<id>53D764ADCB1811EA80AF8AB111CBFB47</id>
<Documentation>Put documentation of the Test Case here.</Documentation>
<IsInProject>true</IsInProject>
<sig>ZWQ9NSZ0Y3Y9NSZsaXNhdj0xMC4zLjAgKDEwLjMuMC4yOTcpJm5vZGVzPTU4ODU2NDc4OQ==</sig>
<subprocess>false</subprocess>

<initState>
</initState>

<resultState>
</resultState>

<deletedProps>
    <key>TestSetId</key>
    <key>WSSERVER1</key>
    <key>WSSERVER2</key>
    <key>WSSERVER3</key>
    <key>Check</key>
    <key>ENDPOINT1</key>
    <key>ENDPOINT2</key>
    <key>ENDPOINT3</key>
    <key>lisa.Write_HTML_Header.rsp.time</key>
    <key>lisa.Write_HTML_Header.rsp</key>
    <key>EnvironmentTested</key>
    <key>WSPORT</key>
    <key>ENDPOINT</key>
    <key>WSPORT1</key>
    <key>WSPORT2</key>
    <key>WSPORT3</key>
    <key>WSSERVER</key>
</deletedProps>

    <Node log="" name="Write_HTML_Header" next="end" quiet="false" think="500-1S" type="com.itko.lisa.test.UserScriptNode" uid="53D764AECB1811EA80AF8AB111CBFB47" useFilters="true" version="1"> 


      <!-- Filters -->
      <Filter type="com.itko.lisa.test.FilterSavePropToFile">
        <valueToFilterKey>lisa.Write_HTML_Header.rsp</valueToFilterKey>
      <file>{{LOCDIR}}/Data/OutputFiles/{{EAR}}_ExecutionResults_{{EnvironmentTested}}.html</file>
      <append>true</append>
      </Filter>

<onerror>continue</onerror>
<language>velocity</language>
<copyProps>TestExecProps</copyProps>
<script>			&lt;html&gt;&#xd;
    &lt;body&gt;&#xd;
        &lt;left&gt; &#xd;
&lt;/center&gt;&#xd;
        &lt;table id="results" border=2 align=center&gt;&#xd;
            &lt;tr&gt;&#xd;
			&lt;th align=left colspan="9" bgcolor=black&gt;&lt;font color = 'magenta'&gt;Operation : RefundAccountReversal &lt;/th&gt;&#xd;
			&lt;/tr&gt;&#xd;
			&lt;tr&gt; &#xd;
                &lt;th bgcolor=black width=100&gt;&lt;font color = 'magenta'&gt;TC_Id&lt;/th&gt;&#xd;
				&lt;th bgcolor=black width=500&gt;&lt;font color = 'magenta'&gt;TestCase_Description&lt;/th&gt;&#xd;
				&lt;th bgcolor=black width=100&gt;&lt;font color = 'magenta'&gt;Execution_MODE&lt;/th&gt;&#xd;
				&lt;th bgcolor=black width=100&gt;&lt;font color = 'magenta'&gt;Result&lt;/th&gt;&#xd;
				&lt;th bgcolor=black width=200&gt;&lt;font color = 'magenta'&gt;Failed_STEP&lt;/th&gt;&#xd;
				&lt;th bgcolor=black width=500&gt;&lt;font color = 'magenta'&gt;Failure Description&lt;/th&gt;&#xd;
                &lt;th bgcolor=black width=500&gt;&lt;font color = 'magenta'&gt;Comments&lt;/th&gt;&#xd;
			&lt;/tr&gt;</script>
    </Node>


    <Node log="" name="end" next="fail" quiet="true" think="0h" type="com.itko.lisa.test.NormalEnd" uid="53D764AFCB1811EA80AF8AB111CBFB47" useFilters="true" version="1"> 

    </Node>


    <Node log="" name="fail" next="abort" quiet="true" think="0h" type="com.itko.lisa.test.Abend" uid="53D764B0CB1811EA80AF8AB111CBFB47" useFilters="true" version="1"> 

    </Node>


    <Node log="" name="abort" next="" quiet="true" think="0h" type="com.itko.lisa.test.AbortStep" uid="53D764B1CB1811EA80AF8AB111CBFB47" useFilters="true" version="1"> 

    </Node>


</TestCase>