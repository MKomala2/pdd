<?xml version="1.0" encoding="UTF-8"?>
<TestCase name="TC001_P_IMEI_Block" version="5">

<meta>
   <create author="MMelagi1" buildNumber="10.3.0.297" date="08/02/2019" host="LP-5CD7521B4F" version="10.3.0"/>
   <lastEdited author="MKomala2" buildNumber="10.3.0.297" date="07/21/2020" host="LP-5CD7355fj7" version="10.3.0"/>
</meta>

<id>AA13E156CB6B11EA9D328AB111CBFB47</id>
<IsInProject>true</IsInProject>
<sig>ZWQ9NSZ0Y3Y9NSZsaXNhdj0xMC4zLjAgKDEwLjMuMC4yOTcpJm5vZGVzPS01NDYxODAyMzc=</sig>
<subprocess>false</subprocess>

<initState>
</initState>

<resultState>
</resultState>

<deletedProps>
</deletedProps>

      <Companion type="com.itko.lisa.test.EventSubprocessCompanion">
<Events>
    <Parameter>
    <key>Step error</key>
    <value/>
    </Parameter>
    <Parameter>
    <key>Abort</key>
    <value/>
    </Parameter>
    <Parameter>
    <key>Cycle failed</key>
    <value/>
    </Parameter>
</Events>
<EventComponents>
    <Parameter>
    <key>Event ID</key>
    <value>EVENT_ID</value>
    </Parameter>
    <Parameter>
    <key>Long Description</key>
    <value>LONG_DESCRIPTION</value>
    </Parameter>
    <Parameter>
    <key>Short Description</key>
    <value>SHORT_DESCRIPTION</value>
    </Parameter>
    <Parameter>
    <key>Step Name</key>
    <value>STEP_NAME</value>
    </Parameter>
    <Parameter>
    <key>Test Name</key>
    <value>TEST_NAME</value>
    </Parameter>
    <Parameter>
    <key>Timestamp</key>
    <value>TIMESTAMP</value>
    </Parameter>
</EventComponents>
<CompanionSubprocess>
<Subprocess>{{LISA_RELATIVE_PROJ_ROOT}}/Tests/Subprocesses/FailReportGenerator.tst</Subprocess>
<fullyParseProps>false</fullyParseProps>
<sendCommonState>false</sendCommonState>
<getCommonState>false</getCommonState>
<onAbort>abort</onAbort>
<Parameters>
    <Parameter>
    <key>Test_Execution_Status</key>
    <value>{{Test_Execution_Status}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>TestCase_Description</key>
    <value>{{TestCase_Description}}</value>
    <name>Referenced 1st in ReportGenerator</name>
    </Parameter>
    <Parameter>
    <key>STEPNAME</key>
    <value>{{STEPNAME}}</value>
    <name>Referenced 1st in ReportGenerator</name>
    </Parameter>
    <Parameter>
    <key>LISA_STEP</key>
    <value>{{LISA_LAST_STEP}}</value>
    <name>Referenced 1st in ReportGenerator</name>
    </Parameter>
    <Parameter>
    <key>FailureDescription</key>
    <value>{{FailureDescription}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>Operation</key>
    <value>{{Operation}}</value>
    <name>Referenced 1st in ReportGenerator</name>
    </Parameter>
    <Parameter>
    <key>Comments</key>
    <value>{{Comments}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>TC_Id</key>
    <value>{{TC_Id}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
</Parameters>
<SaveProps>
</SaveProps>
</CompanionSubprocess>
      </Companion>

    <Node log="" name="ReadScenarios" next="Environment_Selection" quiet="true" think="0" type="com.itko.lisa.test.NoTransNode" uid="AA13E157CB6B11EA9D328AB111CBFB47" useFilters="true" version="1"> 


      <!-- Filters -->
      <Filter type="com.itko.lisa.test.TimeStampFilter">
        <valueToFilterKey>lisa.Do-Nothing Step.rsp</valueToFilterKey>
<pre>true</pre>
<post>false</post>
<datePattern>yyyy-MM-dd'T'HH:mm:ssXXX</datePattern>
<preprop>timestamp</preprop>
<postprop/>
<Offset>0</Offset>
      </Filter>


      <!-- Data Sets -->
<readrec>Read Scenarios</readrec>

      <!-- Assertions -->
<CheckResult assertTrue="false" name="CustomListener" type="com.itko.lisa.test.AssertByScript">
<log>Assertion name: CustomListener checks for: false  is of type: Assert by Script Execution.</log>
<then>continue</then>
<valueToAssertKey/>
        <script>import com.itko.lisa.test.*;&#xd;
import java.text.*;&#xd;
&#xd;
testExec.setStateValue("AssertMessage",""); &#xd;
TestEventListener custom_listener = new com.itko.lisa.test.TestEventListener()&#xd;
{&#xd;
public void testEvent(com.itko.lisa.test.TestEvent testevent)&#xd;
    {&#xd;
if(testevent.getEventId() == com.itko.lisa.test.TestEvent.EVENT_TRANSFAILED)&#xd;
        { &#xd;
testExec.setStateValue("AssertMessage",testevent.getLongDesc()); &#xd;
}&#xd;
}&#xd;
};&#xd;
&#xd;
testExec.setStateValue("Custom_Listener",custom_listener); &#xd;
testExec.getTestEventMgr().addListener(custom_listener,com.itko.lisa.test.TestEvent.noFilter); &#xd;
return true;&#xd;
</script>
        <language>BeanShell</language>
        <copyprops>TestExecPropsAndSystemProps</copyprops>
</CheckResult>

<CheckResult assertTrue="false" name="Check if test case execution required." type="com.itko.lisa.test.CheckResultPropRegEx">
<log>Assertion name: Ensure Property Matches Expression checks for: false is of type: Property Value Expression.</log>
<then>ReadScenarios</then>
<valueToAssertKey/>
        <prop>TC_Execute</prop>
        <param>YES</param>
</CheckResult>

    </Node>


    <Node log="" name="Environment_Selection" next="Initialize Counter" quiet="true" think="0H" type="com.itko.lisa.utils.ExecSubProcessNode" uid="AA13E158CB6B11EA9D328AB111CBFB47" useFilters="true" version="1"> 


      <!-- Assertions -->
<CheckResult assertTrue="false" name="Scripted Assertion" type="com.itko.lisa.test.AssertByScript">
<log>Assertion name: Scripted Assertion checks for: true is of type: Assert by Script Execution.</log>
<then>Subprocess ReportGenerator</then>
<valueToAssertKey/>
        <script>import com.itko.lisa.test.TestEvent;&#xd;
import java.text.*;&#xd;
&#xd;
if(testExec.getStateValue("AssertMessage").equals(""))&#xd;
{&#xd;
testExec.setStateValue("Test_Execution_Status", "Passed");&#xd;
testExec.setStateValue("Timing Log Location", testExec.getStateValue("Log Location"));&#xd;
testExec.setStateValue("Unique BODID", testExec.getStateValue("BODID"));&#xd;
if(testExec.getStateValue("Test_Script_Status")!= "Failed")&#xd;
{&#xd;
testExec.setStateValue("Test_Script_Status", "Passed");&#xd;
 }   &#xd;
return true;&#xd;
}&#xd;
else&#xd;
{&#xd;
testExec.setStateValue("Test_Execution_Status", "Failed");&#xd;
testExec.setStateValue("Timing Log Location", testExec.getStateValue("Log Location"));&#xd;
testExec.setStateValue("Unique BODID", testExec.getStateValue("BODID"));  &#xd;
testExec.setStateValue("Test_Script_Status", "Failed");&#xd;
testExec.raiseEvent(TestEvent.EVENT_TRANSFAILED,"Failure","Failure_Scripted_Assertion");&#xd;
return false;&#xd;
}</script>
        <language>BeanShell</language>
        <copyprops>TestExecPropsAndSystemProps</copyprops>
</CheckResult>

<Subprocess>{{LISA_RELATIVE_PROJ_ROOT}}/Tests/Subprocesses/Environment_Selection.tst</Subprocess>
<fullyParseProps>false</fullyParseProps>
<sendCommonState>false</sendCommonState>
<getCommonState>false</getCommonState>
<onAbort>continue</onAbort>
<Parameters>
    <Parameter>
    <key>EnvironmentTested</key>
    <value>{{EnvironmentTested}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
</Parameters>
<SaveProps>
<save>BW_Host</save>
<save>CE_Host</save>
</SaveProps>
    </Node>


    <Node log="" name="Initialize Counter" next="TimeStamp" quiet="false" think="500-1S" type="com.itko.lisa.test.UserScriptNode" uid="AA13E159CB6B11EA9D328AB111CBFB47" useFilters="true" version="1"> 


      <!-- Assertions -->
<CheckResult assertTrue="true" name="Any Exception Then Fail" type="com.itko.lisa.dynexec.CheckInvocationEx">
<log>Assertion name: Any Exception Then Fail checks for: true is of type: Assert on Invocation Exception.</log>
<then>continue</then>
<valueToAssertKey/>
        <param>.*</param>
</CheckResult>

<onerror>continue</onerror>
<language>BeanShell</language>
<copyProps>TestExecProps</copyProps>
<script>int counter=0;&#xd;
testExec.setStateValue("counter",counter);</script>
    </Node>


    <Node log="" name="TimeStamp" next="Load Request" quiet="true" think="0H" type="com.itko.lisa.utils.ExecSubProcessNode" uid="AA13E15ACB6B11EA9D328AB111CBFB47" useFilters="true" version="1"> 

<Subprocess>{{LISA_RELATIVE_PROJ_ROOT}}/Tests/Subprocesses/TimeStamp.tst</Subprocess>
<fullyParseProps>false</fullyParseProps>
<sendCommonState>false</sendCommonState>
<getCommonState>false</getCommonState>
<onAbort>continue</onAbort>
<Parameters>
</Parameters>
<SaveProps>
<save>DATE</save>
<save>TIMESTAMP</save>
</SaveProps>
    </Node>


    <Node log="" name="Load Request" next="Parse Request" quiet="true" think="500-1S" type="com.itko.lisa.test.FileNode" uid="AA13E15BCB6B11EA9D328AB111CBFB47" useFilters="true" version="1"> 


      <!-- Assertions -->
<CheckResult assertTrue="false" name="Scripted Assertion" type="com.itko.lisa.test.AssertByScript">
<log>Assertion name: Scripted Assertion checks for: true is of type: Assert by Script Execution.</log>
<then>Subprocess ReportGenerator</then>
<valueToAssertKey/>
        <script>import com.itko.lisa.test.TestEvent;&#xd;
import java.text.*;&#xd;
&#xd;
if(testExec.getStateValue("AssertMessage").equals(""))&#xd;
{&#xd;
testExec.setStateValue("Test_Execution_Status", "Passed");&#xd;
testExec.setStateValue("Timing Log Location", testExec.getStateValue("Log Location"));&#xd;
testExec.setStateValue("Unique BODID", testExec.getStateValue("BODID"));&#xd;
if(testExec.getStateValue("Test_Script_Status")!= "Failed")&#xd;
{&#xd;
testExec.setStateValue("Test_Script_Status", "Passed");&#xd;
 }   &#xd;
return true;&#xd;
}&#xd;
else&#xd;
{&#xd;
testExec.setStateValue("Test_Execution_Status", "Failed");&#xd;
testExec.setStateValue("Timing Log Location", testExec.getStateValue("Log Location"));&#xd;
testExec.setStateValue("Unique BODID", testExec.getStateValue("BODID"));  &#xd;
testExec.setStateValue("Test_Script_Status", "Failed");&#xd;
testExec.raiseEvent(TestEvent.EVENT_TRANSFAILED,"Failure","Failure_Scripted_Assertion");&#xd;
return false;&#xd;
}</script>
        <language>BeanShell</language>
        <copyprops>TestExecPropsAndSystemProps</copyprops>
</CheckResult>

<Loc>{{LISA_RELATIVE_PROJ_ROOT}}/Data/Requests/DeviceEnterprise/{{RequestFile}}.txt</Loc>
<charset>DEFAULT</charset>
<PropKey>Request</PropKey>
<onFail>continue</onFail>
    </Node>


    <Node log="" name="Parse Request" next="Send BW Request" quiet="true" think="500-1S" type="com.itko.lisa.utils.ParseTextContentNode" uid="AA13E15CCB6B11EA9D328AB111CBFB47" useFilters="true" version="1"> 


      <!-- Assertions -->
<CheckResult assertTrue="true" name="Check CE Execution Mode  flag" type="com.itko.lisa.test.CheckResultPropRegEx">
<log>Assertion name: Check CE flag checks for: true is of type: Property Value Expression.</log>
<then>Send CE Request</then>
<valueToAssertKey/>
        <prop>ExecutionMode</prop>
        <param>CE</param>
</CheckResult>

<CheckResult assertTrue="false" name="Scripted Assertion" type="com.itko.lisa.test.AssertByScript">
<log>Assertion name: Scripted Assertion checks for: true is of type: Assert by Script Execution.</log>
<then>Subprocess ReportGenerator</then>
<valueToAssertKey/>
        <script>import com.itko.lisa.test.TestEvent;&#xd;
import java.text.*;&#xd;
&#xd;
if(testExec.getStateValue("AssertMessage").equals(""))&#xd;
{&#xd;
testExec.setStateValue("Test_Execution_Status", "Passed");&#xd;
testExec.setStateValue("Timing Log Location", testExec.getStateValue("Log Location"));&#xd;
testExec.setStateValue("Unique BODID", testExec.getStateValue("BODID"));&#xd;
if(testExec.getStateValue("Test_Script_Status")!= "Failed")&#xd;
{&#xd;
testExec.setStateValue("Test_Script_Status", "Passed");&#xd;
 }   &#xd;
return true;&#xd;
}&#xd;
else&#xd;
{&#xd;
testExec.setStateValue("Test_Execution_Status", "Failed");&#xd;
testExec.setStateValue("Timing Log Location", testExec.getStateValue("Log Location"));&#xd;
testExec.setStateValue("Unique BODID", testExec.getStateValue("BODID"));  &#xd;
testExec.setStateValue("Test_Script_Status", "Failed");&#xd;
testExec.raiseEvent(TestEvent.EVENT_TRANSFAILED,"Failure","Failure_Scripted_Assertion");&#xd;
return false;&#xd;
}</script>
        <language>BeanShell</language>
        <copyprops>TestExecPropsAndSystemProps</copyprops>
</CheckResult>

<text>e3tSZXF1ZXN0fX0=</text>
<propKey>Request</propKey>
    </Node>


    <Node log="" name="Send BW Request" next="Send CE Request" quiet="false" think="500-1S" type="com.itko.lisa.ws.nx.NxWSStep" uid="AA13E15DCB6B11EA9D328AB111CBFB47" useFilters="true" version="1"> 


      <!-- Filters -->
      <Filter type="com.itko.lisa.test.FilterSaveResponse">
        <valueToFilterKey>lisa.Send BW Request.rsp</valueToFilterKey>
      <prop>BW_Response</prop>
      </Filter>

      <Filter type="com.itko.lisa.test.FilterSavePropToFile">
        <valueToFilterKey>lisa.Send BW Request.rsp</valueToFilterKey>
      <file>{{LOCDIR}}/Data/OutputFiles/{{TC_Id}}_{{TestCase_Description}}_BW.txt</file>
      <append>false</append>
      </Filter>

      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.Send BW Request.rsp</valueToFilterKey>
<prop>BW_ConfirmationCode</prop>
<xpathq>/*[local-name()='Envelope']/*[local-name()='Body']/*[local-name()='rechargeAccountResponse']/*[local-name()='confirmationInfo']/*[local-name()='code']/text()</xpathq>
      </Filter>

      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.Send BW Request.rsp</valueToFilterKey>
<prop>BW_SenderId</prop>
<xpathq>/*[local-name()='Envelope']/*[local-name()='Body']/*[local-name()='getDeviceDetailsResponse']/*[local-name()='header']/*[local-name()='sender']/*[local-name()='senderId']/text()</xpathq>
      </Filter>

      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.Send BW Request.rsp</valueToFilterKey>
<prop>BW_ChannelId</prop>
<xpathq>/*[local-name()='Envelope']/*[local-name()='Body']/*[local-name()='getDeviceDetailsResponse']/*[local-name()='header']/*[local-name()='sender']/*[local-name()='channelId']/text()</xpathq>
      </Filter>

      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.Send BW Request.rsp</valueToFilterKey>
<prop>BW_sessionId</prop>
<xpathq>/*[local-name()='Envelope']/*[local-name()='Body']/*[local-name()='getDeviceDetailsResponse']/*[local-name()='header']/*[local-name()='sender']/*[local-name()='sessionId']/text()</xpathq>
      </Filter>

      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.Send BW Request.rsp</valueToFilterKey>
<prop>BW_activityId</prop>
<xpathq>/*[local-name()='Envelope']/*[local-name()='Body']/*[local-name()='getDeviceDetailsResponse']/*[local-name()='header']/*[local-name()='sender']/*[local-name()='activityId']/text()</xpathq>
      </Filter>

      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.Send BW Request.rsp</valueToFilterKey>
<prop>BW_ApplicationId</prop>
<xpathq>/*[local-name()='Envelope']/*[local-name()='Body']/*[local-name()='getDeviceDetailsResponse']/*[local-name()='header']/*[local-name()='sender']/*[local-name()='applicationId']/text()</xpathq>
      </Filter>

      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.Send BW Request.rsp</valueToFilterKey>
<prop>BW_workflowId</prop>
<xpathq>/*[local-name()='Envelope']/*[local-name()='Body']/*[local-name()='getDeviceDetailsResponse']/*[local-name()='header']/*[local-name()='sender']/*[local-name()='workflowId']/text()</xpathq>
      </Filter>


      <!-- Assertions -->
<CheckResult assertTrue="false" name="Check ConfirmationCode" type="com.itko.lisa.test.AssertByScript">
<log>Assertion name: Check ConfirmationCode checks for: false is of type: Assert by Script Execution.</log>
<then>continue</then>
<valueToAssertKey/>
        <script>String confirmationCode = testExec.getStateValue("BW_ConfirmationCode");&#xd;
if(!confirmationCode.equals("")){&#xd;
 testExec.setStateValue("BW_Code","100");&#xd;
}&#xd;
else{&#xd;
testExec.setStateValue("BW_Code","102");&#xd;
}&#xd;
&#xd;
return true;</script>
        <language>BeanShell</language>
        <copyprops>TestExecPropsAndSystemProps</copyprops>
</CheckResult>

<CheckResult assertTrue="true" name="Check BW Execution Mode  flag" type="com.itko.lisa.test.CheckResultPropRegEx">
<log>Assertion name: Check CE flag checks for: true is of type: Property Value Expression.</log>
<then>Validate Params</then>
<valueToAssertKey/>
        <prop>ExecutionMode</prop>
        <param>BW</param>
</CheckResult>

<CheckResult assertTrue="false" name="Scripted Assertion" type="com.itko.lisa.test.AssertByScript">
<log>Assertion name: Scripted Assertion checks for: true is of type: Assert by Script Execution.</log>
<then>Subprocess ReportGenerator</then>
<valueToAssertKey/>
        <script>import com.itko.lisa.test.TestEvent;&#xd;
import java.text.*;&#xd;
&#xd;
if(testExec.getStateValue("AssertMessage").equals(""))&#xd;
{&#xd;
testExec.setStateValue("Test_Execution_Status", "Passed");&#xd;
testExec.setStateValue("Timing Log Location", testExec.getStateValue("Log Location"));&#xd;
testExec.setStateValue("Unique BODID", testExec.getStateValue("BODID"));&#xd;
if(testExec.getStateValue("Test_Script_Status")!= "Failed")&#xd;
{&#xd;
testExec.setStateValue("Test_Script_Status", "Passed");&#xd;
 }   &#xd;
return true;&#xd;
}&#xd;
else&#xd;
{&#xd;
testExec.setStateValue("Test_Execution_Status", "Failed");&#xd;
testExec.setStateValue("Timing Log Location", testExec.getStateValue("Log Location"));&#xd;
testExec.setStateValue("Unique BODID", testExec.getStateValue("BODID"));  &#xd;
testExec.setStateValue("Test_Script_Status", "Failed");&#xd;
testExec.raiseEvent(TestEvent.EVENT_TRANSFAILED,"Failure","Failure_Scripted_Assertion");&#xd;
return false;&#xd;
}</script>
        <language>BeanShell</language>
        <copyprops>TestExecPropsAndSystemProps</copyprops>
</CheckResult>

<wsdl>{{LISA_RELATIVE_PROJ_URL}}/Data/WSDL/DeviceEnterprise.wsdl</wsdl>
<endpoint>https://{{BW_Host}}/service/soap/ProductManagement/DeviceEnterprise/V1</endpoint>
<targetNamespace>http://services.tmobile.com</targetNamespace>
<service>DeviceEnterprise</service>
<port>DeviceEnterprise</port>
<operation>manageBlackList</operation>
<onError>continue</onError>
<maintainSession>true</maintainSession>
<clearSession>false</clearSession>
<request itko_enc="base64">e3tSZXF1ZXN0fX0=</request>
<style>document</style>
<use>literal</use>
<soapAction>manageBlackList</soapAction>
<sslInfo>
<ssl-keystore-file/>
<ssl-keystore-password-enc>l3e1f0d1aee5ef054b13e52aa2d967547e5e6bc723d12ee87729a2c1293d7821c</ssl-keystore-password-enc>
<ssl-alias/>
<ssl-key-password-enc>l4a5fa71376e02625830a799738f5f2b92dd864074a4ee31f7d08ebee09f488de</ssl-key-password-enc>
</sslInfo>
<wsiInfo>
<wsi-check-wsdl>false</wsi-check-wsdl>
<wsi-check-msg>false</wsi-check-msg>
<wsi-display-type>onlyFailed</wsi-display-type>
<wsi-on-fail>continue</wsi-on-fail>
</wsiInfo>
<transport>
<soapVersionUri>1.1</soapVersionUri>
<httpVersionUri>1.1</httpVersionUri>
<webMethodUri>POST</webMethodUri>
<mtom>false</mtom>
<dontSendRequest>false</dontSendRequest>
<fault-as-error>true</fault-as-error>
<callTimeout>30000</callTimeout>
</transport>
<uddiActive>false</uddiActive>
<uddi-result>
<uddi-selection>
</uddi-selection>
</uddi-result>
<wss4jInfo>
<version>5</version>
<wssecurity>
<isSend>true</isSend>
<must>false</must>
<role/>
</wssecurity>
<wsi-bsp>false</wsi-bsp>
</wss4jInfo>
<wsaInfo>
<useAddressing>false</useAddressing>
<must>false</must>
<to/>
<toOverride>false</toOverride>
<toDefault>true</toDefault>
<from/>
<fromOverride>false</fromOverride>
<fromDefault>true</fromDefault>
<action/>
<actionOverride>false</actionOverride>
<actionDefault>true</actionDefault>
<msgid/>
<msgidOverride>false</msgidOverride>
<msgidDefault>true</msgidDefault>
<replyTo/>
<replyToOverride>false</replyToOverride>
<faultTo/>
<faulttToOverride>false</faulttToOverride>
<addressingVersion>http://www.w3.org/2005/08/addressing</addressingVersion>
</wsaInfo>
<customHTTPHeaderInfo>
</customHTTPHeaderInfo>
<attachments>
<attachment-type>MIME</attachment-type>
</attachments>
<mtom-paths>
</mtom-paths>
    </Node>


    <Node log="" name="Send CE Request" next="Compare_BW_CE" quiet="false" think="500-1S" type="com.itko.lisa.ws.nx.NxWSStep" uid="AA13E15ECB6B11EA9D328AB111CBFB47" useFilters="true" version="1"> 


      <!-- Filters -->
      <Filter type="com.itko.lisa.test.FilterSavePropToFile">
        <valueToFilterKey>lisa.Send CE Request.rsp</valueToFilterKey>
      <file>{{LOCDIR}}/Data/OutputFiles/{{TC_Id}}_{{TestCase_Description}}_CE.txt</file>
      <append>false</append>
      </Filter>

      <Filter type="com.itko.lisa.test.FilterSaveResponse">
        <valueToFilterKey>lisa.Send CE Request.rsp</valueToFilterKey>
      <prop>CE_Response</prop>
      </Filter>

      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.Send CE Request.rsp</valueToFilterKey>
<prop>CE_ConfirmationCode</prop>
<xpathq>/*[local-name()='Envelope']/*[local-name()='Body']/*[local-name()='rechargeAccountResponse']/*[local-name()='confirmationInfo']/*[local-name()='code']/text()</xpathq>
      </Filter>

      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.Send BW Request.rsp</valueToFilterKey>
<prop>CE_SenderId</prop>
<xpathq>/*[local-name()='Envelope']/*[local-name()='Body']/*[local-name()='manageBlackListResponse']/*[local-name()='header']/*[local-name()='sender']/*[local-name()='senderId']/text()</xpathq>
      </Filter>

      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.Send BW Request.rsp</valueToFilterKey>
<prop>CE_ChannelId</prop>
<xpathq>/*[local-name()='Envelope']/*[local-name()='Body']/*[local-name()='manageBlackListResponse']/*[local-name()='header']/*[local-name()='sender']/*[local-name()='channelId']/text()</xpathq>
      </Filter>

      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.Send BW Request.rsp</valueToFilterKey>
<prop>CE_ApplicationId</prop>
<xpathq>/*[local-name()='Envelope']/*[local-name()='Body']/*[local-name()='manageBlackListResponse']/*[local-name()='header']/*[local-name()='sender']/*[local-name()='applicationId']/text()</xpathq>
      </Filter>

      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.Send BW Request.rsp</valueToFilterKey>
<prop>CE_ProviderId</prop>
<xpathq>/*[local-name()='Envelope']/*[local-name()='Body']/*[local-name()='manageBlackListResponse']/*[local-name()='header']/*[local-name()='providerId'][2]/*[local-name()='id']/text()</xpathq>
      </Filter>


      <!-- Assertions -->
<CheckResult assertTrue="false" name="Check ConfirmationCode" type="com.itko.lisa.test.AssertByScript">
<log>Assertion name: Check ConfirmationCode checks for: true is of type: Assert by Script Execution.</log>
<then>continue</then>
<valueToAssertKey/>
        <script>// This script should return a boolean result indicating the assertion is true or false&#xd;
&#xd;
String confirmationCode = testExec.getStateValue("CE_ConfirmationCode");&#xd;
if(!confirmationCode.equals("")){&#xd;
 testExec.setStateValue("CE_Code","100");&#xd;
}&#xd;
else{&#xd;
testExec.setStateValue("CE_Code","102");&#xd;
}&#xd;
&#xd;
return true;</script>
        <language>BeanShell</language>
        <copyprops>TestExecPropsAndSystemProps</copyprops>
</CheckResult>

<CheckResult assertTrue="true" name="Check CE Execution Mode  flag" type="com.itko.lisa.test.CheckResultPropRegEx">
<log>Assertion name: Check CE flag checks for: true is of type: Property Value Expression.</log>
<then>Validate Params</then>
<valueToAssertKey/>
        <prop>ExecutionMode</prop>
        <param>CE</param>
</CheckResult>

<CheckResult assertTrue="false" name="Scripted Assertion" type="com.itko.lisa.test.AssertByScript">
<log>Assertion name: Scripted Assertion checks for: true is of type: Assert by Script Execution.</log>
<then>Subprocess ReportGenerator</then>
<valueToAssertKey/>
        <script>import com.itko.lisa.test.TestEvent;&#xd;
import java.text.*;&#xd;
&#xd;
if(testExec.getStateValue("AssertMessage").equals(""))&#xd;
{&#xd;
testExec.setStateValue("Test_Execution_Status", "Passed");&#xd;
testExec.setStateValue("Timing Log Location", testExec.getStateValue("Log Location"));&#xd;
testExec.setStateValue("Unique BODID", testExec.getStateValue("BODID"));&#xd;
if(testExec.getStateValue("Test_Script_Status")!= "Failed")&#xd;
{&#xd;
testExec.setStateValue("Test_Script_Status", "Passed");&#xd;
 }   &#xd;
return true;&#xd;
}&#xd;
else&#xd;
{&#xd;
testExec.setStateValue("Test_Execution_Status", "Failed");&#xd;
testExec.setStateValue("Timing Log Location", testExec.getStateValue("Log Location"));&#xd;
testExec.setStateValue("Unique BODID", testExec.getStateValue("BODID"));  &#xd;
testExec.setStateValue("Test_Script_Status", "Failed");&#xd;
testExec.raiseEvent(TestEvent.EVENT_TRANSFAILED,"Failure","Failure_Scripted_Assertion");&#xd;
return false;&#xd;
}</script>
        <language>BeanShell</language>
        <copyprops>TestExecPropsAndSystemProps</copyprops>
</CheckResult>

<wsdl>{{LISA_RELATIVE_PROJ_URL}}/Data/WSDL/DeviceEnterprise.wsdl</wsdl>
<endpoint>https://enterprisedevice-{{CE_Host}}/service/soap/ProductManagement/DeviceEnterprise/V1</endpoint>
<targetNamespace>http://services.tmobile.com</targetNamespace>
<service>DeviceEnterprise</service>
<port>DeviceEnterprise</port>
<operation>manageBlackList</operation>
<onError>continue</onError>
<maintainSession>true</maintainSession>
<clearSession>false</clearSession>
<request itko_enc="base64">e3tSZXF1ZXN0fX0=</request>
<style>document</style>
<use>literal</use>
<soapAction>manageBlackList</soapAction>
<sslInfo>
<ssl-keystore-file/>
<ssl-keystore-password-enc>l5e8658ccc305884df1a0f989aac9f0e3f936852cf0b5e8a7eefebe6c6f5c2ae4</ssl-keystore-password-enc>
<ssl-alias/>
<ssl-key-password-enc>l90b543ef2c28a64c7214441f07605b3403e9be9b665978860945885509b69d99</ssl-key-password-enc>
</sslInfo>
<wsiInfo>
<wsi-check-wsdl>false</wsi-check-wsdl>
<wsi-check-msg>false</wsi-check-msg>
<wsi-display-type>onlyFailed</wsi-display-type>
<wsi-on-fail>continue</wsi-on-fail>
</wsiInfo>
<transport>
<soapVersionUri>1.1</soapVersionUri>
<httpVersionUri>1.1</httpVersionUri>
<webMethodUri>POST</webMethodUri>
<mtom>false</mtom>
<dontSendRequest>false</dontSendRequest>
<fault-as-error>true</fault-as-error>
<callTimeout>30000</callTimeout>
</transport>
<uddiActive>false</uddiActive>
<uddi-result>
<uddi-selection>
</uddi-selection>
</uddi-result>
<wss4jInfo>
<version>5</version>
<wssecurity>
<isSend>true</isSend>
<must>false</must>
<role/>
</wssecurity>
<wsi-bsp>false</wsi-bsp>
</wss4jInfo>
<wsaInfo>
<useAddressing>false</useAddressing>
<must>false</must>
<to/>
<toOverride>false</toOverride>
<toDefault>true</toDefault>
<from/>
<fromOverride>false</fromOverride>
<fromDefault>true</fromDefault>
<action/>
<actionOverride>false</actionOverride>
<actionDefault>true</actionDefault>
<msgid/>
<msgidOverride>false</msgidOverride>
<msgidDefault>true</msgidDefault>
<replyTo/>
<replyToOverride>false</replyToOverride>
<faultTo/>
<faulttToOverride>false</faulttToOverride>
<addressingVersion>http://www.w3.org/2005/08/addressing</addressingVersion>
</wsaInfo>
<customHTTPHeaderInfo>
</customHTTPHeaderInfo>
<attachments>
<attachment-type>MIME</attachment-type>
</attachments>
<mtom-paths>
</mtom-paths>
    </Node>


    <Node log="" name="Compare_BW_CE" next="Validate Params" quiet="true" think="0H" type="com.itko.lisa.utils.ExecSubProcessNode" uid="AA13E15FCB6B11EA9D328AB111CBFB47" useFilters="true" version="1"> 


      <!-- Assertions -->
<CheckResult assertTrue="false" name="IgnoreValidDifferences" type="com.itko.lisa.test.AssertByScript">
<log>Assertion name: IgnoreValidDifferences checks for: false is of type: Assert by Script Execution.</log>
<then>continue</then>
<valueToAssertKey/>
        <script>String status = testExec.getStateValue("Test_Execution_Status");&#xd;
//String [] ignoreAttributes = {"child nodes"};&#xd;
String [] ignoreAttributes = {"//Envelope[1].","//Envelope[1]/Body[1].","//Envelope[1]/Header[1]."};&#xd;
if(status.equals("Failed")){&#xd;
&#xd;
 String errMsg = testExec.getStateValue("FailureDescription");&#xd;
 String [] errMsgArray = errMsg.replaceAll("\r","").replaceAll("\n",",").split(",");&#xd;
 &#xd;
/* for(String s : errMsgArray){&#xd;
  testExec.log("Errors in Array", s);&#xd;
 }*/&#xd;
 List ignoreList = new ArrayList();&#xd;
 for(int j=0;j&lt;errMsgArray.length;j++){&#xd;
   for(int i=0;i&lt;ignoreAttributes.length;i++){&#xd;
      if(errMsgArray[j].contains(ignoreAttributes[i])){&#xd;
       ignoreList.add(errMsgArray[j]);&#xd;
       break;&#xd;
      }&#xd;
   }&#xd;
 }&#xd;
&#xd;
//testExec.log("IgnoreListSize", ignoreList.size().toString());&#xd;
String finalErrMsg = "";&#xd;
 if(ignoreList.size() &gt; 0){&#xd;
   for(String s:errMsgArray){&#xd;
     if(!ignoreList.contains(s)){&#xd;
       finalErrMsg = finalErrMsg +s+"\r\n";&#xd;
     }&#xd;
   }&#xd;
  if(finalErrMsg.equals("")){&#xd;
    testExec.setStateValue("FailureDescription", finalErrMsg);&#xd;
    testExec.setStateValue("Test_Execution_Status", "Passed");&#xd;
  }&#xd;
  else{&#xd;
   testExec.setStateValue("FailureDescription", finalErrMsg);&#xd;
  }&#xd;
 }&#xd;
&#xd;
}&#xd;
&#xd;
return true;</script>
        <language>BeanShell</language>
        <copyprops>TestExecPropsAndSystemProps</copyprops>
</CheckResult>

<CheckResult assertTrue="false" name="check for Test_Execution_Status" type="com.itko.lisa.test.CheckResultPropRegEx">
<log>Assertion name: check for TestStatus checks for: false is of type: Property Value Expression.</log>
<then>continue</then>
<valueToAssertKey/>
        <prop>Test_Execution_Status</prop>
        <param>Passed</param>
</CheckResult>

<CheckResult assertTrue="false" name="Scripted Assertion" type="com.itko.lisa.test.AssertByScript">
<log>Assertion name: Scripted Assertion checks for: true is of type: Assert by Script Execution.</log>
<then>Subprocess ReportGenerator</then>
<valueToAssertKey/>
        <script>import com.itko.lisa.test.TestEvent;&#xd;
import java.text.*;&#xd;
&#xd;
if(testExec.getStateValue("AssertMessage").equals(""))&#xd;
{&#xd;
testExec.setStateValue("Test_Execution_Status", "Passed");&#xd;
testExec.setStateValue("Timing Log Location", testExec.getStateValue("Log Location"));&#xd;
testExec.setStateValue("Unique BODID", testExec.getStateValue("BODID"));&#xd;
if(testExec.getStateValue("Test_Script_Status")!= "Failed")&#xd;
{&#xd;
testExec.setStateValue("Test_Script_Status", "Passed");&#xd;
 }   &#xd;
return true;&#xd;
}&#xd;
else&#xd;
{&#xd;
testExec.setStateValue("Test_Execution_Status", "Failed");&#xd;
testExec.setStateValue("Timing Log Location", testExec.getStateValue("Log Location"));&#xd;
testExec.setStateValue("Unique BODID", testExec.getStateValue("BODID"));  &#xd;
testExec.setStateValue("Test_Script_Status", "Failed");&#xd;
testExec.raiseEvent(TestEvent.EVENT_TRANSFAILED,"Failure","Failure_Scripted_Assertion");&#xd;
return false;&#xd;
}</script>
        <language>BeanShell</language>
        <copyprops>TestExecPropsAndSystemProps</copyprops>
</CheckResult>

<Subprocess>{{LISA_RELATIVE_PROJ_ROOT}}/Tests/Subprocesses/Compare_BW_CE.tst</Subprocess>
<fullyParseProps>false</fullyParseProps>
<sendCommonState>false</sendCommonState>
<getCommonState>false</getCommonState>
<onAbort>continue</onAbort>
<Parameters>
    <Parameter>
    <key>CE_Response</key>
    <value>{{CE_Response}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>BW_Response</key>
    <value>{{BW_Response}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>TC_Id</key>
    <value>{{TC_Id}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>TestCase_Description</key>
    <value>{{TestCase_Description}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>Operation</key>
    <value>{{Operation}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>CE_Code</key>
    <value>{{CE_Code}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>BW_Code</key>
    <value>{{BW_Code}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>IgnoreAttributes</key>
    <value>providerId</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
</Parameters>
<SaveProps>
<save>FailureDescription</save>
<save>Test_Execution_Status</save>
</SaveProps>
    </Node>


    <Node log="" name="Validate Params" next="Wait for SPlunk" quiet="false" think="500-1S" type="com.itko.lisa.test.UserScriptNode" uid="AA13E160CB6B11EA9D328AB111CBFB47" useFilters="true" version="1"> 


      <!-- Assertions -->
<CheckResult assertTrue="true" name="Any Exception Then Fail" type="com.itko.lisa.dynexec.CheckInvocationEx">
<log>Assertion name: Any Exception Then Fail checks for: true is of type: Assert on Invocation Exception.</log>
<then>continue</then>
<valueToAssertKey/>
        <param>.*</param>
</CheckResult>

<CheckResult assertTrue="false" name="check for Test_Execution_Status" type="com.itko.lisa.test.CheckResultPropRegEx">
<log>Assertion name: check for TestStatus checks for: false is of type: Property Value Expression.</log>
<then>continue</then>
<valueToAssertKey/>
        <prop>Test_Execution_Status</prop>
        <param>Passed</param>
</CheckResult>

<CheckResult assertTrue="false" name="Scripted Assertion" type="com.itko.lisa.test.AssertByScript">
<log>Assertion name: Scripted Assertion checks for: true is of type: Assert by Script Execution.</log>
<then>Subprocess ReportGenerator</then>
<valueToAssertKey/>
        <script>import com.itko.lisa.test.TestEvent;&#xd;
import java.text.*;&#xd;
&#xd;
if(testExec.getStateValue("AssertMessage").equals(""))&#xd;
{&#xd;
testExec.setStateValue("Test_Execution_Status", "Passed");&#xd;
testExec.setStateValue("Timing Log Location", testExec.getStateValue("Log Location"));&#xd;
testExec.setStateValue("Unique BODID", testExec.getStateValue("BODID"));&#xd;
if(testExec.getStateValue("Test_Script_Status")!= "Failed")&#xd;
{&#xd;
testExec.setStateValue("Test_Script_Status", "Passed");&#xd;
 }   &#xd;
return true;&#xd;
}&#xd;
else&#xd;
{&#xd;
testExec.setStateValue("Test_Execution_Status", "Failed");&#xd;
testExec.setStateValue("Timing Log Location", testExec.getStateValue("Log Location"));&#xd;
testExec.setStateValue("Unique BODID", testExec.getStateValue("BODID"));  &#xd;
testExec.setStateValue("Test_Script_Status", "Failed");&#xd;
testExec.raiseEvent(TestEvent.EVENT_TRANSFAILED,"Failure","Failure_Scripted_Assertion");&#xd;
return false;&#xd;
}</script>
        <language>BeanShell</language>
        <copyprops>TestExecPropsAndSystemProps</copyprops>
</CheckResult>

<onerror>continue</onerror>
<language>BeanShell</language>
<copyProps>TestExecProps</copyProps>
<script>String expSenderId = testExec.getStateValue("senderId");&#xd;
String expChannelId = testExec.getStateValue("channelId");&#xd;
String expApplicationId = testExec.getStateValue("applicationId");&#xd;
String expInteractionId = testExec.getStateValue("interactionId");&#xd;
String expResponseCode = "100";&#xd;
String expOrderResponseCode  ="100";&#xd;
String expQuantity = testExec.getStateValue("Quantity");&#xd;
String expUOM = testExec.getStateValue("UOM");&#xd;
String expSKU = testExec.getStateValue("SKU");&#xd;
String expGroup = testExec.getStateValue("Group");&#xd;
String expPackage = testExec.getStateValue("Package");&#xd;
String expInventoryChannelCode = testExec.getStateValue("InventoryChannelCode");&#xd;
String expProductType = testExec.getStateValue("ProductType");&#xd;
&#xd;
String errorMsg="";&#xd;
int count = 0;&#xd;
String executionMode = testExec.getStateValue("ExecutionMode");&#xd;
String code = "";&#xd;
String providerId = "";&#xd;
String actSenderId = "";&#xd;
String actChannelId = "";&#xd;
String actApplicationId = "";&#xd;
String actInteractionId = "";&#xd;
String actOrderResponseCode = "";&#xd;
String actOrderQty = "";&#xd;
String actAvailableQty = "";&#xd;
String actUOM = "";&#xd;
String actSKU = "";&#xd;
String actGroup = "";&#xd;
String actPackage = "";&#xd;
String actProductType = "";&#xd;
String availableStartDate = "";&#xd;
String availableEndDate = "";&#xd;
String inventoryStatus = "";&#xd;
String entryTypeChannel = "";&#xd;
String preAuthorizedIndicator = "";&#xd;
String shipNode = "";&#xd;
&#xd;
if(executionMode.equals("BW")){&#xd;
 code = testExec.getStateValue("BW_Code");&#xd;
 providerId = testExec.getStateValue("BW_ProviderId");&#xd;
 actSenderId = testExec.getStateValue("BW_SenderId");&#xd;
 actChannelId = testExec.getStateValue("BW_ChannelId");&#xd;
 actApplicationId = testExec.getStateValue("BW_ApplicationId");&#xd;
 actInteractionId = testExec.getStateValue("BW_InteractionId");&#xd;
 actOrderResponseCode = testExec.getStateValue("BW_OrderResponseCode");&#xd;
 actOrderQty = testExec.getStateValue("BW_OrderQty");&#xd;
 actAvailableQty = testExec.getStateValue("BW_AvailableQty");&#xd;
 actUOM = testExec.getStateValue("BW_UOM");&#xd;
 actSKU = testExec.getStateValue("BW_SKU");&#xd;
 actGroup = testExec.getStateValue("BW_Group");&#xd;
 actPackage = testExec.getStateValue("BW_Package");&#xd;
 availableStartDate = testExec.getStateValue("BW_AvailableStartDate");&#xd;
 availableEndDate = testExec.getStateValue("BW_AvailableEndDate");&#xd;
 inventoryStatus = testExec.getStateValue("BW_InventoryStatus");&#xd;
 entryTypeChannel = testExec.getStateValue("BW_EntryTypeChannel");&#xd;
 preAuthorizedIndicator = testExec.getStateValue("BW_PreAuthorizedIndicator");&#xd;
 shipNode = testExec.getStateValue("BW_ShipNode");&#xd;
 actProductType = testExec.getStateValue("BW_ProductType");&#xd;
}&#xd;
&#xd;
else {&#xd;
 code = testExec.getStateValue("CE_Code");&#xd;
 providerId = testExec.getStateValue("CE_ProviderId");&#xd;
 actSenderId = testExec.getStateValue("CE_SenderId");&#xd;
 actChannelId = testExec.getStateValue("CE_ChannelId");&#xd;
 actApplicationId = testExec.getStateValue("CE_ApplicationId");&#xd;
 actInteractionId = testExec.getStateValue("CE_InteractionId");&#xd;
 actOrderResponseCode = testExec.getStateValue("CE_OrderResponseCode");&#xd;
 actOrderQty = testExec.getStateValue("CE_OrderQty");&#xd;
 actAvailableQty = testExec.getStateValue("CE_AvailableQty");&#xd;
 actUOM = testExec.getStateValue("CE_UOM");&#xd;
 actSKU = testExec.getStateValue("CE_SKU");&#xd;
 actGroup = testExec.getStateValue("CE_Group");&#xd;
 actPackage = testExec.getStateValue("CE_Package");&#xd;
 availableStartDate = testExec.getStateValue("CE_AvailableStartDate");&#xd;
 availableEndDate = testExec.getStateValue("CE_AvailableEndDate");&#xd;
 inventoryStatus = testExec.getStateValue("CE_InventoryStatus");&#xd;
 entryTypeChannel = testExec.getStateValue("CE_EntryTypeChannel");&#xd;
 preAuthorizedIndicator = testExec.getStateValue("CE_PreAuthorizedIndicator");&#xd;
 shipNode = testExec.getStateValue("CE_ShipNode");&#xd;
 actProductType = testExec.getStateValue("CE_ProductType");&#xd;
}&#xd;
&#xd;
testExec.log("group","{{CE_Group}}");&#xd;
//Validation&#xd;
&#xd;
if(!code.equals(expResponseCode)){&#xd;
 errorMsg += "ResponseStatusCode Expected:"+expResponseCode+" Actual:"+code+".\r\n";&#xd;
 count++;&#xd;
}&#xd;
else if(!expOrderResponseCode.equals(actOrderResponseCode)){&#xd;
  errorMsg += "Order ResponseCode - Expected:"+expOrderResponseCode+" Actual:"+actOrderResponseCode+".\r\n";&#xd;
  count++;&#xd;
}&#xd;
else{&#xd;
 if(providerId.equals("")){&#xd;
  errorMsg += "Provider Id is null.\r\n";&#xd;
  count++;&#xd;
 }&#xd;
 if(!expSenderId.equals(actSenderId)){&#xd;
  errorMsg += "SenderId - Expected:"+expSenderId+" Actual:"+actSenderId+".\r\n";&#xd;
  count++;&#xd;
 }&#xd;
 if(!expChannelId.equals(actChannelId)){&#xd;
  errorMsg += "ChannelId - Expected:"+expChannelId+" Actual:"+actChannelId+".\r\n";&#xd;
  count++;&#xd;
 }&#xd;
 if(!expApplicationId.equals(actApplicationId)){&#xd;
  errorMsg += "ApplicationId - Expected:"+expApplicationId+" Actual:"+actApplicationId+".\r\n";&#xd;
  count++;&#xd;
 }&#xd;
 if(!expInteractionId.equals(actInteractionId)){&#xd;
  errorMsg += "InteractionId - Expected:"+expInteractionId+" Actual:"+actInteractionId+".\r\n";&#xd;
  count++;&#xd;
 }&#xd;
 if(!expChannelId.equals(entryTypeChannel) || entryTypeChannel.equals("")){&#xd;
  errorMsg += "EntryType - Expected:"+expChannelId+" Actual:"+entryTypeChannel+".\r\n";&#xd;
  count++;&#xd;
 }&#xd;
 if(shipNode.equals("")){&#xd;
  errorMsg += "ShipNode is null.\r\n";&#xd;
  count++;&#xd;
 }&#xd;
 if(!expQuantity.equals(actOrderQty) || actOrderQty.equals("")){&#xd;
  errorMsg += "OrderQty - Expected:"+expQuantity+" Actual:"+actOrderQty+".\r\n";&#xd;
  count++;&#xd;
 }&#xd;
 if(!expUOM.equals(actUOM) || actUOM.equals("")){&#xd;
  errorMsg += "UnitOfMeasure - Expected:"+expUOM+" Actual:"+actUOM+".\r\n";&#xd;
  count++;&#xd;
 }&#xd;
 if(!expSKU.equals(actSKU) || actSKU.equals("")){&#xd;
  errorMsg += "ItemID(SKU) - Expected:"+expSKU+" Actual:"+actSKU+".\r\n";&#xd;
  count++;&#xd;
 }&#xd;
 if(!expGroup.equals(actGroup) || actGroup.equals("")){&#xd;
  errorMsg += "Group - Expected:"+expGroup+" Actual:"+actGroup+".\r\n";&#xd;
  count++;&#xd;
 }&#xd;
 if(!expPackage.equals(actPackage) || actPackage.equals("")){&#xd;
  errorMsg += "Package - Expected:"+expPackage+" Actual:"+actPackage+".\r\n";&#xd;
  count++;&#xd;
 }&#xd;
 if(!expProductType.equals(actProductType) || actProductType.equals("")){&#xd;
  errorMsg += "ProductType - Expected:"+expProductType+" Actual:"+actProductType+".\r\n";&#xd;
  count++;&#xd;
 }&#xd;
 if(preAuthorizedIndicator.equals("")){&#xd;
  errorMsg += "PreAuthorizedIndicator is null.\r\n";&#xd;
  count++;&#xd;
 }&#xd;
 if(inventoryStatus.equals("AVAILABLE")){&#xd;
	 if(availableStartDate.equals(""))&#xd;
	 {&#xd;
		 errorMsg += "AvailableStartDate is null.\r\n";&#xd;
		 count++;&#xd;
	 }&#xd;
	 if(availableEndDate.equals(""))&#xd;
	 {&#xd;
		 errorMsg += "AvailableEndDate is null.\r\n";&#xd;
		 count++;&#xd;
	 }&#xd;
	 if(actAvailableQty.equals(""))&#xd;
	 {&#xd;
		 errorMsg += "AvailableQty is null.\r\n";&#xd;
		 count++;&#xd;
	 } &#xd;
 }&#xd;
}&#xd;
&#xd;
if(count&gt;0){&#xd;
    testExec.setStateValue("Test_Execution_Status", "Failed");&#xd;
    testExec.setStateValue("FailureDescription", errorMsg);&#xd;
}&#xd;
else{&#xd;
    testExec.setStateValue("Test_Execution_Status", "Passed");&#xd;
    testExec.setStateValue("FailureDescription", "");&#xd;
}</script>
    </Node>


    <Node log="" name="Wait for SPlunk" next="SplunkLogs Validations" quiet="false" think="500-1S" type="com.itko.lisa.test.UserScriptNode" uid="AA13E161CB6B11EA9D328AB111CBFB47" useFilters="true" version="1"> 


      <!-- Assertions -->
<CheckResult assertTrue="true" name="Skip Splunk Validation" type="com.itko.lisa.test.AssertByScript">
<log>Skip Splunk Validation if wait time is greater than 5 minutes</log>
<then>continue</then>
<valueToAssertKey/>
        <script>// This script should return a boolean result indicating the assertion is true or false&#xd;
if(counter&gt;5)&#xd;
{&#xd;
testExec.setStateValue("Test_Execution_Status","Failed");&#xd;
return true;&#xd;
}</script>
        <language>BeanShell</language>
        <copyprops>TestExecPropsAndSystemProps</copyprops>
</CheckResult>

<CheckResult assertTrue="false" name="Scripted Assertion" type="com.itko.lisa.test.AssertByScript">
<log>Assertion name: Scripted Assertion checks for: true is of type: Assert by Script Execution.</log>
<then>Subprocess ReportGenerator</then>
<valueToAssertKey/>
        <script>import com.itko.lisa.test.TestEvent;&#xd;
import java.text.*;&#xd;
&#xd;
if(testExec.getStateValue("AssertMessage").equals(""))&#xd;
{&#xd;
testExec.setStateValue("Test_Execution_Status", "Passed");&#xd;
testExec.setStateValue("Timing Log Location", testExec.getStateValue("Log Location"));&#xd;
testExec.setStateValue("Unique BODID", testExec.getStateValue("BODID"));&#xd;
if(testExec.getStateValue("Test_Script_Status")!= "Failed")&#xd;
{&#xd;
testExec.setStateValue("Test_Script_Status", "Passed");&#xd;
 }   &#xd;
return true;&#xd;
}&#xd;
else&#xd;
{&#xd;
testExec.setStateValue("Test_Execution_Status", "Failed");&#xd;
testExec.setStateValue("Timing Log Location", testExec.getStateValue("Log Location"));&#xd;
testExec.setStateValue("Unique BODID", testExec.getStateValue("BODID"));  &#xd;
testExec.setStateValue("Test_Script_Status", "Failed");&#xd;
testExec.raiseEvent(TestEvent.EVENT_TRANSFAILED,"Failure","Failure_Scripted_Assertion");&#xd;
return false;&#xd;
}</script>
        <language>BeanShell</language>
        <copyprops>TestExecPropsAndSystemProps</copyprops>
</CheckResult>

<onerror>continue</onerror>
<language>BeanShell</language>
<copyProps>TestExecProps</copyProps>
<script>Thread.sleep(10000);&#xd;
counter=counter+1;&#xd;
testExec.setStateValue("counter",counter);&#xd;
</script>
    </Node>


    <Node log="" name="SplunkLogs Validations" next="Convert XML to JSon - Handler Response" quiet="false" think="500-1S" type="com.itko.lisa.ws.rest.RESTNode" uid="AA13E162CB6B11EA9D328AB111CBFB47" useFilters="true" version="3"> 


      <!-- Filters -->
      <Filter type="com.itko.lisa.test.FilterSaveResponse">
        <valueToFilterKey>lisa.SplunkLogs Validations.rsp</valueToFilterKey>
      <prop>raw_xml</prop>
      </Filter>


      <!-- Assertions -->
<CheckResult assertTrue="true" name="Poll back" type="com.itko.lisa.test.AssertByScript">
<log>Assertion name: Scripted Assertion checks for: true is of type: Assert by Script Execution.</log>
<then>Wait for SPlunk</then>
<valueToAssertKey/>
        <script>// This script should return a boolean result indicating the assertion is true or false&#xd;
if(raw_xml.isEmpty()){&#xd;
 return true;&#xd;
}&#xd;
if(raw_xml.length() &lt; 100){&#xd;
return true;&#xd;
}&#xd;
else{&#xd;
return false;&#xd;
}</script>
        <language>BeanShell</language>
        <copyprops>TestExecPropsAndSystemProps</copyprops>
</CheckResult>

<CheckResult assertTrue="false" name="Scripted Assertion" type="com.itko.lisa.test.AssertByScript">
<log>Assertion name: Scripted Assertion checks for: true is of type: Assert by Script Execution.</log>
<then>Subprocess ReportGenerator</then>
<valueToAssertKey/>
        <script>import com.itko.lisa.test.TestEvent;&#xd;
import java.text.*;&#xd;
&#xd;
if(testExec.getStateValue("AssertMessage").equals(""))&#xd;
{&#xd;
testExec.setStateValue("Test_Execution_Status", "Passed");&#xd;
testExec.setStateValue("Timing Log Location", testExec.getStateValue("Log Location"));&#xd;
testExec.setStateValue("Unique BODID", testExec.getStateValue("BODID"));&#xd;
if(testExec.getStateValue("Test_Script_Status")!= "Failed")&#xd;
{&#xd;
testExec.setStateValue("Test_Script_Status", "Passed");&#xd;
 }   &#xd;
return true;&#xd;
}&#xd;
else&#xd;
{&#xd;
testExec.setStateValue("Test_Execution_Status", "Failed");&#xd;
testExec.setStateValue("Timing Log Location", testExec.getStateValue("Log Location"));&#xd;
testExec.setStateValue("Unique BODID", testExec.getStateValue("BODID"));  &#xd;
testExec.setStateValue("Test_Script_Status", "Failed");&#xd;
testExec.raiseEvent(TestEvent.EVENT_TRANSFAILED,"Failure","Failure_Scripted_Assertion");&#xd;
return false;&#xd;
}</script>
        <language>BeanShell</language>
        <copyprops>TestExecPropsAndSystemProps</copyprops>
</CheckResult>

<url>https://{{SplunkHostname}}:{{splunkport}}/services/search/jobs/export?search=search+{{Query}}&amp;output_mode=json</url>
<data-type>text</data-type>
      <header field="Authorization" value="Basic U1ZDX0RFVl9PRmRldlRlc3Q6bX02TThrRiQtRTlzYTQqUXtUM3RMNWIrNyFQZHBSMi8/NWdTM3FaJT1LOGUyY0NfWCF5NH03R3pCbzZ7"/>
<httpMethod>GET</httpMethod>
<onError>continue</onError>
<encode-test-props-in-url>true</encode-test-props-in-url>
    </Node>


    <Node log="" name="Convert XML to JSon - Handler Response" next="Subprocess ReportGenerator" quiet="false" think="500-1S" type="com.itko.lisa.test.UserScriptNode" uid="AA13E163CB6B11EA9D328AB111CBFB47" useFilters="true" version="1"> 


      <!-- Assertions -->
<CheckResult assertTrue="true" name="Any Exception Then Fail" type="com.itko.lisa.dynexec.CheckInvocationEx">
<log>Assertion name: Any Exception Then Fail checks for: true is of type: Assert on Invocation Exception.</log>
<then>continue</then>
<valueToAssertKey/>
        <param>.*</param>
</CheckResult>

<CheckResult assertTrue="false" name="Scripted Assertion" type="com.itko.lisa.test.AssertByScript">
<log>Assertion name: Scripted Assertion checks for: true is of type: Assert by Script Execution.</log>
<then>Subprocess ReportGenerator</then>
<valueToAssertKey/>
        <script>import com.itko.lisa.test.TestEvent;&#xd;
import java.text.*;&#xd;
&#xd;
if(testExec.getStateValue("AssertMessage").equals(""))&#xd;
{&#xd;
testExec.setStateValue("Test_Execution_Status", "Passed");&#xd;
testExec.setStateValue("Timing Log Location", testExec.getStateValue("Log Location"));&#xd;
testExec.setStateValue("Unique BODID", testExec.getStateValue("BODID"));&#xd;
if(testExec.getStateValue("Test_Script_Status")!= "Failed")&#xd;
{&#xd;
testExec.setStateValue("Test_Script_Status", "Passed");&#xd;
 }   &#xd;
return true;&#xd;
}&#xd;
else&#xd;
{&#xd;
testExec.setStateValue("Test_Execution_Status", "Failed");&#xd;
testExec.setStateValue("Timing Log Location", testExec.getStateValue("Log Location"));&#xd;
testExec.setStateValue("Unique BODID", testExec.getStateValue("BODID"));  &#xd;
testExec.setStateValue("Test_Script_Status", "Failed");&#xd;
testExec.raiseEvent(TestEvent.EVENT_TRANSFAILED,"Failure","Failure_Scripted_Assertion");&#xd;
return false;&#xd;
}</script>
        <language>BeanShell</language>
        <copyprops>TestExecPropsAndSystemProps</copyprops>
</CheckResult>

<onerror>continue</onerror>
<language>BeanShell</language>
<copyProps>TestExecProps</copyProps>
<script>import java.util.regex.Matcher;&#xd;
import java.util.regex.Pattern;&#xd;
import org.json.JSONObject;&#xd;
import org.json.XML;&#xd;
		try {&#xd;
		String xml = "";&#xd;
        int PRETTY_PRINT_INDENT_FACTOR = 4;&#xd;
		String test = testExec.getStateValue("raw_xml");&#xd;
     &#xd;
        if(!test.isEmpty()){&#xd;
        Pattern pattern = Pattern.compile("(?&lt;=attachment=)(.*)(?=_serial)");&#xd;
		Matcher matcher = pattern.matcher(test);&#xd;
	   if (matcher.find())&#xd;
		{&#xd;
              &#xd;
			xml = matcher.group(1);&#xd;
&#xd;
		}&#xd;
		xml = xml.substring(0, xml.length() - 2);&#xd;
        xml = xml.replace("\\", "");&#xd;
        JSONObject xmlJSONObj = XML.toJSONObject(xml);&#xd;
        String json = xmlJSONObj.toString(PRETTY_PRINT_INDENT_FACTOR);&#xd;
        testExec.setStateValue("xml", xml);&#xd;
        testExec.setStateValue("json", json);&#xd;
        return json;&#xd;
        }&#xd;
        else{&#xd;
        //return null;&#xd;
        JSONObject xmlJSONObj = XML.toJSONObject(xml);&#xd;
        String json = xmlJSONObj.toString(PRETTY_PRINT_INDENT_FACTOR);&#xd;
        return json;&#xd;
        }&#xd;
		} catch (Exception e) {&#xd;
			// TODO Auto-generated catch block&#xd;
		}</script>
    </Node>


    <Node log="" name="Subprocess ReportGenerator" next="end" quiet="true" think="0H" type="com.itko.lisa.utils.ExecSubProcessNode" uid="AA13E164CB6B11EA9D328AB111CBFB47" useFilters="true" version="1"> 

<Subprocess>{{LISA_RELATIVE_PROJ_ROOT}}/Tests/Subprocesses/ReportGenerator.tst</Subprocess>
<fullyParseProps>false</fullyParseProps>
<sendCommonState>false</sendCommonState>
<getCommonState>false</getCommonState>
<onAbort>continue</onAbort>
<Parameters>
    <Parameter>
    <key>Test_Execution_Status</key>
    <value>{{Test_Execution_Status}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>TestCase_Description</key>
    <value>{{TestCase_Description}}</value>
    <name>Referenced 1st in ReportGenerator</name>
    </Parameter>
    <Parameter>
    <key>STEPNAME</key>
    <value>{{LISA_LAST_STEP}}</value>
    <name>Referenced 1st in ReportGenerator</name>
    </Parameter>
    <Parameter>
    <key>FailureDescription</key>
    <value>{{FailureDescription}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>ExecutionMode</key>
    <value>{{ExecutionMode}}</value>
    <name>Referenced 1st in ReportGenerator</name>
    </Parameter>
    <Parameter>
    <key>Comments</key>
    <value>MSISDN : {{MSISDN}} &lt;BR&gt; IMEI : {{imei}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>TC_Id</key>
    <value>{{TC_Id}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>Operation</key>
    <value>{{Operation}}</value>
    <name>Referenced 1st in Subprocess Historical_update</name>
    </Parameter>
    <Parameter>
    <key>EAR</key>
    <value>{{EAR}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
</Parameters>
<SaveProps>
</SaveProps>
    </Node>


    <Node log="" name="abort" next="" quiet="true" think="0h" type="com.itko.lisa.test.AbortStep" uid="AA13E165CB6B11EA9D328AB111CBFB47" useFilters="true" version="1"> 

    </Node>


    <Node log="" name="fail" next="abort" quiet="true" think="0h" type="com.itko.lisa.test.Abend" uid="AA13E166CB6B11EA9D328AB111CBFB47" useFilters="true" version="1"> 

    </Node>


    <Node log="" name="end" next="fail" quiet="true" think="0h" type="com.itko.lisa.test.NormalEnd" uid="AA13E167CB6B11EA9D328AB111CBFB47" useFilters="true" version="1"> 

    </Node>


    <DataSet atend="end" local="false" maxItemsToFetch="100" name="Read Scenarios" random="false" type="custom.dataset.ExcelDataFile">
<sample>rO0ABXNyABFqYXZhLnV0aWwuSGFzaE1hcAUH2sHDFmDRAwACRgAKbG9hZEZhY3RvckkACXRocmVzaG9sZHhwP0AAAAAAABh3CAAAACAAAAASdAALRGVzY3JpcHRpb250ACdWZXJpZnkgdGhlIGRldmljZSdzIGVsaWdpYmxlIGZvciB1bmxvY2t0AAZNU0lTRE50AAo0MDQyNzkyODQ3dAAKVENfRXhlY3V0ZXQAA1lFU3QADWRldmljZUJyYW5kSWR0AAhULU1vYmlsZXQADm92ZXJyaWRlUmVhc29udAANSW50ZXJuYXRpb25hbHQABGltc2l0AA8yMzQxMDU1NjM4ODg1NDl0AAZ1c2VySWR0AAhNS29tYWxhMnQAFVJlYWQgU2NlbmFyaW9zX1Jvd051bXQAATF0AAhzZW5kZXJJZHQACEVSSUNTU09OdAAMb3ZlcnJpZGVUeXBldAAJRlJPTlRMSU5FdAAFVENfSWR0AAVUQzAwMXQABGltZWl0AA8zNTU4NzM4NDc5MjgxMjJ0AAtSZXF1ZXN0RmlsZXQAF3VwZGF0ZVVubG9ja0VsaWdpYmlsaXR5dAACaWR0AAdFUklDMTIzdAAUVGVzdENhc2VfRGVzY3JpcHRpb250AC51cGRhdGVVbmxvY2tFbGlnaWJpbGl0eV9kZXZpY2VCcmFuZElkKFRNT0JJTEUpdAANYXBwbGljYXRpb25JZHQABEVDTk10AAljaGFubmVsSWR0AAZSRVRBSUx0AAl0aW1lc3RhbXB0ABMyMDE0LTA2LTI5VDEyOjAwOjAweA==</sample>
    <location>{{LISA_RELATIVE_PROJ_ROOT}}/Data/InputFiles/DeviceEnterprise.xlsx</location>
    <sheetname>manageBlackList</sheetname>
    <sRow>1</sRow>
    <eRow>2</eRow>
    </DataSet>

</TestCase>