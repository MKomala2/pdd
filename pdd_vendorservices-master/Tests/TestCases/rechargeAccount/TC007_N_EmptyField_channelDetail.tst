<?xml version="1.0" ?>

<TestCase name="TC007_N_EmptyField_channelDetail" version="5">

<meta>
   <create version="10.3.0" buildNumber="10.3.0.297" author="MMelagi1" date="08/02/2019" host="LP-5CD7521B4F" />
   <lastEdited version="10.3.0" buildNumber="10.3.0.297" author="MKomala2" date="07/23/2020" host="LP-5CD7355fj7" />
</meta>

<id>B102C147C28811EAA4478AB111CBFB47</id>
<IsInProject>true</IsInProject>
<sig>ZWQ9NSZ0Y3Y9NSZsaXNhdj0xMC4zLjAgKDEwLjMuMC4yOTcpJm5vZGVzPS03NDE4MjM1ODE=</sig>
<subprocess>false</subprocess>

<initState>
</initState>

<resultState>
</resultState>

<deletedProps>
</deletedProps>

      <Companion type="com.itko.lisa.test.EventSubprocessCompanion" >
<Events>
    <Parameter>
    <key>Step error</key>
    <value></value>
    </Parameter>
    <Parameter>
    <key>Abort</key>
    <value></value>
    </Parameter>
    <Parameter>
    <key>Cycle failed</key>
    <value></value>
    </Parameter>
</Events>
<EventComponents>
    <Parameter>
    <key>Event ID</key>
    <value>EVENT_ID</value>
    </Parameter>
    <Parameter>
    <key>Long Description</key>
    <value>LONG_DESCRIPTION</value>
    </Parameter>
    <Parameter>
    <key>Short Description</key>
    <value>SHORT_DESCRIPTION</value>
    </Parameter>
    <Parameter>
    <key>Step Name</key>
    <value>STEP_NAME</value>
    </Parameter>
    <Parameter>
    <key>Test Name</key>
    <value>TEST_NAME</value>
    </Parameter>
    <Parameter>
    <key>Timestamp</key>
    <value>TIMESTAMP</value>
    </Parameter>
</EventComponents>
<CompanionSubprocess>
<Subprocess>{{LISA_RELATIVE_PROJ_ROOT}}/Tests/Subprocesses/FailReportGenerator.tst</Subprocess>
<fullyParseProps>false</fullyParseProps>
<sendCommonState>false</sendCommonState>
<getCommonState>false</getCommonState>
<onAbort>abort</onAbort>
<Parameters>
    <Parameter>
    <key>Test_Execution_Status</key>
    <value>{{Test_Execution_Status}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>TestCase_Description</key>
    <value>{{TestCase_Description}}</value>
    <name>Referenced 1st in ReportGenerator</name>
    </Parameter>
    <Parameter>
    <key>STEPNAME</key>
    <value>{{STEPNAME}}</value>
    <name>Referenced 1st in ReportGenerator</name>
    </Parameter>
    <Parameter>
    <key>LISA_STEP</key>
    <value>{{LISA_LAST_STEP}}</value>
    <name>Referenced 1st in ReportGenerator</name>
    </Parameter>
    <Parameter>
    <key>FailureDescription</key>
    <value>{{FailureDescription}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>Operation</key>
    <value>{{Operation}}</value>
    <name>Referenced 1st in ReportGenerator</name>
    </Parameter>
    <Parameter>
    <key>Comments</key>
    <value>{{Comments}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>TC_Id</key>
    <value>{{TC_Id}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
</Parameters>
<SaveProps>
</SaveProps>
</CompanionSubprocess>
      </Companion>

    <Node name="ReadScenarios" log=""
          type="com.itko.lisa.test.NoTransNode" 
          version="1" 
          uid="B102C148C28811EAA4478AB111CBFB47" 
          think="0" 
          useFilters="true" 
          quiet="true" 
          next="Environment_Selection" > 


      <!-- Filters -->
      <Filter type="com.itko.lisa.test.TimeStampFilter">
        <valueToFilterKey>lisa.Do-Nothing Step.rsp</valueToFilterKey>
<pre>true</pre>
<post>false</post>
<datePattern>yyyy-MM-dd&apos;T&apos;HH:mm:ssXXX</datePattern>
<preprop>timestamp</preprop>
<postprop></postprop>
<Offset>0</Offset>
      </Filter>


      <!-- Data Sets -->
<readrec>Read Scenarios</readrec>

      <!-- Assertions -->
<CheckResult assertTrue="false" name="CustomListener" type="com.itko.lisa.test.AssertByScript">
<log>Assertion name: CustomListener checks for: false  is of type: Assert by Script Execution.</log>
<then>continue</then>
<valueToAssertKey></valueToAssertKey>
        <script>import com.itko.lisa.test.*;&#13;&#10;import java.text.*;&#13;&#10;&#13;&#10;testExec.setStateValue(&quot;AssertMessage&quot;,&quot;&quot;); &#13;&#10;TestEventListener custom_listener = new com.itko.lisa.test.TestEventListener()&#13;&#10;{&#13;&#10;public void testEvent(com.itko.lisa.test.TestEvent testevent)&#13;&#10;    {&#13;&#10;if(testevent.getEventId() == com.itko.lisa.test.TestEvent.EVENT_TRANSFAILED)&#13;&#10;        { &#13;&#10;testExec.setStateValue(&quot;AssertMessage&quot;,testevent.getLongDesc()); &#13;&#10;}&#13;&#10;}&#13;&#10;};&#13;&#10;&#13;&#10;testExec.setStateValue(&quot;Custom_Listener&quot;,custom_listener); &#13;&#10;testExec.getTestEventMgr().addListener(custom_listener,com.itko.lisa.test.TestEvent.noFilter); &#13;&#10;return true;&#13;&#10;</script>
        <language>BeanShell</language>
        <copyprops>TestExecPropsAndSystemProps</copyprops>
</CheckResult>

<CheckResult assertTrue="false" name="Check if test case execution required." type="com.itko.lisa.test.CheckResultPropRegEx">
<log>Assertion name: Ensure Property Matches Expression checks for: false is of type: Property Value Expression.</log>
<then>ReadScenarios</then>
<valueToAssertKey></valueToAssertKey>
        <prop>TC_Execute</prop>
        <param>YES</param>
</CheckResult>

    </Node>


    <Node name="Environment_Selection" log=""
          type="com.itko.lisa.utils.ExecSubProcessNode" 
          version="1" 
          uid="B102C149C28811EAA4478AB111CBFB47" 
          think="0H" 
          useFilters="true" 
          quiet="true" 
          next="TimeStamp" > 


      <!-- Assertions -->
<CheckResult assertTrue="false" name="Scripted Assertion" type="com.itko.lisa.test.AssertByScript">
<log>Assertion name: Scripted Assertion checks for: true is of type: Assert by Script Execution.</log>
<then>Subprocess ReportGenerator</then>
<valueToAssertKey></valueToAssertKey>
        <script>import com.itko.lisa.test.TestEvent;&#13;&#10;import java.text.*;&#13;&#10;&#13;&#10;if(testExec.getStateValue(&quot;AssertMessage&quot;).equals(&quot;&quot;))&#13;&#10;{&#13;&#10;testExec.setStateValue(&quot;Test_Execution_Status&quot;, &quot;Passed&quot;);&#13;&#10;testExec.setStateValue(&quot;Timing Log Location&quot;, testExec.getStateValue(&quot;Log Location&quot;));&#13;&#10;testExec.setStateValue(&quot;Unique BODID&quot;, testExec.getStateValue(&quot;BODID&quot;));&#13;&#10;if(testExec.getStateValue(&quot;Test_Script_Status&quot;)!= &quot;Failed&quot;)&#13;&#10;{&#13;&#10;testExec.setStateValue(&quot;Test_Script_Status&quot;, &quot;Passed&quot;);&#13;&#10; }   &#13;&#10;return true;&#13;&#10;}&#13;&#10;else&#13;&#10;{&#13;&#10;testExec.setStateValue(&quot;Test_Execution_Status&quot;, &quot;Failed&quot;);&#13;&#10;testExec.setStateValue(&quot;Timing Log Location&quot;, testExec.getStateValue(&quot;Log Location&quot;));&#13;&#10;testExec.setStateValue(&quot;Unique BODID&quot;, testExec.getStateValue(&quot;BODID&quot;));  &#13;&#10;testExec.setStateValue(&quot;Test_Script_Status&quot;, &quot;Failed&quot;);&#13;&#10;testExec.raiseEvent(TestEvent.EVENT_TRANSFAILED,&quot;Failure&quot;,&quot;Failure_Scripted_Assertion&quot;);&#13;&#10;return false;&#13;&#10;}</script>
        <language>BeanShell</language>
        <copyprops>TestExecPropsAndSystemProps</copyprops>
</CheckResult>

<Subprocess>{{LISA_RELATIVE_PROJ_ROOT}}/Tests/Subprocesses/Environment_Selection.tst</Subprocess>
<fullyParseProps>false</fullyParseProps>
<sendCommonState>false</sendCommonState>
<getCommonState>false</getCommonState>
<onAbort>continue</onAbort>
<Parameters>
    <Parameter>
    <key>EnvironmentTested</key>
    <value>{{EnvironmentTested}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
</Parameters>
<SaveProps>
<save>BW_Host</save>
<save>CE_Host</save>
</SaveProps>
    </Node>


    <Node name="TimeStamp" log=""
          type="com.itko.lisa.utils.ExecSubProcessNode" 
          version="1" 
          uid="B102C14AC28811EAA4478AB111CBFB47" 
          think="0H" 
          useFilters="true" 
          quiet="true" 
          next="Load Request" > 

<Subprocess>{{LISA_RELATIVE_PROJ_ROOT}}/Tests/Subprocesses/TimeStamp.tst</Subprocess>
<fullyParseProps>false</fullyParseProps>
<sendCommonState>false</sendCommonState>
<getCommonState>false</getCommonState>
<onAbort>continue</onAbort>
<Parameters>
</Parameters>
<SaveProps>
<save>DATE</save>
<save>TIMESTAMP</save>
</SaveProps>
    </Node>


    <Node name="Load Request" log=""
          type="com.itko.lisa.test.FileNode" 
          version="1" 
          uid="B102C14BC28811EAA4478AB111CBFB47" 
          think="500-1S" 
          useFilters="true" 
          quiet="true" 
          next="Parse Request" > 


      <!-- Assertions -->
<CheckResult assertTrue="false" name="Scripted Assertion" type="com.itko.lisa.test.AssertByScript">
<log>Assertion name: Scripted Assertion checks for: true is of type: Assert by Script Execution.</log>
<then>Subprocess ReportGenerator</then>
<valueToAssertKey></valueToAssertKey>
        <script>import com.itko.lisa.test.TestEvent;&#13;&#10;import java.text.*;&#13;&#10;&#13;&#10;if(testExec.getStateValue(&quot;AssertMessage&quot;).equals(&quot;&quot;))&#13;&#10;{&#13;&#10;testExec.setStateValue(&quot;Test_Execution_Status&quot;, &quot;Passed&quot;);&#13;&#10;testExec.setStateValue(&quot;Timing Log Location&quot;, testExec.getStateValue(&quot;Log Location&quot;));&#13;&#10;testExec.setStateValue(&quot;Unique BODID&quot;, testExec.getStateValue(&quot;BODID&quot;));&#13;&#10;if(testExec.getStateValue(&quot;Test_Script_Status&quot;)!= &quot;Failed&quot;)&#13;&#10;{&#13;&#10;testExec.setStateValue(&quot;Test_Script_Status&quot;, &quot;Passed&quot;);&#13;&#10; }   &#13;&#10;return true;&#13;&#10;}&#13;&#10;else&#13;&#10;{&#13;&#10;testExec.setStateValue(&quot;Test_Execution_Status&quot;, &quot;Failed&quot;);&#13;&#10;testExec.setStateValue(&quot;Timing Log Location&quot;, testExec.getStateValue(&quot;Log Location&quot;));&#13;&#10;testExec.setStateValue(&quot;Unique BODID&quot;, testExec.getStateValue(&quot;BODID&quot;));  &#13;&#10;testExec.setStateValue(&quot;Test_Script_Status&quot;, &quot;Failed&quot;);&#13;&#10;testExec.raiseEvent(TestEvent.EVENT_TRANSFAILED,&quot;Failure&quot;,&quot;Failure_Scripted_Assertion&quot;);&#13;&#10;return false;&#13;&#10;}</script>
        <language>BeanShell</language>
        <copyprops>TestExecPropsAndSystemProps</copyprops>
</CheckResult>

<Loc>{{LISA_RELATIVE_PROJ_ROOT}}/Data/Requests/{{RequestFile}}</Loc>
<charset>DEFAULT</charset>
<PropKey>Request</PropKey>
<onFail>continue</onFail>
    </Node>


    <Node name="Parse Request" log=""
          type="com.itko.lisa.utils.ParseTextContentNode" 
          version="1" 
          uid="B102C14CC28811EAA4478AB111CBFB47" 
          think="500-1S" 
          useFilters="true" 
          quiet="true" 
          next="Send BW Request" > 


      <!-- Assertions -->
<CheckResult assertTrue="true" name="Check CE Execution Mode  flag" type="com.itko.lisa.test.CheckResultPropRegEx">
<log>Assertion name: Check CE flag checks for: true is of type: Property Value Expression.</log>
<then>Send CE Request</then>
<valueToAssertKey></valueToAssertKey>
        <prop>ExecutionMode</prop>
        <param>CE</param>
</CheckResult>

<CheckResult assertTrue="false" name="Scripted Assertion" type="com.itko.lisa.test.AssertByScript">
<log>Assertion name: Scripted Assertion checks for: true is of type: Assert by Script Execution.</log>
<then>Subprocess ReportGenerator</then>
<valueToAssertKey></valueToAssertKey>
        <script>import com.itko.lisa.test.TestEvent;&#13;&#10;import java.text.*;&#13;&#10;&#13;&#10;if(testExec.getStateValue(&quot;AssertMessage&quot;).equals(&quot;&quot;))&#13;&#10;{&#13;&#10;testExec.setStateValue(&quot;Test_Execution_Status&quot;, &quot;Passed&quot;);&#13;&#10;testExec.setStateValue(&quot;Timing Log Location&quot;, testExec.getStateValue(&quot;Log Location&quot;));&#13;&#10;testExec.setStateValue(&quot;Unique BODID&quot;, testExec.getStateValue(&quot;BODID&quot;));&#13;&#10;if(testExec.getStateValue(&quot;Test_Script_Status&quot;)!= &quot;Failed&quot;)&#13;&#10;{&#13;&#10;testExec.setStateValue(&quot;Test_Script_Status&quot;, &quot;Passed&quot;);&#13;&#10; }   &#13;&#10;return true;&#13;&#10;}&#13;&#10;else&#13;&#10;{&#13;&#10;testExec.setStateValue(&quot;Test_Execution_Status&quot;, &quot;Failed&quot;);&#13;&#10;testExec.setStateValue(&quot;Timing Log Location&quot;, testExec.getStateValue(&quot;Log Location&quot;));&#13;&#10;testExec.setStateValue(&quot;Unique BODID&quot;, testExec.getStateValue(&quot;BODID&quot;));  &#13;&#10;testExec.setStateValue(&quot;Test_Script_Status&quot;, &quot;Failed&quot;);&#13;&#10;testExec.raiseEvent(TestEvent.EVENT_TRANSFAILED,&quot;Failure&quot;,&quot;Failure_Scripted_Assertion&quot;);&#13;&#10;return false;&#13;&#10;}</script>
        <language>BeanShell</language>
        <copyprops>TestExecPropsAndSystemProps</copyprops>
</CheckResult>

<text>e3tSZXF1ZXN0fX0=</text>
<propKey>Request</propKey>
    </Node>


    <Node name="Send BW Request" log=""
          type="com.itko.lisa.ws.nx.NxWSStep" 
          version="1" 
          uid="B102C14DC28811EAA4478AB111CBFB47" 
          think="500-1S" 
          useFilters="true" 
          quiet="false" 
          next="Send CE Request" > 


      <!-- Filters -->
      <Filter type="com.itko.lisa.test.FilterSaveResponse">
        <valueToFilterKey>lisa.Send BW Request.rsp</valueToFilterKey>
      <prop>BW_Response</prop>
      </Filter>

      <Filter type="com.itko.lisa.test.FilterSavePropToFile">
        <valueToFilterKey>lisa.Send BW Request.rsp</valueToFilterKey>
      <file>{{LOCDIR}}/Data/OutputFiles/{{TC_Id}}_{{TestCase_Description}}_BW.txt</file>
      <append>false</append>
      </Filter>

      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.Send BW Request.rsp</valueToFilterKey>
<prop>BW_ConfirmationCode</prop>
<xpathq>/*[local-name()=&apos;Envelope&apos;]/*[local-name()=&apos;Body&apos;]/*[local-name()=&apos;redeemVendorCouponResponse&apos;]/*[local-name()=&apos;confirmationInfo&apos;]/*[local-name()=&apos;code&apos;]/text()</xpathq>
      </Filter>

      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.Send BW Request.rsp</valueToFilterKey>
<prop>BW_CorrelationId</prop>
<xpathq>/*[local-name()=&apos;Envelope&apos;]/*[local-name()=&apos;Body&apos;]/*[local-name()=&apos;Fault&apos;]/*[local-name()=&apos;detail&apos;]/*[local-name()=&apos;errorInfo&apos;]/*[local-name()=&apos;correlationId&apos;]/text()</xpathq>
      </Filter>

      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.Send BW Request.rsp</valueToFilterKey>
<prop>BW_errorCode</prop>
<xpathq>/*[local-name()=&apos;Envelope&apos;]/*[local-name()=&apos;Body&apos;]/*[local-name()=&apos;Fault&apos;]/*[local-name()=&apos;detail&apos;]/*[local-name()=&apos;errorInfo&apos;]/*[local-name()=&apos;errorCode&apos;]/text()</xpathq>
      </Filter>

      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.Send BW Request.rsp</valueToFilterKey>
<prop>BW_errorMessage</prop>
<xpathq>/*[local-name()=&apos;Envelope&apos;]/*[local-name()=&apos;Body&apos;]/*[local-name()=&apos;Fault&apos;]/*[local-name()=&apos;detail&apos;]/*[local-name()=&apos;errorInfo&apos;]/*[local-name()=&apos;errorMessage&apos;]/text()</xpathq>
      </Filter>

      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.Send BW Request.rsp</valueToFilterKey>
<prop>BW_faultcode</prop>
<xpathq>/*[local-name()=&apos;Envelope&apos;]/*[local-name()=&apos;Body&apos;]/*[local-name()=&apos;Fault&apos;]/*[local-name()=&apos;faultcode&apos;]/text()</xpathq>
      </Filter>

      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.Send BW Request.rsp</valueToFilterKey>
<prop>BW_faultstring</prop>
<xpathq>/*[local-name()=&apos;Envelope&apos;]/*[local-name()=&apos;Body&apos;]/*[local-name()=&apos;Fault&apos;]/*[local-name()=&apos;faultstring&apos;]/text()</xpathq>
      </Filter>


      <!-- Assertions -->
<CheckResult assertTrue="false" name="Check ConfirmationCode" type="com.itko.lisa.test.AssertByScript">
<log>Assertion name: Check ConfirmationCode checks for: false is of type: Assert by Script Execution.</log>
<then>continue</then>
<valueToAssertKey></valueToAssertKey>
        <script>String confirmationCode = testExec.getStateValue(&quot;BW_ConfirmationCode&quot;);&#13;&#10;if(!confirmationCode.equals(&quot;&quot;)){&#13;&#10; testExec.setStateValue(&quot;BW_Code&quot;,&quot;100&quot;);&#13;&#10;}&#13;&#10;else{&#13;&#10;testExec.setStateValue(&quot;BW_Code&quot;,&quot;501&quot;);&#13;&#10;}&#13;&#10;&#13;&#10;return true;</script>
        <language>BeanShell</language>
        <copyprops>TestExecPropsAndSystemProps</copyprops>
</CheckResult>

<CheckResult assertTrue="true" name="Check BW Execution Mode  flag" type="com.itko.lisa.test.CheckResultPropRegEx">
<log>Assertion name: Check CE flag checks for: true is of type: Property Value Expression.</log>
<then>Validate Params</then>
<valueToAssertKey></valueToAssertKey>
        <prop>ExecutionMode</prop>
        <param>BW</param>
</CheckResult>

<CheckResult assertTrue="false" name="Scripted Assertion" type="com.itko.lisa.test.AssertByScript">
<log>Assertion name: Scripted Assertion checks for: true is of type: Assert by Script Execution.</log>
<then>Subprocess ReportGenerator</then>
<valueToAssertKey></valueToAssertKey>
        <script>import com.itko.lisa.test.TestEvent;&#13;&#10;import java.text.*;&#13;&#10;&#13;&#10;if(testExec.getStateValue(&quot;AssertMessage&quot;).equals(&quot;&quot;))&#13;&#10;{&#13;&#10;testExec.setStateValue(&quot;Test_Execution_Status&quot;, &quot;Passed&quot;);&#13;&#10;testExec.setStateValue(&quot;Timing Log Location&quot;, testExec.getStateValue(&quot;Log Location&quot;));&#13;&#10;testExec.setStateValue(&quot;Unique BODID&quot;, testExec.getStateValue(&quot;BODID&quot;));&#13;&#10;if(testExec.getStateValue(&quot;Test_Script_Status&quot;)!= &quot;Failed&quot;)&#13;&#10;{&#13;&#10;testExec.setStateValue(&quot;Test_Script_Status&quot;, &quot;Passed&quot;);&#13;&#10; }   &#13;&#10;return true;&#13;&#10;}&#13;&#10;else&#13;&#10;{&#13;&#10;testExec.setStateValue(&quot;Test_Execution_Status&quot;, &quot;Failed&quot;);&#13;&#10;testExec.setStateValue(&quot;Timing Log Location&quot;, testExec.getStateValue(&quot;Log Location&quot;));&#13;&#10;testExec.setStateValue(&quot;Unique BODID&quot;, testExec.getStateValue(&quot;BODID&quot;));  &#13;&#10;testExec.setStateValue(&quot;Test_Script_Status&quot;, &quot;Failed&quot;);&#13;&#10;testExec.raiseEvent(TestEvent.EVENT_TRANSFAILED,&quot;Failure&quot;,&quot;Failure_Scripted_Assertion&quot;);&#13;&#10;return false;&#13;&#10;}</script>
        <language>BeanShell</language>
        <copyprops>TestExecPropsAndSystemProps</copyprops>
</CheckResult>

<wsdl>{{LISA_RELATIVE_PROJ_URL}}/Data/WSDL/rpxVendorServices.wsdl</wsdl>
<endpoint>https://{{BW_Host}}/RevenueManagement/VendorServices/V1</endpoint>
<targetNamespace>http://www.tmobile.com/schema/rpx/vendorServices</targetNamespace>
<service>vendorServices</service>
<port>vendorServicesPort</port>
<operation>rechargeAccount</operation>
<onError>continue (quiet)</onError>
<maintainSession>true</maintainSession>
<clearSession>false</clearSession>
<request itko_enc="base64">e3tSZXF1ZXN0fX0=</request>
<style>document</style>
<use>literal</use>
<soapAction>urn:rechargeAccount</soapAction>
<sslInfo>
<ssl-keystore-password-enc>l27338be85ebc9603cfee1f206f9b13d6643174a2e32bae62333f7b304e5ad65d</ssl-keystore-password-enc>
<ssl-key-password-enc>l69fb6a66ede2e3aea4a08ace1d4dcadbbb19d37ed7459d76037d789d5bffc737</ssl-key-password-enc>
</sslInfo>
<wsiInfo>
<wsi-check-wsdl>false</wsi-check-wsdl>
<wsi-check-msg>false</wsi-check-msg>
<wsi-display-type>onlyFailed</wsi-display-type>
<wsi-on-fail>continue</wsi-on-fail>
</wsiInfo>
<transport>
<soapVersionUri>1.1</soapVersionUri>
<httpVersionUri>1.1</httpVersionUri>
<webMethodUri>POST</webMethodUri>
<mtom>false</mtom>
<dontSendRequest>false</dontSendRequest>
<fault-as-error>true</fault-as-error>
<callTimeout>30000</callTimeout>
</transport>
<uddiActive>false</uddiActive>
<uddi-result>
<uddi-selection>
</uddi-selection>
</uddi-result>
<wss4jInfo>
<version>5</version>
<wssecurity>
<isSend>true</isSend>
<must>false</must>
</wssecurity>
<wsi-bsp>false</wsi-bsp>
</wss4jInfo>
<wsaInfo>
<useAddressing>false</useAddressing>
<must>false</must>
<to></to>
<toOverride>false</toOverride>
<toDefault>true</toDefault>
<from></from>
<fromOverride>false</fromOverride>
<fromDefault>true</fromDefault>
<action></action>
<actionOverride>false</actionOverride>
<actionDefault>true</actionDefault>
<msgid></msgid>
<msgidOverride>false</msgidOverride>
<msgidDefault>true</msgidDefault>
<replyTo></replyTo>
<replyToOverride>false</replyToOverride>
<faultTo></faultTo>
<faulttToOverride>false</faulttToOverride>
<addressingVersion>http://www.w3.org/2005/08/addressing</addressingVersion>
</wsaInfo>
<customHTTPHeaderInfo>
</customHTTPHeaderInfo>
<attachments>
<attachment-type>MIME</attachment-type>
</attachments>
<mtom-paths>
</mtom-paths>
    </Node>


    <Node name="Send CE Request" log=""
          type="com.itko.lisa.ws.nx.NxWSStep" 
          version="1" 
          uid="B102C14EC28811EAA4478AB111CBFB47" 
          think="500-1S" 
          useFilters="true" 
          quiet="false" 
          next="Compare_BW_CE" > 


      <!-- Filters -->
      <Filter type="com.itko.lisa.test.FilterSavePropToFile">
        <valueToFilterKey>lisa.Send CE Request.rsp</valueToFilterKey>
      <file>{{LOCDIR}}/Data/OutputFiles/{{TC_Id}}_{{TestCase_Description}}_CE.txt</file>
      <append>false</append>
      </Filter>

      <Filter type="com.itko.lisa.test.FilterSaveResponse">
        <valueToFilterKey>lisa.Send CE Request.rsp</valueToFilterKey>
      <prop>CE_Response</prop>
      </Filter>

      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.Send CE Request.rsp</valueToFilterKey>
<prop>CE_CorrelationId</prop>
<xpathq>/*[local-name()=&apos;Envelope&apos;]/*[local-name()=&apos;Body&apos;]/*[local-name()=&apos;rechargeAccountResponse&apos;]/*[local-name()=&apos;correlationId&apos;]/text()</xpathq>
      </Filter>

      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.Send CE Request.rsp</valueToFilterKey>
<prop>CE_ConfirmationCode</prop>
<xpathq>/*[local-name()=&apos;Envelope&apos;]/*[local-name()=&apos;Body&apos;]/*[local-name()=&apos;rechargeAccountResponse&apos;]/*[local-name()=&apos;confirmationInfo&apos;]/*[local-name()=&apos;code&apos;]/text()</xpathq>
      </Filter>

      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.Send BW Request.rsp</valueToFilterKey>
<prop>BW_errorCode</prop>
<xpathq>/*[local-name()=&apos;Envelope&apos;]/*[local-name()=&apos;Body&apos;]/*[local-name()=&apos;Fault&apos;]/*[local-name()=&apos;detail&apos;]/*[local-name()=&apos;errorInfo&apos;]/*[local-name()=&apos;errorCode&apos;]/text()</xpathq>
      </Filter>

      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.Send BW Request.rsp</valueToFilterKey>
<prop>BW_errorMessage</prop>
<xpathq>/*[local-name()=&apos;Envelope&apos;]/*[local-name()=&apos;Body&apos;]/*[local-name()=&apos;Fault&apos;]/*[local-name()=&apos;detail&apos;]/*[local-name()=&apos;errorInfo&apos;]/*[local-name()=&apos;errorMessage&apos;]/text()</xpathq>
      </Filter>

      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.Send BW Request.rsp</valueToFilterKey>
<prop>BW_faultcode</prop>
<xpathq>/*[local-name()=&apos;Envelope&apos;]/*[local-name()=&apos;Body&apos;]/*[local-name()=&apos;Fault&apos;]/*[local-name()=&apos;faultcode&apos;]/text()</xpathq>
      </Filter>

      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.Send BW Request.rsp</valueToFilterKey>
<prop>BW_faultstring</prop>
<xpathq>/*[local-name()=&apos;Envelope&apos;]/*[local-name()=&apos;Body&apos;]/*[local-name()=&apos;Fault&apos;]/*[local-name()=&apos;faultstring&apos;]/text()</xpathq>
      </Filter>


      <!-- Assertions -->
<CheckResult assertTrue="false" name="Check ConfirmationCode" type="com.itko.lisa.test.AssertByScript">
<log>Assertion name: Check ConfirmationCode checks for: true is of type: Assert by Script Execution.</log>
<then>continue</then>
<valueToAssertKey></valueToAssertKey>
        <script>// This script should return a boolean result indicating the assertion is true or false&#13;&#10;&#13;&#10;String confirmationCode = testExec.getStateValue(&quot;CE_ConfirmationCode&quot;);&#13;&#10;if(!confirmationCode.equals(&quot;&quot;)){&#13;&#10; testExec.setStateValue(&quot;CE_Code&quot;,&quot;100&quot;);&#13;&#10;}&#13;&#10;else{&#13;&#10;testExec.setStateValue(&quot;CE_Code&quot;,&quot;501&quot;);&#13;&#10;}&#13;&#10;&#13;&#10;return true;</script>
        <language>BeanShell</language>
        <copyprops>TestExecPropsAndSystemProps</copyprops>
</CheckResult>

<CheckResult assertTrue="true" name="Check CE Execution Mode  flag" type="com.itko.lisa.test.CheckResultPropRegEx">
<log>Assertion name: Check CE flag checks for: true is of type: Property Value Expression.</log>
<then>Validate Params</then>
<valueToAssertKey></valueToAssertKey>
        <prop>ExecutionMode</prop>
        <param>CE</param>
</CheckResult>

<CheckResult assertTrue="false" name="Scripted Assertion" type="com.itko.lisa.test.AssertByScript">
<log>Assertion name: Scripted Assertion checks for: true is of type: Assert by Script Execution.</log>
<then>Subprocess ReportGenerator</then>
<valueToAssertKey></valueToAssertKey>
        <script>import com.itko.lisa.test.TestEvent;&#13;&#10;import java.text.*;&#13;&#10;&#13;&#10;if(testExec.getStateValue(&quot;AssertMessage&quot;).equals(&quot;&quot;))&#13;&#10;{&#13;&#10;testExec.setStateValue(&quot;Test_Execution_Status&quot;, &quot;Passed&quot;);&#13;&#10;testExec.setStateValue(&quot;Timing Log Location&quot;, testExec.getStateValue(&quot;Log Location&quot;));&#13;&#10;testExec.setStateValue(&quot;Unique BODID&quot;, testExec.getStateValue(&quot;BODID&quot;));&#13;&#10;if(testExec.getStateValue(&quot;Test_Script_Status&quot;)!= &quot;Failed&quot;)&#13;&#10;{&#13;&#10;testExec.setStateValue(&quot;Test_Script_Status&quot;, &quot;Passed&quot;);&#13;&#10; }   &#13;&#10;return true;&#13;&#10;}&#13;&#10;else&#13;&#10;{&#13;&#10;testExec.setStateValue(&quot;Test_Execution_Status&quot;, &quot;Failed&quot;);&#13;&#10;testExec.setStateValue(&quot;Timing Log Location&quot;, testExec.getStateValue(&quot;Log Location&quot;));&#13;&#10;testExec.setStateValue(&quot;Unique BODID&quot;, testExec.getStateValue(&quot;BODID&quot;));  &#13;&#10;testExec.setStateValue(&quot;Test_Script_Status&quot;, &quot;Failed&quot;);&#13;&#10;testExec.raiseEvent(TestEvent.EVENT_TRANSFAILED,&quot;Failure&quot;,&quot;Failure_Scripted_Assertion&quot;);&#13;&#10;return false;&#13;&#10;}</script>
        <language>BeanShell</language>
        <copyprops>TestExecPropsAndSystemProps</copyprops>
</CheckResult>

<wsdl>{{LISA_RELATIVE_PROJ_URL}}/Data/WSDL/rpxVendorServices.wsdl</wsdl>
<endpoint>https://{{CE_Host}}/RevenueManagement/VendorServices/V1</endpoint>
<targetNamespace>http://www.tmobile.com/schema/rpx/vendorServices</targetNamespace>
<service>vendorServices</service>
<port>vendorServicesPort</port>
<operation>getCouponReversal</operation>
<onError>continue (quiet)</onError>
<maintainSession>true</maintainSession>
<clearSession>false</clearSession>
<request itko_enc="base64">e3tSZXF1ZXN0fX0=</request>
<style>document</style>
<use>literal</use>
<soapAction>urn:getCouponReversal</soapAction>
<sslInfo>
<ssl-keystore-password-enc>l1f8d3c4f42ca224b7a5b270d04659ff815dabdef72f1e2b1ef0b8b60ae392eb4</ssl-keystore-password-enc>
<ssl-key-password-enc>la75e1be2de953d2de1cc815cd22e2fd40238522cac2c37341c903254b86d2b3e</ssl-key-password-enc>
</sslInfo>
<wsiInfo>
<wsi-check-wsdl>false</wsi-check-wsdl>
<wsi-check-msg>false</wsi-check-msg>
<wsi-display-type>onlyFailed</wsi-display-type>
<wsi-on-fail>continue</wsi-on-fail>
</wsiInfo>
<transport>
<soapVersionUri>1.1</soapVersionUri>
<httpVersionUri>1.1</httpVersionUri>
<webMethodUri>POST</webMethodUri>
<mtom>false</mtom>
<dontSendRequest>false</dontSendRequest>
<fault-as-error>true</fault-as-error>
<callTimeout>30000</callTimeout>
</transport>
<uddiActive>false</uddiActive>
<uddi-result>
<uddi-selection>
</uddi-selection>
</uddi-result>
<wss4jInfo>
<version>5</version>
<wssecurity>
<isSend>true</isSend>
<must>false</must>
</wssecurity>
<wsi-bsp>false</wsi-bsp>
</wss4jInfo>
<wsaInfo>
<useAddressing>false</useAddressing>
<must>false</must>
<to></to>
<toOverride>false</toOverride>
<toDefault>true</toDefault>
<from></from>
<fromOverride>false</fromOverride>
<fromDefault>true</fromDefault>
<action></action>
<actionOverride>false</actionOverride>
<actionDefault>true</actionDefault>
<msgid></msgid>
<msgidOverride>false</msgidOverride>
<msgidDefault>true</msgidDefault>
<replyTo></replyTo>
<replyToOverride>false</replyToOverride>
<faultTo></faultTo>
<faulttToOverride>false</faulttToOverride>
<addressingVersion>http://www.w3.org/2005/08/addressing</addressingVersion>
</wsaInfo>
<customHTTPHeaderInfo>
</customHTTPHeaderInfo>
<attachments>
<attachment-type>MIME</attachment-type>
</attachments>
<mtom-paths>
</mtom-paths>
    </Node>


    <Node name="Compare_BW_CE" log=""
          type="com.itko.lisa.utils.ExecSubProcessNode" 
          version="1" 
          uid="B102C14FC28811EAA4478AB111CBFB47" 
          think="0H" 
          useFilters="true" 
          quiet="true" 
          next="Validate Params" > 


      <!-- Assertions -->
<CheckResult assertTrue="false" name="check for Test_Execution_Status" type="com.itko.lisa.test.CheckResultPropRegEx">
<log>Assertion name: check for TestStatus checks for: false is of type: Property Value Expression.</log>
<then>continue</then>
<valueToAssertKey></valueToAssertKey>
        <prop>Test_Execution_Status</prop>
        <param>Passed</param>
</CheckResult>

<CheckResult assertTrue="false" name="Scripted Assertion" type="com.itko.lisa.test.AssertByScript">
<log>Assertion name: Scripted Assertion checks for: true is of type: Assert by Script Execution.</log>
<then>Subprocess ReportGenerator</then>
<valueToAssertKey></valueToAssertKey>
        <script>import com.itko.lisa.test.TestEvent;&#13;&#10;import java.text.*;&#13;&#10;&#13;&#10;if(testExec.getStateValue(&quot;AssertMessage&quot;).equals(&quot;&quot;))&#13;&#10;{&#13;&#10;testExec.setStateValue(&quot;Test_Execution_Status&quot;, &quot;Passed&quot;);&#13;&#10;testExec.setStateValue(&quot;Timing Log Location&quot;, testExec.getStateValue(&quot;Log Location&quot;));&#13;&#10;testExec.setStateValue(&quot;Unique BODID&quot;, testExec.getStateValue(&quot;BODID&quot;));&#13;&#10;if(testExec.getStateValue(&quot;Test_Script_Status&quot;)!= &quot;Failed&quot;)&#13;&#10;{&#13;&#10;testExec.setStateValue(&quot;Test_Script_Status&quot;, &quot;Passed&quot;);&#13;&#10; }   &#13;&#10;return true;&#13;&#10;}&#13;&#10;else&#13;&#10;{&#13;&#10;testExec.setStateValue(&quot;Test_Execution_Status&quot;, &quot;Failed&quot;);&#13;&#10;testExec.setStateValue(&quot;Timing Log Location&quot;, testExec.getStateValue(&quot;Log Location&quot;));&#13;&#10;testExec.setStateValue(&quot;Unique BODID&quot;, testExec.getStateValue(&quot;BODID&quot;));  &#13;&#10;testExec.setStateValue(&quot;Test_Script_Status&quot;, &quot;Failed&quot;);&#13;&#10;testExec.raiseEvent(TestEvent.EVENT_TRANSFAILED,&quot;Failure&quot;,&quot;Failure_Scripted_Assertion&quot;);&#13;&#10;return false;&#13;&#10;}</script>
        <language>BeanShell</language>
        <copyprops>TestExecPropsAndSystemProps</copyprops>
</CheckResult>

<Subprocess>{{LISA_RELATIVE_PROJ_ROOT}}/Tests/Subprocesses/Compare_BW_CE.tst</Subprocess>
<fullyParseProps>false</fullyParseProps>
<sendCommonState>false</sendCommonState>
<getCommonState>false</getCommonState>
<onAbort>continue</onAbort>
<Parameters>
    <Parameter>
    <key>CE_Response</key>
    <value>{{CE_Response}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>BW_Response</key>
    <value>{{BW_Response}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>TC_Id</key>
    <value>{{TC_Id}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>TestCase_Description</key>
    <value>{{TestCase_Description}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>Operation</key>
    <value>{{Operation}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>CE_Code</key>
    <value>{{CE_Code}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>BW_Code</key>
    <value>{{BW_Code}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>IgnoreAttributes</key>
    <value>providerId,keyValue</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
</Parameters>
<SaveProps>
<save>FailureDescription</save>
<save>Test_Execution_Status</save>
</SaveProps>
    </Node>


    <Node name="Validate Params" log=""
          type="com.itko.lisa.test.UserScriptNode" 
          version="1" 
          uid="B102C150C28811EAA4478AB111CBFB47" 
          think="500-1S" 
          useFilters="true" 
          quiet="false" 
          next="Subprocess ReportGenerator" > 


      <!-- Assertions -->
<CheckResult assertTrue="true" name="Any Exception Then Fail" type="com.itko.lisa.dynexec.CheckInvocationEx">
<log>Assertion name: Any Exception Then Fail checks for: true is of type: Assert on Invocation Exception.</log>
<then>continue</then>
<valueToAssertKey></valueToAssertKey>
        <param>.*</param>
</CheckResult>

<CheckResult assertTrue="false" name="check for Test_Execution_Status" type="com.itko.lisa.test.CheckResultPropRegEx">
<log>Assertion name: check for TestStatus checks for: false is of type: Property Value Expression.</log>
<then>continue</then>
<valueToAssertKey></valueToAssertKey>
        <prop>Test_Execution_Status</prop>
        <param>Passed</param>
</CheckResult>

<CheckResult assertTrue="false" name="Scripted Assertion" type="com.itko.lisa.test.AssertByScript">
<log>Assertion name: Scripted Assertion checks for: true is of type: Assert by Script Execution.</log>
<then>Subprocess ReportGenerator</then>
<valueToAssertKey></valueToAssertKey>
        <script>import com.itko.lisa.test.TestEvent;&#13;&#10;import java.text.*;&#13;&#10;&#13;&#10;if(testExec.getStateValue(&quot;AssertMessage&quot;).equals(&quot;&quot;))&#13;&#10;{&#13;&#10;testExec.setStateValue(&quot;Test_Execution_Status&quot;, &quot;Passed&quot;);&#13;&#10;testExec.setStateValue(&quot;Timing Log Location&quot;, testExec.getStateValue(&quot;Log Location&quot;));&#13;&#10;testExec.setStateValue(&quot;Unique BODID&quot;, testExec.getStateValue(&quot;BODID&quot;));&#13;&#10;if(testExec.getStateValue(&quot;Test_Script_Status&quot;)!= &quot;Failed&quot;)&#13;&#10;{&#13;&#10;testExec.setStateValue(&quot;Test_Script_Status&quot;, &quot;Passed&quot;);&#13;&#10; }   &#13;&#10;return true;&#13;&#10;}&#13;&#10;else&#13;&#10;{&#13;&#10;testExec.setStateValue(&quot;Test_Execution_Status&quot;, &quot;Failed&quot;);&#13;&#10;testExec.setStateValue(&quot;Timing Log Location&quot;, testExec.getStateValue(&quot;Log Location&quot;));&#13;&#10;testExec.setStateValue(&quot;Unique BODID&quot;, testExec.getStateValue(&quot;BODID&quot;));  &#13;&#10;testExec.setStateValue(&quot;Test_Script_Status&quot;, &quot;Failed&quot;);&#13;&#10;testExec.raiseEvent(TestEvent.EVENT_TRANSFAILED,&quot;Failure&quot;,&quot;Failure_Scripted_Assertion&quot;);&#13;&#10;return false;&#13;&#10;}</script>
        <language>BeanShell</language>
        <copyprops>TestExecPropsAndSystemProps</copyprops>
</CheckResult>

<onerror>continue</onerror>
<language>BeanShell</language>
<copyProps>TestExecProps</copyProps>
<script>String errorMsg=&quot;&quot;;&#13;&#10;int count = 0;&#13;&#10;String executionMode = testExec.getStateValue(&quot;ExecutionMode&quot;);&#13;&#10;String expCorrelationId = testExec.getStateValue(&quot;correlationId&quot;);&#13;&#10;String expRechargeAmountInCents = testExec.getStateValue(&quot;rechargeAmountInCents&quot;);&#13;&#10;String expMSISDN = testExec.getStateValue(&quot;msisdn&quot;);&#13;&#10;&#13;&#10;String confrimationCode=&quot;&quot;;&#13;&#10;String actCorrelationId = &quot;&quot;;&#13;&#10;String actRechargeAmountInCents = &quot;&quot;;&#13;&#10;String actRechargePromotedAmtInCents = &quot;&quot;;&#13;&#10;String actMSISDN = &quot;&quot;;&#13;&#10;String actPrepaidSubscriberId = &quot;&quot;;&#13;&#10;String subscriberType = &quot;&quot;;&#13;&#10;String totalBalInCents = &quot;&quot;;&#13;&#10;String balExpiryDate = &quot;&quot;;&#13;&#10;String periodicChargeStatus = &quot;&quot;;&#13;&#10;if(executionMode.equals(&quot;BW&quot;)){&#13;&#10;confrimationCode = testExec.getStateValue(&quot;BW_ConfirmationCode&quot;);&#13;&#10;actCorrelationId = testExec.getStateValue(&quot;BW_CorrelationId&quot;);&#13;&#10;actRechargeAmountInCents = testExec.getStateValue(&quot;BW_RechargeAmtInCents&quot;);&#13;&#10;actRechargePromotedAmtInCents = testExec.getStateValue(&quot;BW_RechargePromotedAmtInCents&quot;);&#13;&#10;actMSISDN = testExec.getStateValue(&quot;BW_MSISDN&quot;);&#13;&#10;actPrepaidSubscriberId = testExec.getStateValue(&quot;BW_PrepaidSubscriberId&quot;);&#13;&#10;subscriberType = testExec.getStateValue(&quot;BW_SubscriberType&quot;);&#13;&#10;totalBalInCents = testExec.getStateValue(&quot;BW_TotalBalInCents&quot;);&#13;&#10;balExpiryDate = testExec.getStateValue(&quot;BW_BalExpiryDate&quot;);&#13;&#10;periodicChargeStatus = testExec.getStateValue(&quot;BW_PeriodicChargeStatus&quot;);&#13;&#10;}&#13;&#10;else {&#13;&#10;confrimationCode = testExec.getStateValue(&quot;CE_ConfirmationCode&quot;);&#13;&#10;actCorrelationId = testExec.getStateValue(&quot;CE_CorrelationId&quot;);&#13;&#10;actRechargeAmountInCents = testExec.getStateValue(&quot;CE_RechargeAmtInCents&quot;);&#13;&#10;actRechargePromotedAmtInCents = testExec.getStateValue(&quot;CE_RechargePromotedAmtInCents&quot;);&#13;&#10;actMSISDN = testExec.getStateValue(&quot;CE_MSISDN&quot;);&#13;&#10;actPrepaidSubscriberId = testExec.getStateValue(&quot;CE_PrepaidSubscriberId&quot;);&#13;&#10;subscriberType = testExec.getStateValue(&quot;CE_SubscriberType&quot;);&#13;&#10;totalBalInCents = testExec.getStateValue(&quot;CE_TotalBalInCents&quot;);&#13;&#10;balExpiryDate = testExec.getStateValue(&quot;CE_BalExpiryDate&quot;);&#13;&#10;periodicChargeStatus = testExec.getStateValue(&quot;CE_PeriodicChargeStatus&quot;);&#13;&#10;}&#13;&#10;&#13;&#10;&#13;&#10;//Validation&#13;&#10;&#13;&#10;if(confrimationCode.equals(&quot;&quot;)){&#13;&#10; errorMsg += &quot;Confirmation Code is not returned.Actual Confirmation Code:&quot;+confrimationCode+&quot;.\r\n&quot;;&#13;&#10; count++;&#13;&#10;}&#13;&#10;else{&#13;&#10; if(!expCorrelationId.equals(actCorrelationId)){&#13;&#10;  errorMsg += &quot;CorrelationId - Expected:&quot;+expCorrelationId+&quot; Actual:&quot;+actCorrelationId+&quot;.\r\n&quot;;&#13;&#10;  count++;&#13;&#10; }&#13;&#10; if(!expRechargeAmountInCents.equals(actRechargeAmountInCents)){&#13;&#10;  errorMsg += &quot;RechargeAmountInCents - Expected:&quot;+expRechargeAmountInCents+&quot; Actual:&quot;+actRechargeAmountInCents+&quot;.\r\n&quot;;&#13;&#10;  count++;&#13;&#10; }&#13;&#10; if(!expRechargeAmountInCents.equals(actRechargePromotedAmtInCents)){&#13;&#10;  errorMsg += &quot;RechargePromotedAmountInCents - Expected:&quot;+expRechargeAmountInCents+&quot; Actual:&quot;+actRechargePromotedAmtInCents+&quot;.\r\n&quot;;&#13;&#10;  count++;&#13;&#10; }&#13;&#10; if(!expMSISDN.equals(actMSISDN)){&#13;&#10;  errorMsg += &quot;MSISDN - Expected:&quot;+expMSISDN+&quot; Actual:&quot;+actMSISDN+&quot;.\r\n&quot;;&#13;&#10;  count++;&#13;&#10; }&#13;&#10; if(!expMSISDN.equals(actPrepaidSubscriberId)){&#13;&#10;  errorMsg += &quot;Prepaid SubscriberID - Expected:&quot;+expMSISDN+&quot; Actual:&quot;+actPrepaidSubscriberId+&quot;.\r\n&quot;;&#13;&#10;  count++;&#13;&#10; }&#13;&#10; if(!subscriberType.equals(&quot;Prepaid&quot;)){&#13;&#10;  errorMsg += &quot;SubscriberType - Expected:Prepaid Actual:&quot;+subscriberType+&quot;.\r\n&quot;;&#13;&#10;  count++;&#13;&#10; }&#13;&#10; if(totalBalInCents.equals(&quot;&quot;)){&#13;&#10;  errorMsg += &quot;TotalBalanceInCents returned null.\r\n&quot;;&#13;&#10;  count++;&#13;&#10; }&#13;&#10; if(balExpiryDate.equals(&quot;&quot;)){&#13;&#10;  errorMsg += &quot;BalanceExpiryDate returned null.\r\n&quot;;&#13;&#10;  count++;&#13;&#10; }&#13;&#10; if(!periodicChargeStatus.equals(&quot;paid&quot;)){&#13;&#10;  errorMsg += &quot;PeriodicChargeStatus - Expected:paid Actual:&quot;+periodicChargeStatus+&quot;.\r\n&quot;;&#13;&#10;  count++;&#13;&#10; }&#13;&#10;}&#13;&#10;&#13;&#10;if(count&gt;0){&#13;&#10;    testExec.setStateValue(&quot;Test_Execution_Status&quot;, &quot;Failed&quot;);&#13;&#10;    testExec.setStateValue(&quot;FailureDescription&quot;, errorMsg);&#13;&#10;}&#13;&#10;else{&#13;&#10;    testExec.setStateValue(&quot;Test_Execution_Status&quot;, &quot;Passed&quot;);&#13;&#10;    testExec.setStateValue(&quot;FailureDescription&quot;, &quot;&quot;);&#13;&#10;}</script>
    </Node>


    <Node name="Subprocess ReportGenerator" log=""
          type="com.itko.lisa.utils.ExecSubProcessNode" 
          version="1" 
          uid="B102C151C28811EAA4478AB111CBFB47" 
          think="0H" 
          useFilters="true" 
          quiet="true" 
          next="ReadScenarios" > 

<Subprocess>{{LISA_RELATIVE_PROJ_ROOT}}/Tests/Subprocesses/ReportGenerator.tst</Subprocess>
<fullyParseProps>false</fullyParseProps>
<sendCommonState>false</sendCommonState>
<getCommonState>false</getCommonState>
<onAbort>continue</onAbort>
<Parameters>
    <Parameter>
    <key>Test_Execution_Status</key>
    <value>{{Test_Execution_Status}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>TestCase_Description</key>
    <value>{{TestCase_Description}}</value>
    <name>Referenced 1st in ReportGenerator</name>
    </Parameter>
    <Parameter>
    <key>STEPNAME</key>
    <value>{{LISA_LAST_STEP}}</value>
    <name>Referenced 1st in ReportGenerator</name>
    </Parameter>
    <Parameter>
    <key>FailureDescription</key>
    <value>{{FailureDescription}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>ExecutionMode</key>
    <value>{{ExecutionMode}}</value>
    <name>Referenced 1st in ReportGenerator</name>
    </Parameter>
    <Parameter>
    <key>Comments</key>
    <value>MSISDN : {{MSISDN}} &lt;BR&gt; IMEI : {{imei}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>TC_Id</key>
    <value>{{TC_Id}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>Operation</key>
    <value>{{Operation}}</value>
    <name>Referenced 1st in Subprocess Historical_update</name>
    </Parameter>
    <Parameter>
    <key>EAR</key>
    <value>{{EAR}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
</Parameters>
<SaveProps>
</SaveProps>
    </Node>


    <Node name="end" log=""
          type="com.itko.lisa.test.NormalEnd" 
          version="1" 
          uid="B102C152C28811EAA4478AB111CBFB47" 
          think="0h" 
          useFilters="true" 
          quiet="true" 
          next="fail" > 

    </Node>


    <Node name="fail" log=""
          type="com.itko.lisa.test.Abend" 
          version="1" 
          uid="B102C153C28811EAA4478AB111CBFB47" 
          think="0h" 
          useFilters="true" 
          quiet="true" 
          next="abort" > 

    </Node>


    <Node name="abort" log=""
          type="com.itko.lisa.test.AbortStep" 
          version="1" 
          uid="B102C154C28811EAA4478AB111CBFB47" 
          think="0h" 
          useFilters="true" 
          quiet="true" 
          next="" > 

    </Node>


    <DataSet type="custom.dataset.ExcelDataFile" name="Read Scenarios" atend="end" local="false" random="false" maxItemsToFetch="100" >
<sample>rO0ABXNyABFqYXZhLnV0aWwuSGFzaE1hcAUH2sHDFmDRAwACRgAKbG9hZEZhY3RvckkACXRocmVzaG9sZHhwP0AAAAAAABh3CAAAACAAAAASdAAKVENfRXhlY3V0ZXQAA1lFU3QACnBvc0JhdGNoSWR0AAEwdAAQcG9zVHJhbnNhY3Rpb25JZHQADDAwMDAxNDYzMzAwMnQACmVtcGxveWVlSWR0AAg4MDU2NjEwMXQACnRlcm1pbmFsSWR0AAY1NjYxMDF0AAh1c2VyTmFtZXQAEFBBWVNQT1RfRVBJTl9QQ1J0AAdzdG9yZUlkdAAMVk9YWFgwMDAwMDAwdAATZGVhbGVyVHJhbnNhY3Rpb25JZHEAfgAHdAAVUmVhZCBTY2VuYXJpb3NfUm93TnVtdAABMXQADWNoYW5uZWxEZXRhaWx0AARlYXB5dAALcmVxdWVzdE5hbWV0ABFnZXRDb3Vwb25SZXZlcnNhbHQACHBhc3N3b3JkdAAKUGFzc0B3b3JkMXQABVRDX0lkdAAFVEMwMDF0AAptZXJjaGFudElkdAAGNDIwMjI0dAALY2hhbm5lbE5hbWV0AAZWRU5ET1J0AAtSZXF1ZXN0RmlsZXEAfgAWdAAUVGVzdENhc2VfRGVzY3JpcHRpb250ABpQX1JldmVyc2VPcmlnaW5hbEdldGNvdXBvbnQADWRlYWxlckJhdGNoSWR0AAEweA==</sample>
    <location>C:/Users/mahaboob_s/Desktop/T-Mobile/U2_Project/Sprint 15/pdd_vendorservices-master/Data/InputFiles/DataSheet.xlsx</location>
    <sheetname>RechargeAccount</sheetname>
    <sRow>7</sRow>
    <eRow>8</eRow>
    </DataSet>

</TestCase>
