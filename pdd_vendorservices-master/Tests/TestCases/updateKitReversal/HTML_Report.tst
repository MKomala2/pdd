<?xml version="1.0" ?>

<TestCase name="HTML_Report" version="5">

<meta>
   <create version="8.2.0" buildNumber="8.2.0.244" author="spothed1" date="08/08/2017" host="LNAR-HXWTYF2" />
   <lastEdited version="10.3.0" buildNumber="10.3.0.297" author="MKomala2" date="07/07/2020" host="LP-5CD7355fj7" />
</meta>

<id>97185E8BB83711EAB5808AB111CBFB47</id>
<Documentation>Put documentation of the Test Case here.</Documentation>
<IsInProject>true</IsInProject>
<sig>ZWQ9NSZ0Y3Y9NSZsaXNhdj0xMC4zLjAgKDEwLjMuMC4yOTcpJm5vZGVzPTU4ODU2NDc4OQ==</sig>
<subprocess>false</subprocess>

<initState>
</initState>

<resultState>
</resultState>

<deletedProps>
    <key>TestSetId</key>
    <key>WSSERVER1</key>
    <key>WSSERVER2</key>
    <key>WSSERVER3</key>
    <key>Check</key>
    <key>ENDPOINT1</key>
    <key>ENDPOINT2</key>
    <key>ENDPOINT3</key>
    <key>lisa.Write_HTML_Header.rsp.time</key>
    <key>lisa.Write_HTML_Header.rsp</key>
    <key>EnvironmentTested</key>
    <key>WSPORT</key>
    <key>ENDPOINT</key>
    <key>WSPORT1</key>
    <key>WSPORT2</key>
    <key>WSPORT3</key>
    <key>WSSERVER</key>
</deletedProps>

    <Node name="Write_HTML_Header" log=""
          type="com.itko.lisa.test.UserScriptNode" 
          version="1" 
          uid="97185E8CB83711EAB5808AB111CBFB47" 
          think="500-1S" 
          useFilters="true" 
          quiet="false" 
          next="end" > 


      <!-- Filters -->
      <Filter type="com.itko.lisa.test.FilterSavePropToFile">
        <valueToFilterKey>lisa.Write_HTML_Header.rsp</valueToFilterKey>
      <file>{{LOCDIR}}/Data/OutputFiles/{{EAR}}_ExecutionResults_{{EnvironmentTested}}.html</file>
      <append>true</append>
      </Filter>

<onerror>continue</onerror>
<language>velocity</language>
<copyProps>TestExecProps</copyProps>
<script>&#9;&#9;&#9;&lt;html&gt;&#13;&#10;    &lt;body&gt;&#13;&#10;        &lt;left&gt; &#13;&#10;&lt;/center&gt;&#13;&#10;        &lt;table id=&quot;results&quot; border=2 align=center&gt;&#13;&#10;            &lt;tr&gt;&#13;&#10;&#9;&#9;&#9;&lt;th align=left colspan=&quot;9&quot; bgcolor=black&gt;&lt;font color = &apos;magenta&apos;&gt;Operation : RefundAccountReversal &lt;/th&gt;&#13;&#10;&#9;&#9;&#9;&lt;/tr&gt;&#13;&#10;&#9;&#9;&#9;&lt;tr&gt; &#13;&#10;                &lt;th bgcolor=black width=100&gt;&lt;font color = &apos;magenta&apos;&gt;TC_Id&lt;/th&gt;&#13;&#10;&#9;&#9;&#9;&#9;&lt;th bgcolor=black width=500&gt;&lt;font color = &apos;magenta&apos;&gt;TestCase_Description&lt;/th&gt;&#13;&#10;&#9;&#9;&#9;&#9;&lt;th bgcolor=black width=100&gt;&lt;font color = &apos;magenta&apos;&gt;Execution_MODE&lt;/th&gt;&#13;&#10;&#9;&#9;&#9;&#9;&lt;th bgcolor=black width=100&gt;&lt;font color = &apos;magenta&apos;&gt;Result&lt;/th&gt;&#13;&#10;&#9;&#9;&#9;&#9;&lt;th bgcolor=black width=200&gt;&lt;font color = &apos;magenta&apos;&gt;Failed_STEP&lt;/th&gt;&#13;&#10;&#9;&#9;&#9;&#9;&lt;th bgcolor=black width=500&gt;&lt;font color = &apos;magenta&apos;&gt;Failure Description&lt;/th&gt;&#13;&#10;                &lt;th bgcolor=black width=500&gt;&lt;font color = &apos;magenta&apos;&gt;Comments&lt;/th&gt;&#13;&#10;&#9;&#9;&#9;&lt;/tr&gt;</script>
    </Node>


    <Node name="end" log=""
          type="com.itko.lisa.test.NormalEnd" 
          version="1" 
          uid="97185E8DB83711EAB5808AB111CBFB47" 
          think="0h" 
          useFilters="true" 
          quiet="true" 
          next="fail" > 

    </Node>


    <Node name="fail" log=""
          type="com.itko.lisa.test.Abend" 
          version="1" 
          uid="97185E8EB83711EAB5808AB111CBFB47" 
          think="0h" 
          useFilters="true" 
          quiet="true" 
          next="abort" > 

    </Node>


    <Node name="abort" log=""
          type="com.itko.lisa.test.AbortStep" 
          version="1" 
          uid="97185E8FB83711EAB5808AB111CBFB47" 
          think="0h" 
          useFilters="true" 
          quiet="true" 
          next="" > 

    </Node>


</TestCase>
