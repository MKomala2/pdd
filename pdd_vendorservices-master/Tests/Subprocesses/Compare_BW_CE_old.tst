<?xml version="1.0" encoding="UTF-8"?>
<TestCase name="Compare_BW_CE_old" version="5">

<meta>
   <create author="asanka4" buildNumber="10.3.0.297" date="04/13/2020" host="LP-BW4VPF2" version="10.3.0"/>
   <lastEdited author="asanka4" buildNumber="10.3.0.297" date="05/22/2020" host="LP-BW4VPF2" version="10.3.0"/>
</meta>

<id>B0BB18C07D9211EA974618DBF2466751</id>
<Documentation>Put documentation of the Test Case here.</Documentation>
<IsInProject>true</IsInProject>
<sig>ZWQ9NSZ0Y3Y9NSZsaXNhdj0xMC4zLjAgKDEwLjMuMC4yOTcpJm5vZGVzPS0xNTg4MjQwNjEy</sig>
<subprocess>true</subprocess>

<initState>
    <Parameter>
    <key>CE_Response</key>
    <value>{{CE_Response}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>BW_Response</key>
    <value>{{BW_Response}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>TC_Id</key>
    <value>{{TC_Id}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>TestCase_Description</key>
    <value>{{TestCase_Description}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>Operation</key>
    <value>{{Operation}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>CE_Code</key>
    <value>{{CE_Code}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>BW_Code</key>
    <value>{{BW_Code}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>IgnoreAttributes</key>
    <value>{{IgnoreAttributes}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
</initState>

<resultState>
    <Parameter>
    <key>ExecutionMode</key>
    <value/>
    <name>Set in project.config configuration</name>
    </Parameter>
    <Parameter>
    <key>FailureDescription</key>
    <value/>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>LOCDIR</key>
    <value/>
    <name>Set in project.config configuration</name>
    </Parameter>
    <Parameter>
    <key>Test_Execution_Status</key>
    <value/>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
</resultState>

<deletedProps>
    <key>WSSERVER1</key>
    <key>STEPNAME</key>
    <key>EnvironmentTestedE2E</key>
    <key>ODCAppTestJenkinsJobPath</key>
    <key>lisa.Set Ignore Attributes.rsp.time</key>
    <key>lisa.Compare CE and BW.rsp.time</key>
    <key>ENDPOINT1</key>
    <key>ENDPOINT2</key>
    <key>LocalPath</key>
    <key>lisa.Set Ignore Attributes.rsp</key>
    <key>EnvironmentTested</key>
    <key>lisa.continue (quiet).rsp</key>
    <key>WSPORT</key>
    <key>lisa.Compare CE and BW.rsp</key>
    <key>Type</key>
    <key>WSPORT1</key>
    <key>ENDPOINT</key>
    <key>lisa.vse.execution.mode</key>
    <key>lisa.Execute script (JSR-223).rsp.time</key>
    <key>lisa.continue (quiet).rsp.time</key>
    <key>HandlerName</key>
    <key>ODCSmokeTestJenkinsJobPath</key>
    <key>lisa.Execute script (JSR-223).rsp</key>
    <key>WSSERVER</key>
</deletedProps>

    <Node log="" name="Set Ignore Attributes" next="Compare CE and BW" quiet="false" think="500-1S" type="com.itko.lisa.test.UserScriptNode" uid="E42524497F4D11EA960518DBF2466751" useFilters="true" version="1"> 


      <!-- Assertions -->
<CheckResult assertTrue="true" name="Any Exception Then Fail" type="com.itko.lisa.dynexec.CheckInvocationEx">
<log>Assertion name: Any Exception Then Fail checks for: true is of type: Assert on Invocation Exception.</log>
<then>fail</then>
<valueToAssertKey/>
        <param>.*</param>
</CheckResult>

<onerror>continue</onerror>
<language>BeanShell</language>
<copyProps>TestExecProps</copyProps>
<script>&#xd;
String ignoreAttributes = testExec.getStateValue("IgnoreAttributes");&#xd;
String [] ignoreAttributesArray = ignoreAttributes.split(",");&#xd;
testExec.setStateValue("IgnoreAttributes", ignoreAttributesArray);&#xd;
return true;</script>
    </Node>


    <Node log="" name="Compare CE and BW" next="end" quiet="false" think="500-1S" type="com.itko.lisa.test.UserScriptNode" uid="237D14177D9511EA974618DBF2466751" useFilters="true" version="1"> 


      <!-- Assertions -->
<CheckResult assertTrue="true" name="Any Exception Then Fail" type="com.itko.lisa.dynexec.CheckInvocationEx">
<log>Assertion name: Any Exception Then Fail checks for: true is of type: Assert on Invocation Exception.</log>
<then>fail</then>
<valueToAssertKey/>
        <param>.*</param>
</CheckResult>

<onerror>continue</onerror>
<language>BeanShell</language>
<copyProps>TestExecProps</copyProps>
<script>import java.io.BufferedReader;&#xd;
import java.io.FileInputStream;&#xd;
import java.io.FileNotFoundException;&#xd;
import java.io.IOException;&#xd;
import java.io.InputStreamReader;&#xd;
import java.util.List;&#xd;
import org.custommonkey.xmlunit.DetailedDiff;&#xd;
import org.custommonkey.xmlunit.Diff;&#xd;
import org.custommonkey.xmlunit.Difference;&#xd;
import org.custommonkey.xmlunit.XMLUnit;&#xd;
import org.xml.sax.SAXException;&#xd;
import javax.xml.parsers.DocumentBuilderFactory;&#xd;
import javax.xml.parsers.DocumentBuilder;&#xd;
import org.w3c.dom.Document;&#xd;
import java.io.StringReader;&#xd;
import org.xml.sax.InputSource;&#xd;
&#xd;
String errorMsg="";&#xd;
int count = 0;&#xd;
&#xd;
String ce_responsecode = testExec.getStateValue("CE_Code");&#xd;
String bw_responsecode = testExec.getStateValue("BW_Code");&#xd;
&#xd;
//Getting&#xd;
String [] ignoreAttributesArray = testExec.getStateValue("IgnoreAttributes");&#xd;
&#xd;
if(!ce_responsecode.equals(bw_responsecode)){&#xd;
 errorMsg +="CE Response code:"+ce_responsecode+" and BW Response code:"+bw_responsecode+".\r\n";&#xd;
 count++;&#xd;
}&#xd;
else{&#xd;
String cefilePath = "{{LOCDIR}}/Data/OutputFiles/{{TC_Id}}_{{TestCase_Description}}_CE.txt";&#xd;
String bwfilePath = "{{LOCDIR}}/Data/OutputFiles/{{TC_Id}}_{{TestCase_Description}}_BW.txt";&#xd;
&#xd;
&#xd;
FileInputStream fis_ce = new FileInputStream(cefilePath);&#xd;
FileInputStream fis_bw = new FileInputStream(bwfilePath);&#xd;
&#xd;
BufferedReader ceReader = new BufferedReader(new InputStreamReader(fis_ce));&#xd;
BufferedReader bwReader = new BufferedReader(new InputStreamReader(fis_bw));&#xd;
&#xd;
XMLUnit.setIgnoreWhitespace(true);&#xd;
&#xd;
Diff xmlDiff = new Diff(bwReader, ceReader);&#xd;
//Diff xmlDiff = new Diff(ceDocument, bwDocument);&#xd;
DetailedDiff detailXmlDiff = new DetailedDiff(xmlDiff);&#xd;
&#xd;
&#xd;
List differences = detailXmlDiff.getAllDifferences();&#xd;
&#xd;
//Add valid attributes to new list if present. &#xd;
List&lt;Difference&gt; validAttributesList = new ArrayList&lt;Difference&gt;();&#xd;
for(int i=0;i&lt;ignoreAttributesArray.length;i++){&#xd;
  for(Difference difference: differences){&#xd;
       String str = difference.toString();&#xd;
       if(str.contains(ignoreAttributesArray[i]) &amp;&amp; (str.contains("text value")|| str.contains("attribute value"))){&#xd;
         validAttributesList.add(difference);&#xd;
       }&#xd;
  }&#xd;
}&#xd;
&#xd;
// Ignore namespace errors&#xd;
for(Difference difference: differences){&#xd;
       String str = difference.toString();&#xd;
       if(str.contains("namespace prefix")){&#xd;
         validAttributesList.add(difference);&#xd;
       }&#xd;
  }&#xd;
&#xd;
//Remove Valid attributes from difference list&#xd;
for(int i=0;i&lt;validAttributesList.size();i++){&#xd;
   differences.remove(validAttributesList.get(i));&#xd;
}&#xd;
&#xd;
//Convert the remaining difference to error message&#xd;
for(Difference difference: differences){&#xd;
String str = difference.toString();&#xd;
int index1 = str.indexOf("comparing");&#xd;
int index2 = str.lastIndexOf(" at ");&#xd;
String err = str.substring(0, index1 - 3)+" at /"+str.substring(index2+3)+".\r\n";&#xd;
err= err.replaceAll("'", "\"");	&#xd;
errorMsg+= err;&#xd;
count++;&#xd;
}&#xd;
&#xd;
}&#xd;
if(count&gt;0){&#xd;
    testExec.setStateValue("Test_Execution_Status", "Failed");&#xd;
    testExec.setStateValue("FailureDescription", errorMsg);&#xd;
}&#xd;
else{&#xd;
    testExec.setStateValue("Test_Execution_Status", "Passed");&#xd;
    testExec.setStateValue("FailureDescription", "");&#xd;
}&#xd;
return true;</script>
    </Node>


    <Node log="" name="end" next="fail" quiet="true" think="0h" type="com.itko.lisa.test.NormalEnd" uid="B0BB18C67D9211EA974618DBF2466751" useFilters="true" version="1"> 

    </Node>


    <Node log="" name="fail" next="abort" quiet="true" think="0h" type="com.itko.lisa.test.Abend" uid="B0BB18C47D9211EA974618DBF2466751" useFilters="true" version="1"> 

    </Node>


    <Node log="" name="abort" next="" quiet="true" think="0h" type="com.itko.lisa.test.AbortStep" uid="B0BB18C27D9211EA974618DBF2466751" useFilters="true" version="1"> 

    </Node>


</TestCase>