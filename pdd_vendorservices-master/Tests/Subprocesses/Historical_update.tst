<?xml version="1.0" ?>

<TestCase name="Historical_update" version="5">

<meta>
   <create version="10.3.0" buildNumber="10.3.0.297" author="dsingh39" date="04/16/2020" host="LP-146LPQ2" />
   <lastEdited version="10.3.0" buildNumber="10.3.0.297" author="asanka4" date="04/16/2020" host="LP-BW4VPF2" />
</meta>

<id>FD0BB48D801611EA960518DBF2466751</id>
<Documentation>Put documentation of the Test Case here.</Documentation>
<IsInProject>true</IsInProject>
<sig>ZWQ9NSZ0Y3Y9NSZsaXNhdj0xMC4zLjAgKDEwLjMuMC4yOTcpJm5vZGVzPTIxMDg1MTExNjM=</sig>
<subprocess>true</subprocess>

<initState>
    <Parameter>
    <key>TC_Id</key>
    <value>{{TC_Id}}</value>
    <name>TESTCASEID</name>
    </Parameter>
    <Parameter>
    <key>TestCase_Description</key>
    <value>{{TestCase_Description}}</value>
    <name>TestCase_Description</name>
    </Parameter>
    <Parameter>
    <key>Operation</key>
    <value>{{Operation}}</value>
    <name>Operation</name>
    </Parameter>
    <Parameter>
    <key>EnvironmentTested</key>
    <value>{{EnvironmentTested}}</value>
    <name>EnvironmentTested</name>
    </Parameter>
    <Parameter>
    <key>Test_Execution_Status</key>
    <value>{{Test_Execution_Status}}</value>
    <name>Result</name>
    </Parameter>
    <Parameter>
    <key>STEPNAME</key>
    <value>{{STEPNAME}}</value>
    <name>Failed at step</name>
    </Parameter>
    <Parameter>
    <key>Comments</key>
    <value>{{Comments}}</value>
    <name>Comments</name>
    </Parameter>
    <Parameter>
    <key>FailureDescription</key>
    <value>{{FailureDescription}}</value>
    <name>FailureDescription</name>
    </Parameter>
</initState>

<resultState>
    <Parameter>
    <key>LOCDIR</key>
    <value></value>
    <name>Set in project.config configuration</name>
    </Parameter>
    <Parameter>
    <key>lisa.Insert_result.rsp</key>
    <value></value>
    <name>Set 1st in Insert_result</name>
    </Parameter>
    <Parameter>
    <key>lisa.Insert_result.rsp.time</key>
    <value></value>
    <name>Set 1st in Insert_result</name>
    </Parameter>
</resultState>

<deletedProps>
    <key>EnvironmentTestedE2E</key>
    <key>lisa.Frame_update_query.rsp.time</key>
    <key>LocalPath</key>
    <key>ExecutionMode</key>
    <key>lisa.Frame_update_query.rsp</key>
    <key>HandlerName</key>
</deletedProps>

    <Node name="Frame_update_query" log=""
          type="com.itko.lisa.test.TestNodeLogger" 
          version="1" 
          uid="FD0C9EEE801611EA960518DBF2466751" 
          think="500-1S" 
          useFilters="true" 
          quiet="true" 
          next="Insert_result" > 

<log>INSERT INTO AUTOMATIONRESULTS (&#13;&#10;&#9;&#9;TESTCASEID,&#13;&#10;&#9;&#9;SCENARIONAME,&#13;&#10;&#9;&#9;OPERATION,&#13;&#10;&#9;&#9;ENVIRONMENT,&#13;&#10;&#9;&#9;RESULT,&#13;&#10;&#9;&#9;FAILEDSTEP, &#13;&#10;&#9;&#9;COMMENTS,&#13;&#10;&#9;&#9;EXECUTIONTIMESTAMP,&#13;&#10;&#9;&#9;FAILUREDESCRIPTION)&#13;&#10;VALUES (&#13;&#10;&#9;&apos;{{TC_Id}}&apos;,&#13;&#10;&#9;&apos;{{TestCase_Description}}&apos;,&#13;&#10;&#9;&apos;{{Operation}}&apos;,&#13;&#10;&#9;&apos;{{EnvironmentTested}}&apos;,&#13;&#10;&#9;&apos;{{Test_Execution_Status}}&apos;,&#13;&#10;&#9;&apos;{{STEPNAME}}&apos;,&#13;&#10;&#9;&apos;{{Comments}}&apos;,&#13;&#10;&#9;SYSTIMESTAMP,&#13;&#10;&#9;&apos;{{FailureDescription}}&apos;&#13;&#10;)</log>
    </Node>


    <Node name="Insert_result" log=""
          type="com.itko.lisa.jdbc.JDBCNode" 
          version="1" 
          uid="FD0C9EEF801611EA960518DBF2466751" 
          think="500-1S" 
          useFilters="true" 
          quiet="false" 
          next="end" > 

<driver>oracle.jdbc.driver.OracleDriver</driver>
<dataSourceConnect>false</dataSourceConnect>
<jndiFactory></jndiFactory>
<jndiServerURL></jndiServerURL>
<jndiDataSourceName></jndiDataSourceName>
<connect>jdbc:oracle:thin:@itomt1.unix.gsm1900.org:4684:itomt1</connect>
<user>PPD_AUTOMATION</user>
<password_enc>l86183971ad30d38aa4c638cf7a0f6d86441d2ac87d4531d169ea5d2be89e854e32171246732644ed35d55d</password_enc>
<onSQLError>continue (quiet)</onSQLError>
<resultSet>false</resultSet>
<maxRows>-1</maxRows>
<keepOpen>false</keepOpen>
<usePool>true</usePool>
<sql>{{LASTRESPONSE}}</sql>
<IsStoredProc>false</IsStoredProc>
    </Node>


    <Node name="abort" log=""
          type="com.itko.lisa.test.AbortStep" 
          version="1" 
          uid="FD0C9EF2801611EA960518DBF2466751" 
          think="0h" 
          useFilters="true" 
          quiet="true" 
          next="" > 

    </Node>


    <Node name="fail" log=""
          type="com.itko.lisa.test.Abend" 
          version="1" 
          uid="FD0C9EF1801611EA960518DBF2466751" 
          think="0h" 
          useFilters="true" 
          quiet="true" 
          next="abort" > 

    </Node>


    <Node name="end" log=""
          type="com.itko.lisa.test.NormalEnd" 
          version="1" 
          uid="FD0C9EF0801611EA960518DBF2466751" 
          think="0h" 
          useFilters="true" 
          quiet="true" 
          next="fail" > 

    </Node>


</TestCase>
