<?xml version="1.0" ?>

<TestCase name="FailReportGenerator" version="5">

<meta>
   <create version="10.3.0" buildNumber="10.3.0.297" author="asanka4" date="04/08/2019" host="LP-BW4VPF2" />
   <lastEdited version="10.3.0" buildNumber="10.3.0.297" author="asanka4" date="04/12/2020" host="LP-BW4VPF2" />
</meta>

<id>B613C8CF7D1011EAB79318DBF2466751</id>
<Documentation>Put documentation of the Test Case here.</Documentation>
<IsInProject>true</IsInProject>
<sig>ZWQ9NSZ0Y3Y9NSZsaXNhdj0xMC4zLjAgKDEwLjMuMC4yOTcpJm5vZGVzPTEzOTQzNTQ1MTk=</sig>
<subprocess>true</subprocess>

<initState>
    <Parameter>
    <key>Test_Execution_Status</key>
    <value>{{Test_Execution_Status}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>TestCase_Description</key>
    <value>{{TestCase_Description}}</value>
    <name>Referenced 1st in ReportGenerator</name>
    </Parameter>
    <Parameter>
    <key>STEPNAME</key>
    <value>{{STEPNAME}}</value>
    <name>Referenced 1st in ReportGenerator</name>
    </Parameter>
    <Parameter>
    <key>LISA_STEP</key>
    <value>{{LISA_LAST_STEP}}</value>
    <name>Referenced 1st in ReportGenerator</name>
    </Parameter>
    <Parameter>
    <key>FailureDescription</key>
    <value>{{FailureDescription}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>Operation</key>
    <value>{{Operation}}</value>
    <name>Referenced 1st in ReportGenerator</name>
    </Parameter>
    <Parameter>
    <key>Comments</key>
    <value>{{Comments}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>TC_Id</key>
    <value>{{TC_Id}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
</initState>

<resultState>
    <Parameter>
    <key>ENDPOINT2</key>
    <value></value>
    <name>Set in project.config configuration</name>
    </Parameter>
</resultState>

<deletedProps>
    <key>lisa.Confirm Step Failed.rsp</key>
    <key>ENDPOINT1</key>
    <key>LocalPath</key>
    <key>lisa.ReportGenerator.rsp</key>
    <key>Result_Path</key>
    <key>lisa.ReportGenerator.rsp.time</key>
    <key>WSPORT1</key>
    <key>URIFlag</key>
    <key>lisa.Execute script (JSR-223).rsp</key>
    <key>WSSERVER</key>
    <key>WSSERVER1</key>
    <key>html_report_path</key>
    <key>WSSERVER2</key>
    <key>EnvironmentTestedE2E</key>
    <key>LOCDIR</key>
    <key>ODCAppTestJenkinsJobPath</key>
    <key>JenkinsJobPath4</key>
    <key>JenkinsJobPath5</key>
    <key>JenkinsJobPath2</key>
    <key>JenkinsJobPath3</key>
    <key>lisa.TestCase_Status_Color.rsp</key>
    <key>JenkinsJobPath1</key>
    <key>EnvironmentTested</key>
    <key>WSPORT</key>
    <key>Type</key>
    <key>Tdm_Env</key>
    <key>ENDPOINT</key>
    <key>lisa.TestCase_Status_Color.rsp.time</key>
    <key>lisa.vse.execution.mode</key>
    <key>lisa.Execute script (JSR-223).rsp.time</key>
    <key>HandlerName</key>
    <key>lisa.Confirm Step Failed.rsp.time</key>
    <key>ODCSmokeTestJenkinsJobPath</key>
</deletedProps>

    <Node name="TestCase_Status_Color" log=""
          type="com.itko.lisa.test.UserScriptNode" 
          version="1" 
          uid="B613C8D07D1011EAB79318DBF2466751" 
          think="500-1S" 
          useFilters="true" 
          quiet="false" 
          next="Confirm Step Failed" > 


      <!-- Filters -->
      <Filter type="com.itko.lisa.test.FilterProperty2Property">
        <valueToFilterKey>Error</valueToFilterKey>
      <toProp>Error_Reason</toProp>
      <pre>false</pre>
      <post>true</post>
      </Filter>

<onerror>continue (quiet)</onerror>
<language>BeanShell</language>
<copyProps>TestExecPropsAndSystemProps</copyProps>
<script>String Stat = testExec.getStateValue(&quot;Test_Execution_Status&quot;);&#13;&#10;testExec.log(&quot;&gt;&gt;&gt;&gt;&gt;&quot;,Stat);&#13;&#10;&#13;&#10;&#13;&#10;if (Stat.equals(&quot;Passed&quot;))&#13;&#10;{&#13;&#10;    testExec.setStateValue(&quot;color&quot;,&quot;GREEN&quot;);&#13;&#10;}&#13;&#10;else&#13;&#10;{  &#13;&#10;    testExec.setStateValue(&quot;color&quot;,&quot;RED&quot;);&#13;&#10;    &#13;&#10;}&#13;&#10;</script>
    </Node>


    <Node name="Confirm Step Failed" log=""
          type="com.itko.lisa.test.UserScriptNode" 
          version="1" 
          uid="B613C8D17D1011EAB79318DBF2466751" 
          think="500-1S" 
          useFilters="true" 
          quiet="false" 
          next="ReportGenerator" > 


      <!-- Assertions -->
<CheckResult assertTrue="true" name="Any Exception Then Fail" type="com.itko.lisa.dynexec.CheckInvocationEx">
<log>Assertion name: Any Exception Then Fail checks for: true is of type: Assert on Invocation Exception.</log>
<then>fail</then>
<valueToAssertKey></valueToAssertKey>
        <param>.*</param>
</CheckResult>

<onerror>abort</onerror>
<language>BeanShell</language>
<copyProps>TestExecProps</copyProps>
<script>String Stat = testExec.getStateValue(&quot;Test_Execution_Status&quot;);&#13;&#10;testExec.log(&quot;&gt;&gt;&gt;&gt;&gt;&quot;,Stat);&#13;&#10;String stepName = testExec.getStateValue(&quot;STEPNAME&quot;);&#13;&#10;String lisaLastStep = testExec.getStateValue(&quot;LISA_STEP&quot;);&#13;&#10;&#13;&#10;if (Stat.equals(&quot;Passed&quot;))&#13;&#10;{&#13;&#10;    testExec.setStateValue(&quot;StepName&quot;,stepName);&#13;&#10;}&#13;&#10;else&#13;&#10;{&#13;&#10;  if(lisaLastStep.contains(&quot;Validate&quot;))&#13;&#10;  {&#13;&#10;   testExec.setStateValue(&quot;StepName&quot;,stepName);&#13;&#10;  }&#13;&#10;  else{ &#13;&#10;   testExec.setStateValue(&quot;StepName&quot;,lisaLastStep);&#13;&#10;  }&#13;&#10;}&#13;&#10;</script>
    </Node>


    <Node name="ReportGenerator" log=""
          type="com.itko.lisa.test.UserScriptNode" 
          version="1" 
          uid="B613C8D27D1011EAB79318DBF2466751" 
          think="500-1S" 
          useFilters="true" 
          quiet="false" 
          next="end" > 


      <!-- Filters -->
      <Filter type="com.itko.lisa.test.FilterSavePropToFile">
        <valueToFilterKey>lisa.ReportGenerator.rsp</valueToFilterKey>
      <file>{{LOCDIR}}/Data/OutputFiles/FailExecutionResults_{{EnvironmentTested}}.html</file>
      <append>true</append>
      </Filter>

<onerror>continue (quiet)</onerror>
<language>velocity</language>
<copyProps>TestExecPropsAndSystemProps</copyProps>
<script>            &lt;tr&gt;&#13;&#10;                &lt;td width=100&gt;{{TC_Id}}&lt;/td&gt;&#13;&#10;&#9;&#9;&#9;&#9;&lt;td width=500&gt;{{TestCase_Description}}&lt;/td&gt;&#13;&#10;                &lt;td width=300&gt;{{Operation}}&lt;/td&gt;&#13;&#10;&#9;&#9;&#9;&#9;&lt;td bgcolor={{color}} width=100&gt;{{Test_Execution_Status}}&lt;/td&gt;&#13;&#10;&#9;&#9;&#9;&#9;&lt;td width=200&gt;{{StepName}}&lt;/td&gt;&#13;&#10;&#9;&#9;&#9;&#9;&lt;td width=500&gt;{{FailureDescription}}&lt;/td&gt;&#13;&#10;                &lt;td width=300&gt;{{Comments}}&lt;/td&gt;&#13;&#10;&#9;&#9;&#9;&lt;/tr&gt;&#13;&#10;</script>
    </Node>


    <Node name="end" log=""
          type="com.itko.lisa.test.NormalEnd" 
          version="1" 
          uid="B613C8D57D1011EAB79318DBF2466751" 
          think="0h" 
          useFilters="true" 
          quiet="true" 
          next="fail" > 

    </Node>


    <Node name="fail" log=""
          type="com.itko.lisa.test.Abend" 
          version="1" 
          uid="B613C8D47D1011EAB79318DBF2466751" 
          think="0h" 
          useFilters="true" 
          quiet="true" 
          next="abort" > 

    </Node>


    <Node name="abort" log=""
          type="com.itko.lisa.test.AbortStep" 
          version="1" 
          uid="B613C8D37D1011EAB79318DBF2466751" 
          think="0h" 
          useFilters="true" 
          quiet="true" 
          next="" > 

    </Node>


</TestCase>
