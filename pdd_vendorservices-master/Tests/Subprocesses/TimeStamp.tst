<?xml version="1.0" encoding="UTF-8"?>
<TestCase name="TimeStamp" version="5">

<meta>
   <create author="RKumar72" buildNumber="8.2.0.244" date="12/01/2017" host="LP-R901F8E4" version="8.2.0"/>
   <lastEdited author="dsingh39" buildNumber="10.2.4.153" date="04/05/2018" host="rabi-PC" version="10.2.4"/>
</meta>

<id>82C743CABBA011EAB25B8AB111CBFB47</id>
<Documentation>Put documentation of the Test Case here.</Documentation>
<IsInProject>true</IsInProject>
<sig>ZWQ9NSZ0Y3Y9NSZsaXNhdj0xMC4yLjQgKDEwLjIuNC4xNTMpJm5vZGVzPTExOTkzMjg5NTU=</sig>
<subprocess>true</subprocess>

<initState>
</initState>

<resultState>
    <Parameter>
    <key>DATE</key>
    <value/>
    <name>Set 1st in Generate Sysdate</name>
    </Parameter>
    <Parameter>
    <key>TIMESTAMP</key>
    <value/>
    <name>Set 1st in Generate TimeStamp</name>
    </Parameter>
</resultState>

<deletedProps>
    <key>html_report_path</key>
    <key>DataAccessPath</key>
    <key>lisa.Generate Sysdate.rsp.time</key>
    <key>csv_result_path</key>
    <key>-------------------------------------------------------------------------------------------------</key>
    <key>CREATE_ORDER_ENDPOINT</key>
    <key>USER</key>
    <key>lisa.Output Log Message.rsp</key>
    <key>EnvironmentTested</key>
    <key>lisa.Generate Sysdate.rsp</key>
    <key>Response</key>
    <key>WSPORT</key>
    <key>Result_Path</key>
    <key>lisa.Generate TimeStamp.rsp</key>
    <key>EMSServer</key>
    <key>DataAccessHostname</key>
    <key>lisa.Generate TimeStamp.rsp.time</key>
    <key>PWD</key>
    <key>HandlerName</key>
    <key>lisa.Output Log Message.rsp.time</key>
    <key>WSSERVER</key>
</deletedProps>

    <Node log="" name="Generate Sysdate" next="Generate TimeStamp" quiet="false" think="500-1S" type="com.itko.lisa.test.ScriptNode" uid="82C743CBBBA011EAB25B8AB111CBFB47" useFilters="true" version="1"> 


      <!-- Filters -->
      <Filter type="com.itko.lisa.test.FilterSaveResponse">
        <valueToFilterKey>lisa.Generate Sysdate.rsp</valueToFilterKey>
      <prop>DATE</prop>
      </Filter>


      <!-- Assertions -->
<CheckResult assertTrue="true" name="Any Exception Then Fail" type="com.itko.lisa.dynexec.CheckInvocationEx">
<log>Assertion name: Any Exception Then Fail checks for: true is of type: Assert on Invocation Exception.</log>
<then>continue</then>
<valueToAssertKey/>
        <param>.*</param>
</CheckResult>

<onerror>continue</onerror>
<script>import java.text.SimpleDateFormat;&#xd;
testExec.setStateValue("TimeStamp", new SimpleDateFormat("yyyy-MM-dd").format(new Date()));&#xd;
return testExec.getStateValue("TimeStamp");&#xd;
</script>
    </Node>


    <Node log="" name="Generate TimeStamp" next="Output Log Message" quiet="false" think="500-1S" type="com.itko.lisa.test.ScriptNode" uid="82C743CCBBA011EAB25B8AB111CBFB47" useFilters="true" version="1"> 


      <!-- Filters -->
      <Filter type="com.itko.lisa.test.FilterSaveResponse">
        <valueToFilterKey>lisa.Generate TimeStamp.rsp</valueToFilterKey>
      <prop>TIMESTAMP</prop>
      </Filter>


      <!-- Assertions -->
<CheckResult assertTrue="true" name="Any Exception Then Fail" type="com.itko.lisa.dynexec.CheckInvocationEx">
<log>Assertion name: Any Exception Then Fail checks for: true is of type: Assert on Invocation Exception.</log>
<then>continue</then>
<valueToAssertKey/>
        <param>.*</param>
</CheckResult>

<onerror>continue</onerror>
<script>import java.text.SimpleDateFormat;&#xd;
testExec.setStateValue("TimeStamp1", new SimpleDateFormat("HH:mm:ss").format(new Date()));&#xd;
return testExec.getStateValue("TimeStamp1");&#xd;
</script>
    </Node>


    <Node log="" name="Output Log Message" next="end" quiet="true" think="500-1S" type="com.itko.lisa.test.TestNodeLogger" uid="82C743CDBBA011EAB25B8AB111CBFB47" useFilters="true" version="1"> 

<log>DATE={{DATE}} &#xd;
TIMESTAMP={{TIMESTAMP}}</log>
    </Node>


    <Node log="" name="abort" next="" quiet="true" think="0h" type="com.itko.lisa.test.AbortStep" uid="82C743CEBBA011EAB25B8AB111CBFB47" useFilters="true" version="1"> 

    </Node>


    <Node log="" name="fail" next="abort" quiet="true" think="0h" type="com.itko.lisa.test.Abend" uid="82C743CFBBA011EAB25B8AB111CBFB47" useFilters="true" version="1"> 

    </Node>


    <Node log="" name="end" next="fail" quiet="true" think="0h" type="com.itko.lisa.test.NormalEnd" uid="82C743D0BBA011EAB25B8AB111CBFB47" useFilters="true" version="1"> 

    </Node>


</TestCase>