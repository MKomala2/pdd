<?xml version="1.0" ?>

<TestCase name="ReportGenerator" version="5">

<meta>
   <create version="10.3.0" buildNumber="10.3.0.297" author="asanka4" date="04/08/2019" host="LP-BW4VPF2" />
   <lastEdited version="10.3.0" buildNumber="10.3.0.297" author="asanka4" date="04/30/2020" host="LP-BW4VPF2" />
</meta>

<id>9D109DFBB94311E9B73D18DBF2466751</id>
<Documentation>Put documentation of the Test Case here.</Documentation>
<IsInProject>true</IsInProject>
<sig>ZWQ9NSZ0Y3Y9NSZsaXNhdj0xMC4zLjAgKDEwLjMuMC4yOTcpJm5vZGVzPTExNzY4NjYxODA=</sig>
<subprocess>true</subprocess>

<initState>
    <Parameter>
    <key>Test_Execution_Status</key>
    <value>{{Test_Execution_Status}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>TestCase_Description</key>
    <value>{{TestCase_Description}}</value>
    <name>Referenced 1st in ReportGenerator</name>
    </Parameter>
    <Parameter>
    <key>STEPNAME</key>
    <value>{{LISA_LAST_STEP}}</value>
    <name>Referenced 1st in ReportGenerator</name>
    </Parameter>
    <Parameter>
    <key>FailureDescription</key>
    <value>{{FailureDescription}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>ExecutionMode</key>
    <value>{{ExecutionMode}}</value>
    <name>Referenced 1st in ReportGenerator</name>
    </Parameter>
    <Parameter>
    <key>Comments</key>
    <value>{{Comments}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>TC_Id</key>
    <value>{{TC_Id}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>Operation</key>
    <value>{{Operation}}</value>
    <name>Referenced 1st in Subprocess Historical_update</name>
    </Parameter>
    <Parameter>
    <key>EAR</key>
    <value>{{EAR}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
</initState>

<resultState>
    <Parameter>
    <key>ENDPOINT2</key>
    <value></value>
    <name>Set in project.config configuration</name>
    </Parameter>
    <Parameter>
    <key>ExecutionMode</key>
    <value></value>
    <name>Set in project.config configuration</name>
    </Parameter>
    <Parameter>
    <key>lisa.Subprocess Historical_update.rsp</key>
    <value></value>
    <name>Set 1st in Subprocess Historical_update</name>
    </Parameter>
    <Parameter>
    <key>lisa.Subprocess Historical_update.rsp.time</key>
    <value></value>
    <name>Set 1st in Subprocess Historical_update</name>
    </Parameter>
</resultState>

<deletedProps>
    <key>lisa.Confirm Step Failed.rsp</key>
    <key>ENDPOINT1</key>
    <key>LocalPath</key>
    <key>lisa.ReportGenerator.rsp</key>
    <key>Result_Path</key>
    <key>lisa.ReportGenerator.rsp.time</key>
    <key>WSPORT1</key>
    <key>URIFlag</key>
    <key>lisa.Execute script (JSR-223).rsp</key>
    <key>WSSERVER</key>
    <key>WSSERVER1</key>
    <key>html_report_path</key>
    <key>WSSERVER2</key>
    <key>EnvironmentTestedE2E</key>
    <key>LOCDIR</key>
    <key>ODCAppTestJenkinsJobPath</key>
    <key>JenkinsJobPath4</key>
    <key>JenkinsJobPath5</key>
    <key>JenkinsJobPath2</key>
    <key>JenkinsJobPath3</key>
    <key>lisa.TestCase_Status_Color.rsp</key>
    <key>JenkinsJobPath1</key>
    <key>EnvironmentTested</key>
    <key>WSPORT</key>
    <key>Type</key>
    <key>Tdm_Env</key>
    <key>ENDPOINT</key>
    <key>lisa.TestCase_Status_Color.rsp.time</key>
    <key>lisa.vse.execution.mode</key>
    <key>lisa.Execute script (JSR-223).rsp.time</key>
    <key>HandlerName</key>
    <key>lisa.Confirm Step Failed.rsp.time</key>
    <key>ODCSmokeTestJenkinsJobPath</key>
</deletedProps>

    <Node name="TestCase_Status_Color" log=""
          type="com.itko.lisa.test.UserScriptNode" 
          version="1" 
          uid="9D109DFCB94311E9B73D18DBF2466751" 
          think="500-1S" 
          useFilters="true" 
          quiet="false" 
          next="Confirm Step Failed" > 


      <!-- Filters -->
      <Filter type="com.itko.lisa.test.FilterProperty2Property">
        <valueToFilterKey>Error</valueToFilterKey>
      <toProp>Error_Reason</toProp>
      <pre>false</pre>
      <post>true</post>
      </Filter>

<onerror>continue (quiet)</onerror>
<language>BeanShell</language>
<copyProps>TestExecPropsAndSystemProps</copyProps>
<script>String Stat = testExec.getStateValue(&quot;Test_Execution_Status&quot;);&#13;&#10;testExec.log(&quot;&gt;&gt;&gt;&gt;&gt;&quot;,Stat);&#13;&#10;&#13;&#10;&#13;&#10;if (Stat.equals(&quot;Passed&quot;))&#13;&#10;{&#13;&#10;    testExec.setStateValue(&quot;color&quot;,&quot;GREEN&quot;);&#13;&#10;}&#13;&#10;else&#13;&#10;{  &#13;&#10;    testExec.setStateValue(&quot;color&quot;,&quot;RED&quot;);&#13;&#10;    &#13;&#10;}&#13;&#10;</script>
    </Node>


    <Node name="Confirm Step Failed" log=""
          type="com.itko.lisa.test.UserScriptNode" 
          version="1" 
          uid="D5529211B99311E9B73D18DBF2466751" 
          think="500-1S" 
          useFilters="true" 
          quiet="false" 
          next="ReportGenerator" > 


      <!-- Assertions -->
<CheckResult assertTrue="true" name="Any Exception Then Fail" type="com.itko.lisa.dynexec.CheckInvocationEx">
<log>Assertion name: Any Exception Then Fail checks for: true is of type: Assert on Invocation Exception.</log>
<then>fail</then>
<valueToAssertKey></valueToAssertKey>
        <param>.*</param>
</CheckResult>

<onerror>abort</onerror>
<language>BeanShell</language>
<copyProps>TestExecProps</copyProps>
<script>String Stat = testExec.getStateValue(&quot;Test_Execution_Status&quot;);&#13;&#10;testExec.log(&quot;&gt;&gt;&gt;&gt;&gt;&quot;,Stat);&#13;&#10;String stepName = testExec.getStateValue(&quot;STEPNAME&quot;);&#13;&#10;&#13;&#10;testExec.log(&quot;Failed Step name&quot;,stepName);&#13;&#10;&#13;&#10;&#13;&#10;if (Stat.equals(&quot;Passed&quot;))&#13;&#10;{&#13;&#10;    testExec.setStateValue(&quot;StepName&quot;,&quot;&quot;);&#13;&#10;}&#13;&#10;else&#13;&#10;{&#13;&#10;    testExec.setStateValue(&quot;StepName&quot;,stepName);&#13;&#10;}&#13;&#10;&#13;&#10;</script>
    </Node>


    <Node name="ReportGenerator" log=""
          type="com.itko.lisa.test.UserScriptNode" 
          version="1" 
          uid="9D109DFDB94311E9B73D18DBF2466751" 
          think="500-1S" 
          useFilters="true" 
          quiet="false" 
          next="Subprocess Historical_update" > 


      <!-- Filters -->
      <Filter type="com.itko.lisa.test.FilterSavePropToFile">
        <valueToFilterKey>lisa.ReportGenerator.rsp</valueToFilterKey>
      <file>{{LOCDIR}}/Data/OutputFiles/{{EAR}}_ExecutionResults_{{EnvironmentTested}}.html</file>
      <append>true</append>
      </Filter>

<onerror>continue (quiet)</onerror>
<language>velocity</language>
<copyProps>TestExecPropsAndSystemProps</copyProps>
<script>            &lt;tr&gt;&#13;&#10;                &lt;td width=100 align=center&gt;{{TC_Id}}&lt;/td&gt;&#13;&#10;&#9;&#9;&#9;&#9;&lt;td width=500&gt;{{TestCase_Description}}&lt;/td&gt;&#13;&#10;                &lt;td width=100 align=center&gt;{{ExecutionMode}}&lt;/td&gt;&#13;&#10;&#9;&#9;&#9;&#9;&lt;td bgcolor={{color}} width=100 align=center&gt;{{Test_Execution_Status}}&lt;/td&gt;&#13;&#10;&#9;&#9;&#9;&#9;&lt;td width=200 align=center&gt;{{StepName}}&lt;/td&gt;&#13;&#10;&#9;&#9;&#9;&#9;&lt;td width=500&gt;{{FailureDescription}}&lt;/td&gt;&#13;&#10;                &lt;td width=500&gt;{{Comments}}&lt;/td&gt;&#13;&#10;&#9;&#9;&#9;&lt;/tr&gt;&#13;&#10;</script>
    </Node>


    <Node name="Subprocess Historical_update" log=""
          type="com.itko.lisa.utils.ExecSubProcessNode" 
          version="1" 
          uid="985867F801711EA960518DBF2466751" 
          think="0H" 
          useFilters="true" 
          quiet="true" 
          next="end" > 

<Subprocess>{{LISA_RELATIVE_PROJ_ROOT}}/Tests/Subprocesses/Historical_update.tst</Subprocess>
<fullyParseProps>false</fullyParseProps>
<sendCommonState>false</sendCommonState>
<getCommonState>false</getCommonState>
<onAbort>abort</onAbort>
<Parameters>
    <Parameter>
    <key>TC_Id</key>
    <value>{{TC_Id}}</value>
    <name>TESTCASEID</name>
    </Parameter>
    <Parameter>
    <key>TestCase_Description</key>
    <value>{{TestCase_Description}}</value>
    <name>TestCase_Description</name>
    </Parameter>
    <Parameter>
    <key>Operation</key>
    <value>{{Operation}}</value>
    <name>Operation</name>
    </Parameter>
    <Parameter>
    <key>EnvironmentTested</key>
    <value>{{EnvironmentTested}}</value>
    <name>EnvironmentTested</name>
    </Parameter>
    <Parameter>
    <key>Test_Execution_Status</key>
    <value>{{Test_Execution_Status}}</value>
    <name>Result</name>
    </Parameter>
    <Parameter>
    <key>STEPNAME</key>
    <value>{{StepName}}</value>
    <name>Failed at step</name>
    </Parameter>
    <Parameter>
    <key>Comments</key>
    <value>{{Comments}}</value>
    <name>Comments</name>
    </Parameter>
    <Parameter>
    <key>FailureDescription</key>
    <value>{{FailureDescription}}</value>
    <name>FailureDescription</name>
    </Parameter>
</Parameters>
<SaveProps>
</SaveProps>
    </Node>


    <Node name="end" log=""
          type="com.itko.lisa.test.NormalEnd" 
          version="1" 
          uid="9D109E00B94311E9B73D18DBF2466751" 
          think="0h" 
          useFilters="true" 
          quiet="true" 
          next="fail" > 

    </Node>


    <Node name="fail" log=""
          type="com.itko.lisa.test.Abend" 
          version="1" 
          uid="9D109DFFB94311E9B73D18DBF2466751" 
          think="0h" 
          useFilters="true" 
          quiet="true" 
          next="abort" > 

    </Node>


    <Node name="abort" log=""
          type="com.itko.lisa.test.AbortStep" 
          version="1" 
          uid="9D109DFEB94311E9B73D18DBF2466751" 
          think="0h" 
          useFilters="true" 
          quiet="true" 
          next="" > 

    </Node>


</TestCase>
