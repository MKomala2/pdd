<?xml version="1.0" ?>

<TestCase name="TDMAPI" version="5">

<meta>
   <create version="10.3.0" buildNumber="10.3.0.297" author="asanka4" date="04/20/2020" host="LP-BW4VPF2" />
   <lastEdited version="10.3.0" buildNumber="10.3.0.297" author="asanka4" date="04/20/2020" host="LP-BW4VPF2" />
</meta>

<id>36F668B1831D11EA862918DBF2466751</id>
<Documentation>Put documentation of the Test Case here.</Documentation>
<IsInProject>true</IsInProject>
<sig>ZWQ9NSZ0Y3Y9LTEmbGlzYXY9MTAuMy4wICgxMC4zLjAuMjk3KSZub2Rlcz0yMDg2NTk0NDUw</sig>
<subprocess>true</subprocess>

<initState>
    <Parameter>
    <key>TDMScenario</key>
    <value>{{TDMScenario}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>EnvironmentTested</key>
    <value>&lt;&lt; default value &gt;&gt;</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
</initState>

<resultState>
    <Parameter>
    <key>BAN</key>
    <value></value>
    <name>Set 1st in http GET</name>
    </Parameter>
    <Parameter>
    <key>CustomerId</key>
    <value></value>
    <name>Set 1st in http GET</name>
    </Parameter>
    <Parameter>
    <key>LineId</key>
    <value></value>
    <name>Set 1st in http GET</name>
    </Parameter>
    <Parameter>
    <key>MSISDN</key>
    <value></value>
    <name>Set 1st in http GET</name>
    </Parameter>
</resultState>

<deletedProps>
    <key>LOCDIR</key>
    <key>lisa.http GET.rsp.time</key>
    <key>lisa.http GET.rsp</key>
    <key>ExecutionMode</key>
    <key>LocalPath</key>
    <key>HandlerName</key>
</deletedProps>

    <Node name="http GET" log=""
          type="com.itko.lisa.ws.rest.RESTNode" 
          version="3" 
          uid="4ABBC079831D11EA862918DBF2466751" 
          think="500-1S" 
          useFilters="true" 
          quiet="false" 
          next="end" > 


      <!-- Filters -->
      <Filter type="com.ca.lisa.apptest.json.FilterJSONGet">
        <valueToFilterKey>lisa.http GET.rsp</valueToFilterKey>
      <jsonPath>$.dataset.data.record[0].BAN</jsonPath>
      <prop>BAN</prop>
      <lengthProp></lengthProp>
      </Filter>

      <Filter type="com.ca.lisa.apptest.json.FilterJSONGet">
        <valueToFilterKey>lisa.http GET.rsp</valueToFilterKey>
      <jsonPath>$.dataset.data.record[0].MSISDN</jsonPath>
      <prop>MSISDN</prop>
      <lengthProp></lengthProp>
      </Filter>

      <Filter type="com.ca.lisa.apptest.json.FilterJSONGet">
        <valueToFilterKey>lisa.http GET.rsp</valueToFilterKey>
      <jsonPath>$.dataset.data.record[0].CUSTOMER_ID</jsonPath>
      <prop>CustomerId</prop>
      <lengthProp></lengthProp>
      </Filter>

      <Filter type="com.ca.lisa.apptest.json.FilterJSONGet">
        <valueToFilterKey>lisa.http GET.rsp</valueToFilterKey>
      <jsonPath>$.dataset.data.record[0].LINE_ID</jsonPath>
      <prop>LineId</prop>
      <lengthProp></lengthProp>
      </Filter>

<url>https://tdm-ssp.apps.px-prd02.cf.t-mobile.com/u2data?scenario={{TDMScenario}}&amp;env={{EnvironmentTested}}&amp;form=JSON&amp;count=1</url>
<content-type></content-type>
<data-type>text</data-type>
<httpMethod>GET</httpMethod>
<onError>abort</onError>
<encode-test-props-in-url>true</encode-test-props-in-url>
    </Node>


    <Node name="end" log=""
          type="com.itko.lisa.test.NormalEnd" 
          version="1" 
          uid="36F668B7831D11EA862918DBF2466751" 
          think="0h" 
          useFilters="true" 
          quiet="true" 
          next="fail" > 

    </Node>


    <Node name="fail" log=""
          type="com.itko.lisa.test.Abend" 
          version="1" 
          uid="36F668B5831D11EA862918DBF2466751" 
          think="0h" 
          useFilters="true" 
          quiet="true" 
          next="abort" > 

    </Node>


    <Node name="abort" log=""
          type="com.itko.lisa.test.AbortStep" 
          version="1" 
          uid="36F668B3831D11EA862918DBF2466751" 
          think="0h" 
          useFilters="true" 
          quiet="true" 
          next="" > 

    </Node>


</TestCase>
