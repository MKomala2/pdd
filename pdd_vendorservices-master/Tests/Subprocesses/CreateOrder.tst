<?xml version="1.0" ?>

<TestCase name="CreateOrder" version="5">

<meta>
   <create version="10.3.0" buildNumber="10.3.0.297" author="asanka4" date="05/29/2020" host="LP-BW4VPF2" />
   <lastEdited version="10.3.0" buildNumber="10.3.0.297" author="asanka4" date="06/01/2020" host="LP-BW4VPF2" />
</meta>

<id>5AB55809A1C311EAB1C918DBF2466751</id>
<Documentation>Put documentation of the Test Case here.</Documentation>
<IsInProject>true</IsInProject>
<sig>ZWQ9NSZ0Y3Y9NSZsaXNhdj0xMC4zLjAgKDEwLjMuMC4yOTcpJm5vZGVzPS02Njc0NzgzMDc=</sig>
<subprocess>true</subprocess>

<initState>
    <Parameter>
    <key>applicationId</key>
    <value>{{applicationId}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>workflowId</key>
    <value>{{workflowId}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>activityId</key>
    <value>{{activityId}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>timestamp</key>
    <value>{{timestamp}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>paymentMethodCode</key>
    <value>{{paymentMethodCode}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>cardType</key>
    <value>{{cardType}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>cardNumber</key>
    <value>{{cardNumber}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>fullName</key>
    <value>{{fullName}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>cardExpiryDate</key>
    <value>{{cardExpiryDate}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>firstName</key>
    <value>{{firstName}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>lastName</key>
    <value>{{lastName}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>channelId</key>
    <value>{{channelId}}</value>
    <name>Referenced 1st in Subprocess GenerateCartId</name>
    </Parameter>
    <Parameter>
    <key>CE_Host</key>
    <value>{{CE_Host}}</value>
    <name>Referenced 1st in Send CreateOrderCheckout Request</name>
    </Parameter>
    <Parameter>
    <key>CreateOrder_RequestFile</key>
    <value>{{CreateOrder_RequestFile}}</value>
    <name>Referenced 1st in Load Request</name>
    </Parameter>
    <Parameter>
    <key>EnvironmentTested</key>
    <value>{{EnvironmentTested}}</value>
    <name>Referenced 1st in Subprocess GenerateCartId</name>
    </Parameter>
    <Parameter>
    <key>interactionId</key>
    <value>{{interactionId}}</value>
    <name>Referenced 1st in Subprocess GenerateCartId</name>
    </Parameter>
    <Parameter>
    <key>sessionId</key>
    <value>{{sessionId}}</value>
    <name>Referenced 1st in Subprocess GenerateCartId</name>
    </Parameter>
    <Parameter>
    <key>senderId</key>
    <value>{{senderId}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>Operation</key>
    <value>{{Operation}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
</initState>

<resultState>
    <Parameter>
    <key>AuthorizationID</key>
    <value></value>
    <name>Set 1st in Send CreateOrderCheckout Request</name>
    </Parameter>
    <Parameter>
    <key>CartID</key>
    <value></value>
    <name>Set 1st in Subprocess GenerateCartId</name>
    </Parameter>
    <Parameter>
    <key>Code</key>
    <value></value>
    <name>Set 1st in Send CreateOrderCheckout Request</name>
    </Parameter>
    <Parameter>
    <key>ExecutionMode</key>
    <value></value>
    <name>Set in project.config configuration</name>
    </Parameter>
    <Parameter>
    <key>FailureDescription</key>
    <value></value>
    <name>Set 1st in Subprocess GenerateCartId</name>
    </Parameter>
    <Parameter>
    <key>OrderStatus</key>
    <value></value>
    <name>Set 1st in Send CreateOrderCheckout Request</name>
    </Parameter>
    <Parameter>
    <key>Request</key>
    <value></value>
    <name>Set 1st in Parse Request</name>
    </Parameter>
    <Parameter>
    <key>Test_Execution_Status</key>
    <value></value>
    <name>Set 1st in Subprocess GenerateCartId</name>
    </Parameter>
    <Parameter>
    <key>applicationUserId</key>
    <value></value>
    <name>Set 1st in Subprocess GenerateCartId</name>
    </Parameter>
    <Parameter>
    <key>cartItemId</key>
    <value></value>
    <name>Set 1st in Subprocess GenerateCartId</name>
    </Parameter>
    <Parameter>
    <key>lisa.Set Data.rsp</key>
    <value></value>
    <name>Set 1st in Set Data</name>
    </Parameter>
    <Parameter>
    <key>lisa.Set Data.rsp.time</key>
    <value></value>
    <name>Set 1st in Set Data</name>
    </Parameter>
    <Parameter>
    <key>offerSubType</key>
    <value></value>
    <name>Set 1st in Subprocess GenerateCartId</name>
    </Parameter>
    <Parameter>
    <key>offerType</key>
    <value></value>
    <name>Set 1st in Subprocess GenerateCartId</name>
    </Parameter>
    <Parameter>
    <key>offeringVariantId</key>
    <value></value>
    <name>Set 1st in Subprocess GenerateCartId</name>
    </Parameter>
    <Parameter>
    <key>orderType</key>
    <value></value>
    <name>Set 1st in Subprocess GenerateCartId</name>
    </Parameter>
    <Parameter>
    <key>productOfferingId</key>
    <value></value>
    <name>Set 1st in Subprocess GenerateCartId</name>
    </Parameter>
    <Parameter>
    <key>productOfferingLabel</key>
    <value></value>
    <name>Set 1st in Subprocess GenerateCartId</name>
    </Parameter>
    <Parameter>
    <key>productSubType</key>
    <value></value>
    <name>Set 1st in Subprocess GenerateCartId</name>
    </Parameter>
    <Parameter>
    <key>productType</key>
    <value></value>
    <name>Set 1st in Subprocess GenerateCartId</name>
    </Parameter>
    <Parameter>
    <key>sku</key>
    <value></value>
    <name>Set 1st in Subprocess GenerateCartId</name>
    </Parameter>
    <Parameter>
    <key>tillNumber</key>
    <value></value>
    <name>Set 1st in Subprocess GenerateCartId</name>
    </Parameter>
    <Parameter>
    <key>totalChargeAmount</key>
    <value></value>
    <name>Set 1st in Subprocess GenerateCartId</name>
    </Parameter>
    <Parameter>
    <key>totalDueAmount</key>
    <value></value>
    <name>Set 1st in Subprocess GenerateCartId</name>
    </Parameter>
    <Parameter>
    <key>totalTaxAmount</key>
    <value></value>
    <name>Set 1st in Subprocess GenerateCartId</name>
    </Parameter>
</resultState>

<deletedProps>
    <key>lisa.Pass Step.rsp.time</key>
    <key>lisa.Subprocess GenerateCartId.rsp.time</key>
    <key>LOCDIR</key>
    <key>lisa.Send CreateOrderCheckout Request.rsp.time</key>
    <key>lisa.Send CreateOrderCheckout Request.rsp</key>
    <key>lisa.Fail Step.rsp</key>
    <key>lisa.Load Request.rsp.time</key>
    <key>LocalPath</key>
    <key>lisa.Pass Step.rsp</key>
    <key>Code</key>
    <key>lisa.Parse Request.rsp.time</key>
    <key>lisa.Set Context.rsp.time</key>
    <key>lisa.Subprocess GenerateCartId.rsp</key>
    <key>lisa.Fail Step.rsp.time</key>
    <key>OrderStatus</key>
    <key>paymentId</key>
    <key>lisa.Load Request.rsp</key>
    <key>HandlerName</key>
    <key>lisa.Parse Request.rsp</key>
    <key>lisa.Set Context.rsp</key>
</deletedProps>

    <Node name="Subprocess GenerateCartId" log=""
          type="com.itko.lisa.utils.ExecSubProcessNode" 
          version="1" 
          uid="64C6DC7DA1C311EAB1C918DBF2466751" 
          think="0H" 
          useFilters="true" 
          quiet="true" 
          next="Set Data" > 


      <!-- Assertions -->
<CheckResult assertTrue="false" name="check for Test_Execution_Status" type="com.itko.lisa.test.CheckResultPropRegEx">
<log>Assertion name: check for TestStatus checks for: false is of type: Property Value Expression.</log>
<then>Fail Step</then>
<valueToAssertKey></valueToAssertKey>
        <prop>Test_Execution_Status</prop>
        <param>Passed</param>
</CheckResult>

<Subprocess>{{LISA_RELATIVE_PROJ_ROOT}}/Tests/Subprocesses/GenerateCartId.tst</Subprocess>
<fullyParseProps>false</fullyParseProps>
<sendCommonState>false</sendCommonState>
<getCommonState>false</getCommonState>
<onAbort>abort</onAbort>
<Parameters>
    <Parameter>
    <key>EnvironmentTested</key>
    <value>{{EnvironmentTested}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>channelId</key>
    <value>{{channelId}}</value>
    <name>Referenced 1st in Load Request1</name>
    </Parameter>
    <Parameter>
    <key>sessionId</key>
    <value>{{sessionId}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>interactionId</key>
    <value>{{interactionId}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
</Parameters>
<SaveProps>
<save>CartID</save>
<save>FailureDescription</save>
<save>Test_Execution_Status</save>
<save>applicationUserId</save>
<save>cartItemId</save>
<save>offerSubType</save>
<save>offerType</save>
<save>offeringVariantId</save>
<save>orderType</save>
<save>productOfferingId</save>
<save>productOfferingLabel</save>
<save>productSubType</save>
<save>productType</save>
<save>sku</save>
<save>tillNumber</save>
<save>totalChargeAmount</save>
<save>totalDueAmount</save>
<save>totalTaxAmount</save>
</SaveProps>
    </Node>


    <Node name="Set Data" log=""
          type="com.itko.lisa.test.UserScriptNode" 
          version="1" 
          uid="D19532D9A1E511EAAF1518DBF2466751" 
          think="500-1S" 
          useFilters="true" 
          quiet="false" 
          next="Set Context" > 


      <!-- Data Sets -->
<readrec>paymentId</readrec>

      <!-- Assertions -->
<CheckResult assertTrue="true" name="Any Exception Then Fail" type="com.itko.lisa.dynexec.CheckInvocationEx">
<log>Assertion name: Any Exception Then Fail checks for: true is of type: Assert on Invocation Exception.</log>
<then>fail</then>
<valueToAssertKey></valueToAssertKey>
        <param>.*</param>
</CheckResult>

<onerror>Fail Step</onerror>
<language>BeanShell</language>
<copyProps>TestExecProps</copyProps>
<script>testExec.setStateValue(&quot;Context&quot;,&quot;CREATE_ORDER&quot;);&#13;&#10;&#13;&#10;String channel = testExec.getStateValue(&quot;channelId&quot;);&#13;&#10;String operation = testExec.getStateValue(&quot;Operation&quot;);&#13;&#10;&#13;&#10;if(operation.equals(&quot;UpdateOrderCheckout&quot;) &amp;&amp; channel.equals(&quot;RETAIL&quot;))&#13;&#10;{&#13;&#10;&#13;&#10;String amount = testExec.getStateValue(&quot;totalDueAmount&quot;);&#13;&#10;double totalAmount = Double.parseDouble(amount);&#13;&#10;&#13;&#10;double remainingAmount = 1.00;&#13;&#10;double partialAmount = totalAmount - remainingAmount;&#13;&#10;&#13;&#10;testExec.setStateValue(&quot;RemainingAmount&quot;, remainingAmount.toString());&#13;&#10;testExec.setStateValue(&quot;totalDueAmount&quot;, partialAmount.toString());&#13;&#10;&#13;&#10;}&#13;&#10;&#13;&#10;return true;&#13;&#10;</script>
    </Node>


    <Node name="Set Context" log=""
          type="com.itko.lisa.test.TestNodeLogger" 
          version="1" 
          uid="915B5606A1C611EAB1C918DBF2466751" 
          think="500-1S" 
          useFilters="true" 
          quiet="true" 
          next="Load Request" > 

<log>{{Context=&quot;CREATE_ORDER&quot;}}</log>
    </Node>


    <Node name="Load Request" log=""
          type="com.itko.lisa.test.FileNode" 
          version="1" 
          uid="84A3CA59A1C411EAB1C918DBF2466751" 
          think="500-1S" 
          useFilters="true" 
          quiet="true" 
          next="Parse Request" > 

<Loc>{{LISA_RELATIVE_PROJ_ROOT}}/Data/Requests/OrderCheckoutEnterprise/{{CreateOrder_RequestFile}}_Request.txt</Loc>
<charset>DEFAULT</charset>
<PropKey>Request</PropKey>
<onFail>Fail Step</onFail>
    </Node>


    <Node name="Parse Request" log=""
          type="com.itko.lisa.utils.ParseTextContentNode" 
          version="1" 
          uid="8BAE76DEA1C411EAB1C918DBF2466751" 
          think="500-1S" 
          useFilters="true" 
          quiet="true" 
          next="Send CreateOrderCheckout Request" > 

<text>e3tSZXF1ZXN0fX0=</text>
<propKey>Request</propKey>
    </Node>


    <Node name="Send CreateOrderCheckout Request" log=""
          type="com.itko.lisa.ws.nx.NxWSStep" 
          version="1" 
          uid="C5650613A1C611EAB1C918DBF2466751" 
          think="500-1S" 
          useFilters="true" 
          quiet="false" 
          next="Pass Step" > 


      <!-- Filters -->
      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.Send CreateOrderCheckout Request.rsp</valueToFilterKey>
<prop>Code</prop>
<xpathq>/*[local-name()=&apos;Envelope&apos;]/*[local-name()=&apos;Body&apos;]/*[local-name()=&apos;createOrderCheckoutResponse&apos;]/*[local-name()=&apos;responseStatus&apos;]/@code</xpathq>
      </Filter>

      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.Send CreateOrderCheckout Request.rsp</valueToFilterKey>
<prop>OrderStatus</prop>
<xpathq>/*[local-name()=&apos;Envelope&apos;]/*[local-name()=&apos;Body&apos;]/*[local-name()=&apos;createOrderCheckoutResponse&apos;]/*[local-name()=&apos;order&apos;]/*[local-name()=&apos;orderStatus&apos;]/*[local-name()=&apos;name&apos;]/text()</xpathq>
      </Filter>

      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.Send CreateOrderCheckout Request.rsp</valueToFilterKey>
<prop>AuthorizationID</prop>
<xpathq>/*[local-name()=&apos;Envelope&apos;]/*[local-name()=&apos;Body&apos;]/*[local-name()=&apos;createOrderCheckoutResponse&apos;]/*[local-name()=&apos;order&apos;]/*[local-name()=&apos;orderPayment&apos;]/*[local-name()=&apos;creditCardPayment&apos;]/*[local-name()=&apos;authorizationId&apos;]/text()</xpathq>
      </Filter>


      <!-- Assertions -->
<CheckResult assertTrue="false" name="Check Response Code and status" type="com.itko.lisa.test.AssertByScript">
<log>Assertion name: Check Response Code and status checks for: false is of type: Assert by Script Execution.</log>
<then>Fail Step</then>
<valueToAssertKey></valueToAssertKey>
        <script>// This script should return a boolean result indicating the assertion is true or false&#13;&#10;&#13;&#10;String paymentMethodCode = testExec.getStateValue(&quot;paymentMethodCode&quot;);&#13;&#10;String channel = testExec.getStateValue(&quot;channelId&quot;);&#13;&#10;&#13;&#10;String expResponseCode = &quot;100&quot;;&#13;&#10;String expOrderStatus = &quot;Cart Checked Out&quot;;&#13;&#10;String expOrderStatus_Retail = &quot;Created&quot;;&#13;&#10;&#13;&#10;String actResponseCode = testExec.getStateValue(&quot;Code&quot;);&#13;&#10;String actOrderStatus = testExec.getStateValue(&quot;OrderStatus&quot;);&#13;&#10;&#13;&#10;String errMsg = &quot;&quot;;&#13;&#10;int count = 0;&#13;&#10;&#13;&#10;&#13;&#10;if(!actResponseCode.equals(expResponseCode)){&#13;&#10; errorMsg += &quot;ResponseStatusCode Expected:&quot;+expResponseCode+&quot; Actual:&quot;+actResponseCode+&quot;.\r\n&quot;;&#13;&#10; count++;&#13;&#10;}&#13;&#10;else {&#13;&#10; if(!expOrderStatus.equals(actOrderStatus) &amp;&amp; (channelId.equals(&quot;CARE&quot;) || channelId.equals(&quot;WEB&quot;))){&#13;&#10;  errorMsg += &quot;OrderStatus - Expected:&quot;+expOrderStatus+&quot; Actual:&quot;+actOrderStatus+&quot;.\r\n&quot;;&#13;&#10;  count++;&#13;&#10; }&#13;&#10; if(channelId.equals(&quot;RETAIL&quot;)){&#13;&#10;  if(!expOrderStatus.equals(actOrderStatus) &amp;&amp; paymentMethodCode.equals(&quot;Cash&quot;)){&#13;&#10;  errorMsg += &quot;OrderStatus - Expected:&quot;+expOrderStatus+&quot; Actual:&quot;+actOrderStatus+&quot;.\r\n&quot;;&#13;&#10;  count++;&#13;&#10;  }&#13;&#10;  if(!expOrderStatus_Retail.equals(actOrderStatus) &amp;&amp; paymentMethodCode.equals(&quot;Credit&quot;)){&#13;&#10;  errorMsg += &quot;OrderStatus - Expected:&quot;+expOrderStatus_Retail+&quot; Actual:&quot;+actOrderStatus+&quot;.\r\n&quot;;&#13;&#10;  count++;&#13;&#10; }&#13;&#10; }&#13;&#10;}&#13;&#10;&#13;&#10;if(count&gt;0){&#13;&#10;    testExec.setStateValue(&quot;Test_Execution_Status&quot;, &quot;Failed&quot;);&#13;&#10;    testExec.setStateValue(&quot;FailureDescription&quot;, errorMsg);&#13;&#10;}&#13;&#10;else{&#13;&#10;    testExec.setStateValue(&quot;Test_Execution_Status&quot;, &quot;Passed&quot;);&#13;&#10;    testExec.setStateValue(&quot;FailureDescription&quot;, &quot;&quot;);&#13;&#10;}&#13;&#10;&#13;&#10;return true;</script>
        <language>BeanShell</language>
        <copyprops>TestExecPropsAndSystemProps</copyprops>
</CheckResult>

<CheckResult assertTrue="false" name="check for Test_Execution_Status" type="com.itko.lisa.test.CheckResultPropRegEx">
<log>Assertion name: check for TestStatus checks for: false is of type: Property Value Expression.</log>
<then>Fail Step</then>
<valueToAssertKey></valueToAssertKey>
        <prop>Test_Execution_Status</prop>
        <param>Passed</param>
</CheckResult>

<wsdl>{{LISA_RELATIVE_PROJ_URL}}/Data/WSDL/OrderCheckoutEnterprise.wsdl</wsdl>
<endpoint>https://enterpriseordercheckout-{{CE_Host}}/service/soap/OrderManagement/OrderCheckoutEnterprise/V1.0</endpoint>
<targetNamespace>http://services.tmobile.com/OrderManagement/OrderCheckoutEnterprise/V1.0</targetNamespace>
<service>OrderCheckoutEnterprise</service>
<port>OrderCheckoutEnterprise</port>
<operation>createOrderCheckout</operation>
<onError>Fail Step</onError>
<maintainSession>true</maintainSession>
<clearSession>false</clearSession>
<request itko_enc="base64">e3tSZXF1ZXN0fX0=</request>
<style>document</style>
<use>literal</use>
<soapAction>createOrderCheckout</soapAction>
<sslInfo>
<ssl-keystore-password-enc>l0a183a92b706bb7f99fa3296c5e7c1e52e2a5dcc91e2f0936a32dfa23afcc42d</ssl-keystore-password-enc>
<ssl-key-password-enc>l48f8c35b51b9da7374c702983eee8bbe245d023c2640aed4e5eb0b214449a652</ssl-key-password-enc>
</sslInfo>
<wsiInfo>
<wsi-check-wsdl>false</wsi-check-wsdl>
<wsi-check-msg>false</wsi-check-msg>
<wsi-display-type>onlyFailed</wsi-display-type>
<wsi-on-fail>continue</wsi-on-fail>
</wsiInfo>
<transport>
<soapVersionUri>1.1</soapVersionUri>
<httpVersionUri>1.1</httpVersionUri>
<webMethodUri>POST</webMethodUri>
<mtom>false</mtom>
<dontSendRequest>false</dontSendRequest>
<fault-as-error>true</fault-as-error>
<callTimeout>30000</callTimeout>
</transport>
<uddiActive>false</uddiActive>
<uddi-result>
<uddi-selection>
</uddi-selection>
</uddi-result>
<wss4jInfo>
<version>5</version>
<wssecurity>
<isSend>true</isSend>
<must>false</must>
</wssecurity>
<wsi-bsp>false</wsi-bsp>
</wss4jInfo>
<wsaInfo>
<useAddressing>false</useAddressing>
<must>false</must>
<to></to>
<toOverride>false</toOverride>
<toDefault>true</toDefault>
<from></from>
<fromOverride>false</fromOverride>
<fromDefault>true</fromDefault>
<action></action>
<actionOverride>false</actionOverride>
<actionDefault>true</actionDefault>
<msgid></msgid>
<msgidOverride>false</msgidOverride>
<msgidDefault>true</msgidDefault>
<replyTo></replyTo>
<replyToOverride>false</replyToOverride>
<faultTo></faultTo>
<faulttToOverride>false</faulttToOverride>
<addressingVersion>http://www.w3.org/2005/08/addressing</addressingVersion>
</wsaInfo>
<customHTTPHeaderInfo>
</customHTTPHeaderInfo>
<attachments>
<attachment-type>MIME</attachment-type>
</attachments>
<mtom-paths>
</mtom-paths>
    </Node>


    <Node name="Pass Step" log=""
          type="com.itko.lisa.test.UserScriptNode" 
          version="1" 
          uid="6EFCC1AEA1CC11EAAF1518DBF2466751" 
          think="500-1S" 
          useFilters="true" 
          quiet="false" 
          next="end" > 


      <!-- Assertions -->
<CheckResult assertTrue="true" name="Any Exception Then Fail" type="com.itko.lisa.dynexec.CheckInvocationEx">
<log>Assertion name: Any Exception Then Fail checks for: true is of type: Assert on Invocation Exception.</log>
<then>continue</then>
<valueToAssertKey></valueToAssertKey>
        <param>.*</param>
</CheckResult>

<onerror>continue</onerror>
<language>BeanShell</language>
<copyProps>TestExecProps</copyProps>
<script>&#13;&#10;testExec.setStateValue(&quot;FailureDescription&quot;,&quot;&quot;);&#13;&#10;testExec.setStateValue(&quot;Test_Execution_Status&quot;,&quot;Passed&quot;);&#13;&#10;&#13;&#10;if(operation.equals(&quot;UpdateOrderCheckout&quot;) &amp;&amp; channel.equals(&quot;RETAIL&quot;))&#13;&#10;{&#13;&#10;&#13;&#10;testExec.setStateValue(&quot;totalDueAmount&quot;, &quot;{{RemainingAmount}}&quot;);&#13;&#10;&#13;&#10;}&#13;&#10;&#13;&#10;return true;</script>
    </Node>


    <Node name="Fail Step" log=""
          type="com.itko.lisa.test.UserScriptNode" 
          version="1" 
          uid="4A85BBDBA1CC11EAAF1518DBF2466751" 
          think="500-1S" 
          useFilters="true" 
          quiet="false" 
          next="end" > 


      <!-- Assertions -->
<CheckResult assertTrue="true" name="Any Exception Then Fail" type="com.itko.lisa.dynexec.CheckInvocationEx">
<log>Assertion name: Any Exception Then Fail checks for: true is of type: Assert on Invocation Exception.</log>
<then>continue</then>
<valueToAssertKey></valueToAssertKey>
        <param>.*</param>
</CheckResult>

<onerror>continue</onerror>
<language>BeanShell</language>
<copyProps>TestExecProps</copyProps>
<script>&#13;&#10;String step = testExec.setStateValue(&quot;LISA_LAST_STEP&quot;);&#13;&#10;String failMsg = testExec.setStateValue(&quot;FailureDescription&quot;);&#13;&#10;&#13;&#10;if(step.equals(&quot;Subprocess GenerateCartId&quot;)){&#13;&#10;  testExec.setStateValue(&quot;FailureDescription&quot;,&quot;Cart Id is not generated correctly&quot;);&#13;&#10;  testExec.setStateValue(&quot;STEPNAME&quot;,&quot;Subprocess GenerateCartId&quot;);&#13;&#10;}&#13;&#10;else{&#13;&#10; if(failMsg.equals(&quot;&quot;)){&#13;&#10;    testExec.setStateValue(&quot;FailureDescription&quot;,&quot;Create Order Checkout failed&quot;);&#13;&#10; }&#13;&#10;}&#13;&#10;&#13;&#10;testExec.setStateValue(&quot;Test_Execution_Status&quot;,&quot;Failed&quot;);&#13;&#10;&#13;&#10;return true;</script>
    </Node>


    <Node name="abort" log=""
          type="com.itko.lisa.test.AbortStep" 
          version="1" 
          uid="5AB5580BA1C311EAB1C918DBF2466751" 
          think="0h" 
          useFilters="true" 
          quiet="true" 
          next="" > 

    </Node>


    <Node name="fail" log=""
          type="com.itko.lisa.test.Abend" 
          version="1" 
          uid="5AB5580DA1C311EAB1C918DBF2466751" 
          think="0h" 
          useFilters="true" 
          quiet="true" 
          next="abort" > 

    </Node>


    <Node name="end" log=""
          type="com.itko.lisa.test.NormalEnd" 
          version="1" 
          uid="5AB5580FA1C311EAB1C918DBF2466751" 
          think="0h" 
          useFilters="true" 
          quiet="true" 
          next="fail" > 

    </Node>


    <DataSet type="com.itko.lisa.test.DataSetRandIDGenerator" name="paymentId" atend="" local="false" random="false" maxItemsToFetch="100" >
<sample>rO0ABXNyABFqYXZhLnV0aWwuSGFzaE1hcAUH2sHDFmDRAwACRgAKbG9hZEZhY3RvckkACXRocmVzaG9sZHhwP0AAAAAAAAx3CAAAABAAAAABdAAJcGF5bWVudElkdAAKMjIzMTIxMDA3OXg=</sample>
<type>Number</type>
<length>5</length>
<prefix></prefix>
    </DataSet>

</TestCase>
