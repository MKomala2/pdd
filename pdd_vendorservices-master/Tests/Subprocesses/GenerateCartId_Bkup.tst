<?xml version="1.0" encoding="UTF-8"?>
<TestCase name="GenerateCartId_Bkup" version="5">

<meta>
   <create author="asanka4" buildNumber="10.3.0.297" date="04/27/2020" host="LP-BW4VPF2" version="10.3.0"/>
   <lastEdited author="asanka4" buildNumber="10.3.0.297" date="05/05/2020" host="LP-BW4VPF2" version="10.3.0"/>
</meta>

<id>844FCD6290A411EA8E6218DBF2466751</id>
<Documentation>Put documentation of the Test Case here.</Documentation>
<IsInProject>true</IsInProject>
<sig>ZWQ9NSZ0Y3Y9NSZsaXNhdj0xMC4zLjAgKDEwLjMuMC4yOTcpJm5vZGVzPTk2NjgzNTc5Ng==</sig>
<subprocess>true</subprocess>

<initState>
    <Parameter>
    <key>EnvironmentTested</key>
    <value>{{EnvironmentTested}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>channelId</key>
    <value>{{channelId}}</value>
    <name>Referenced 1st in Load Request1</name>
    </Parameter>
</initState>

<resultState>
    <Parameter>
    <key>FailureDescription</key>
    <value/>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>Request1</key>
    <value/>
    <name>Set 1st in Parse Request1</name>
    </Parameter>
    <Parameter>
    <key>Request2</key>
    <value/>
    <name>Set 1st in Parse Request2</name>
    </Parameter>
    <Parameter>
    <key>Request3</key>
    <value/>
    <name>Set 1st in Parse Request3</name>
    </Parameter>
    <Parameter>
    <key>Request4</key>
    <value/>
    <name>Set 1st in Parse Request4</name>
    </Parameter>
    <Parameter>
    <key>Request5</key>
    <value/>
    <name>Set 1st in Parse Request5</name>
    </Parameter>
    <Parameter>
    <key>Test_Execution_Status</key>
    <value/>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>cartId</key>
    <value/>
    <name>Set 1st in Send Request 1</name>
    </Parameter>
    <Parameter>
    <key>cartItemId</key>
    <value/>
    <name>Set 1st in Send Request 1</name>
    </Parameter>
    <Parameter>
    <key>lisa.Parse Request1.rsp</key>
    <value/>
    <name>Set 1st in Parse Request1</name>
    </Parameter>
    <Parameter>
    <key>lisa.Parse Request1.rsp.time</key>
    <value/>
    <name>Set 1st in Parse Request1</name>
    </Parameter>
    <Parameter>
    <key>lisa.Parse Request2.rsp</key>
    <value/>
    <name>Set 1st in Parse Request2</name>
    </Parameter>
    <Parameter>
    <key>lisa.Parse Request2.rsp.time</key>
    <value/>
    <name>Set 1st in Parse Request2</name>
    </Parameter>
    <Parameter>
    <key>lisa.Parse Request3.rsp</key>
    <value/>
    <name>Set 1st in Parse Request3</name>
    </Parameter>
    <Parameter>
    <key>lisa.Parse Request3.rsp.time</key>
    <value/>
    <name>Set 1st in Parse Request3</name>
    </Parameter>
    <Parameter>
    <key>lisa.Parse Request4.rsp</key>
    <value/>
    <name>Set 1st in Parse Request4</name>
    </Parameter>
    <Parameter>
    <key>lisa.Parse Request4.rsp.time</key>
    <value/>
    <name>Set 1st in Parse Request4</name>
    </Parameter>
    <Parameter>
    <key>lisa.Parse Request5.rsp</key>
    <value/>
    <name>Set 1st in Parse Request5</name>
    </Parameter>
    <Parameter>
    <key>lisa.Parse Request5.rsp.time</key>
    <value/>
    <name>Set 1st in Parse Request5</name>
    </Parameter>
    <Parameter>
    <key>rootItemId</key>
    <value/>
    <name>Set 1st in Send Request 1</name>
    </Parameter>
</resultState>

<deletedProps>
    <key>lisa.Pass Step.rsp.time</key>
    <key>lisa.Load Request5.rsp</key>
    <key>lisa.Send Request 6.rsp.time</key>
    <key>lisa.Load Request3.rsp.time</key>
    <key>LocalPath</key>
    <key>lisa.Send Request 3.rsp</key>
    <key>lisa.Load Request2.rsp.time</key>
    <key>lisa.Pass Step.rsp</key>
    <key>lisa.Load Request4.rsp</key>
    <key>lisa.Load Request6.rsp.time</key>
    <key>lisa.Fail Step.rsp.time</key>
    <key>lisa.Send Request 4.rsp</key>
    <key>lisa.Send Request 3.rsp.time</key>
    <key>lisa.Load Request3.rsp</key>
    <key>lisa.Execute script (JSR-223).rsp</key>
    <key>lisa.Load Request4.rsp.time</key>
    <key>lisa.Load Request1.rsp.time</key>
    <key>LOCDIR</key>
    <key>lisa.Send Request 1.rsp</key>
    <key>lisa.Send Request 2.rsp.time</key>
    <key>lisa.Fail Step.rsp</key>
    <key>ExecutionMode</key>
    <key>lisa.Send Request 5.rsp.time</key>
    <key>lisa.Load Request2.rsp</key>
    <key>lisa.Send Request 5.rsp</key>
    <key>lisa.Send Request 1.rsp.time</key>
    <key>lisa.Load Request6.rsp</key>
    <key>lisa.Load Request5.rsp.time</key>
    <key>lisa.Send Request 4.rsp.time</key>
    <key>ENDPOINT</key>
    <key>lisa.Send Request 2.rsp</key>
    <key>lisa.Execute script (JSR-223).rsp.time</key>
    <key>lisa.Load Request1.rsp</key>
    <key>HandlerName</key>
    <key>lisa.Send Request 6.rsp</key>
</deletedProps>

    <Node log="" name="Execute script (JSR-223)" next="Load Request1" quiet="false" think="500-1S" type="com.itko.lisa.test.UserScriptNode" uid="844FCD6390A411EA8E6218DBF2466751" useFilters="true" version="1"> 


      <!-- Data Sets -->
<readrec>sessionId</readrec>
<readrec>interactionId</readrec>

      <!-- Assertions -->
<CheckResult assertTrue="true" name="Any Exception Then Fail" type="com.itko.lisa.dynexec.CheckInvocationEx">
<log>Assertion name: Any Exception Then Fail checks for: true is of type: Assert on Invocation Exception.</log>
<then>Fail Step</then>
<valueToAssertKey/>
        <param>.*</param>
</CheckResult>

<onerror>Fail Step</onerror>
<language>BeanShell</language>
<copyProps>TestExecProps</copyProps>
<script>&#xd;
String envselector = testExec.getStateValue("EnvironmentTested");&#xd;
//String channel = testExec.getStateValue("channelId");&#xd;
&#xd;
testExec.setStateValue("channelId", "WEB");&#xd;
&#xd;
//Set Host&#xd;
if(envselector.equals("QLAB02")){&#xd;
  erricsonhost = "10.253.0.105:8300";&#xd;
}&#xd;
if(envselector.equals("QLAB06")){&#xd;
  erricsonhost = "10.253.74.87:8080";&#xd;
}&#xd;
if(envselector.equals("QLAB03")){&#xd;
  erricsonhost = "10.253.0.105:8200";&#xd;
}&#xd;
if(envselector.equals("QLAB07")){&#xd;
  erricsonhost = "10.253.9.178:8080";&#xd;
}&#xd;
&#xd;
testExec.setStateValue("Erricson_Host",erricsonhost);&#xd;
&#xd;
&#xd;
//Set variables&#xd;
/*if(channel.equals("RETAIL")){&#xd;
&#xd;
  senderId = "TMO";&#xd;
  applicationId = "REBELLION";&#xd;
  segChannelId = "In-Person";&#xd;
  segSubChannelId = "Full Service";&#xd;
  segCategoryName = "T-Mobile Retail";&#xd;
  paymentChannel = "REBELLION";&#xd;
}&#xd;
if(channel.equals("CARE")){&#xd;
&#xd;
  senderId = "CARE";&#xd;
  applicationId = "SAP-CARE";&#xd;
  segChannelId = "Over the Phone";&#xd;
  segSubChannelId = "Care";&#xd;
  segCategoryName = "Frontline";&#xd;
  paymentChannel = "SAPCARE";&#xd;
}&#xd;
if(channel.equals("WEB")){&#xd;
&#xd;
  senderId = "TMO";&#xd;
  applicationId = "REBELLION";&#xd;
  segChannelId = "Unassisted";&#xd;
  segSubChannelId = "Self Service Anonymous";&#xd;
  segCategoryName = "General Anonymous";&#xd;
  paymentChannel = "REBELLION";&#xd;
}&#xd;
&#xd;
testExec.setStateValue("SenderId",senderId);&#xd;
testExec.setStateValue("ApplicationId",applicationId);&#xd;
testExec.setStateValue("SegChannelId",segChannelId);&#xd;
testExec.setStateValue("SegSubChannelId",segSubChannelId);&#xd;
testExec.setStateValue("SegCategoryName",segCategoryName);&#xd;
testExec.setStateValue("PaymentChannel",paymentChannel);*/&#xd;
return true;&#xd;
&#xd;
&#xd;
&#xd;
</script>
    </Node>


    <Node log="" name="Load Request1" next="Parse Request1" quiet="true" think="500-1S" type="com.itko.lisa.test.FileNode" uid="844FCD6490A411EA8E6218DBF2466751" useFilters="true" version="1"> 

<Loc>{{LISA_RELATIVE_PROJ_ROOT}}/Data/Requests/GenerateCart/generateCartId_{{channelId}}_Request1.txt</Loc>
<charset>DEFAULT</charset>
<PropKey>Request1</PropKey>
<onFail>Fail Step</onFail>
    </Node>


    <Node log="" name="Parse Request1" next="Send Request 1" quiet="true" think="500-1S" type="com.itko.lisa.utils.ParseTextContentNode" uid="844FCD6590A411EA8E6218DBF2466751" useFilters="true" version="1"> 

<text>e3tSZXF1ZXN0MX19</text>
<propKey>Request1</propKey>
    </Node>


    <Node log="" name="Send Request 1" next="Load Request2" quiet="false" think="500-1S" type="com.itko.lisa.ws.nx.NxWSStep" uid="844FCD6690A411EA8E6218DBF2466751" useFilters="true" version="1"> 


      <!-- Filters -->
      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.Send Request 1.rsp</valueToFilterKey>
<prop>cartId</prop>
<xpathq>/*[local-name()='Envelope']/*[local-name()='Body']/*[local-name()='updateCartResponse']/*[local-name()='cart']/@orderId</xpathq>
      </Filter>

      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.Send Request 1.rsp</valueToFilterKey>
<prop>cartItemId</prop>
<xpathq>/*[local-name()='Envelope']/*[local-name()='Body']/*[local-name()='updateCartResponse']/*[local-name()='cart']/*[local-name()='lineOfServiceList']/*[local-name()='LineOfService']/*[local-name()='planCartItemId']/text()</xpathq>
      </Filter>

      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.Send Request 1.rsp</valueToFilterKey>
<prop>rootItemId</prop>
<xpathq>/*[local-name()='Envelope']/*[local-name()='Body']/*[local-name()='updateCartResponse']/*[local-name()='cart']/*[local-name()='lineOfServiceList']/*[local-name()='LineOfService']/*[local-name()='rootItemId']/text()</xpathq>
      </Filter>

<wsdl>{{LISA_RELATIVE_PROJ_URL}}/Data/WSDL/CartWSIL_Golv.wsdl</wsdl>
<endpoint>http://{{Erricson_Host}}/cwf/services/CartON?wsdl</endpoint>
<targetNamespace>http://services.tmobile.com/OrderManagement/CartWSIL/V1</targetNamespace>
<service>CartWSIL</service>
<port>CartWSIL</port>
<operation>updateCart</operation>
<onError>Fail Step</onError>
<maintainSession>true</maintainSession>
<clearSession>false</clearSession>
<request itko_enc="base64">e3tSZXF1ZXN0MX19</request>
<style>document</style>
<use>literal</use>
<soapAction>updateCart</soapAction>
<sslInfo>
<ssl-keystore-password-enc>lac6c7e3a19daa9cf8f0a264ec878721f910e8a00d2ebb169a9aaee0f52439309</ssl-keystore-password-enc>
<ssl-key-password-enc>l760baedf435186270795c66fd585289e7fba600cf7dfcb2c6a643ccd9e6edb19</ssl-key-password-enc>
</sslInfo>
<wsiInfo>
<wsi-check-wsdl>false</wsi-check-wsdl>
<wsi-check-msg>false</wsi-check-msg>
<wsi-display-type>onlyFailed</wsi-display-type>
<wsi-on-fail>continue</wsi-on-fail>
</wsiInfo>
<transport>
<soapVersionUri>1.1</soapVersionUri>
<httpVersionUri>1.1</httpVersionUri>
<webMethodUri>POST</webMethodUri>
<mtom>false</mtom>
<dontSendRequest>false</dontSendRequest>
<fault-as-error>true</fault-as-error>
<callTimeout>30000</callTimeout>
</transport>
<uddiActive>false</uddiActive>
<uddi-result>
<uddi-selection>
</uddi-selection>
</uddi-result>
<wss4jInfo>
<version>5</version>
<wssecurity>
<isSend>true</isSend>
<must>false</must>
</wssecurity>
<wsi-bsp>false</wsi-bsp>
</wss4jInfo>
<wsaInfo>
<useAddressing>false</useAddressing>
<must>false</must>
<to/>
<toOverride>false</toOverride>
<toDefault>true</toDefault>
<from/>
<fromOverride>false</fromOverride>
<fromDefault>true</fromDefault>
<action/>
<actionOverride>false</actionOverride>
<actionDefault>true</actionDefault>
<msgid/>
<msgidOverride>false</msgidOverride>
<msgidDefault>true</msgidDefault>
<replyTo/>
<replyToOverride>false</replyToOverride>
<faultTo/>
<faulttToOverride>false</faulttToOverride>
<addressingVersion>http://www.w3.org/2005/08/addressing</addressingVersion>
</wsaInfo>
<customHTTPHeaderInfo>
</customHTTPHeaderInfo>
<attachments>
<attachment-type>MIME</attachment-type>
</attachments>
<mtom-paths>
</mtom-paths>
    </Node>


    <Node log="" name="Load Request2" next="Parse Request2" quiet="true" think="500-1S" type="com.itko.lisa.test.FileNode" uid="844FCD6790A411EA8E6218DBF2466751" useFilters="true" version="1"> 

<Loc>{{LISA_RELATIVE_PROJ_ROOT}}/Data/Requests/GenerateCart/generateCartId_{{channelId}}_Request2.txt</Loc>
<charset>DEFAULT</charset>
<PropKey>Request2</PropKey>
<onFail>Fail Step</onFail>
    </Node>


    <Node log="" name="Parse Request2" next="Send Request 2" quiet="true" think="500-1S" type="com.itko.lisa.utils.ParseTextContentNode" uid="844FCD6890A411EA8E6218DBF2466751" useFilters="true" version="1"> 

<text>e3tSZXF1ZXN0Mn19</text>
<propKey>Request2</propKey>
    </Node>


    <Node log="" name="Send Request 2" next="Load Request3" quiet="false" think="500-1S" type="com.itko.lisa.ws.nx.NxWSStep" uid="844FCD6990A411EA8E6218DBF2466751" useFilters="true" version="1"> 

<wsdl>{{LISA_RELATIVE_PROJ_URL}}/Data/WSDL/CartWSIL_Golv.wsdl</wsdl>
<endpoint>http://{{Erricson_Host}}/cwf/services/CartON?wsdl</endpoint>
<targetNamespace>http://services.tmobile.com/OrderManagement/CartWSIL/V1</targetNamespace>
<service>CartWSIL</service>
<port>CartWSIL</port>
<operation>updateCart</operation>
<onError>Fail Step</onError>
<maintainSession>true</maintainSession>
<clearSession>false</clearSession>
<request itko_enc="base64">e3tSZXF1ZXN0Mn19</request>
<style>document</style>
<use>literal</use>
<soapAction>updateCart</soapAction>
<sslInfo>
<ssl-keystore-password-enc>lcf3089c00150bb73c6f0d2515f5a43e63b7d475d949ee4d245944163e52dcfbf</ssl-keystore-password-enc>
<ssl-key-password-enc>l0930a20c040ff50e2f93d3a62cb8fa15aee6fe195b2457fca19b9cbe0e12c3f4</ssl-key-password-enc>
</sslInfo>
<wsiInfo>
<wsi-check-wsdl>false</wsi-check-wsdl>
<wsi-check-msg>false</wsi-check-msg>
<wsi-display-type>onlyFailed</wsi-display-type>
<wsi-on-fail>continue</wsi-on-fail>
</wsiInfo>
<transport>
<soapVersionUri>1.1</soapVersionUri>
<httpVersionUri>1.1</httpVersionUri>
<webMethodUri>POST</webMethodUri>
<mtom>false</mtom>
<dontSendRequest>false</dontSendRequest>
<fault-as-error>true</fault-as-error>
<callTimeout>30000</callTimeout>
</transport>
<uddiActive>false</uddiActive>
<uddi-result>
<uddi-selection>
</uddi-selection>
</uddi-result>
<wss4jInfo>
<version>5</version>
<wssecurity>
<isSend>true</isSend>
<must>false</must>
</wssecurity>
<wsi-bsp>false</wsi-bsp>
</wss4jInfo>
<wsaInfo>
<useAddressing>false</useAddressing>
<must>false</must>
<to/>
<toOverride>false</toOverride>
<toDefault>true</toDefault>
<from/>
<fromOverride>false</fromOverride>
<fromDefault>true</fromDefault>
<action/>
<actionOverride>false</actionOverride>
<actionDefault>true</actionDefault>
<msgid/>
<msgidOverride>false</msgidOverride>
<msgidDefault>true</msgidDefault>
<replyTo/>
<replyToOverride>false</replyToOverride>
<faultTo/>
<faulttToOverride>false</faulttToOverride>
<addressingVersion>http://www.w3.org/2005/08/addressing</addressingVersion>
</wsaInfo>
<customHTTPHeaderInfo>
</customHTTPHeaderInfo>
<attachments>
<attachment-type>MIME</attachment-type>
</attachments>
<mtom-paths>
</mtom-paths>
    </Node>


    <Node log="" name="Load Request3" next="Parse Request3" quiet="true" think="500-1S" type="com.itko.lisa.test.FileNode" uid="844FCD6A90A411EA8E6218DBF2466751" useFilters="true" version="1"> 

<Loc>{{LISA_RELATIVE_PROJ_ROOT}}/Data/Requests/GenerateCart/generateCartId_{{channelId}}_Request3.txt</Loc>
<charset>DEFAULT</charset>
<PropKey>Request3</PropKey>
<onFail>Fail Step</onFail>
    </Node>


    <Node log="" name="Parse Request3" next="Send Request 3" quiet="true" think="500-1S" type="com.itko.lisa.utils.ParseTextContentNode" uid="844FCD6B90A411EA8E6218DBF2466751" useFilters="true" version="1"> 

<text>e3tSZXF1ZXN0M319</text>
<propKey>Request3</propKey>
    </Node>


    <Node log="" name="Send Request 3" next="Load Request4" quiet="false" think="500-1S" type="com.itko.lisa.ws.nx.NxWSStep" uid="844FCD6C90A411EA8E6218DBF2466751" useFilters="true" version="1"> 

<wsdl>{{LISA_RELATIVE_PROJ_URL}}/Data/WSDL/CartWSIL_Golv.wsdl</wsdl>
<endpoint>http://{{Erricson_Host}}/cwf/services/CartON?wsdl</endpoint>
<targetNamespace>http://services.tmobile.com/OrderManagement/CartWSIL/V1</targetNamespace>
<service>CartWSIL</service>
<port>CartWSIL</port>
<operation>updateCart</operation>
<onError>Fail Step</onError>
<maintainSession>true</maintainSession>
<clearSession>false</clearSession>
<request itko_enc="base64">e3tSZXF1ZXN0M319</request>
<style>document</style>
<use>literal</use>
<soapAction>updateCart</soapAction>
<sslInfo>
<ssl-keystore-password-enc>la14eb8a9395ebc894de084373dcabc506b4f7b258457d257a2e5454ef5a7ebbe</ssl-keystore-password-enc>
<ssl-key-password-enc>l7079aeccc2d10992705a30963b16b97ae31efb7c274d1ab813e748bb4fa383f0</ssl-key-password-enc>
</sslInfo>
<wsiInfo>
<wsi-check-wsdl>false</wsi-check-wsdl>
<wsi-check-msg>false</wsi-check-msg>
<wsi-display-type>onlyFailed</wsi-display-type>
<wsi-on-fail>continue</wsi-on-fail>
</wsiInfo>
<transport>
<soapVersionUri>1.1</soapVersionUri>
<httpVersionUri>1.1</httpVersionUri>
<webMethodUri>POST</webMethodUri>
<mtom>false</mtom>
<dontSendRequest>false</dontSendRequest>
<fault-as-error>true</fault-as-error>
<callTimeout>30000</callTimeout>
</transport>
<uddiActive>false</uddiActive>
<uddi-result>
<uddi-selection>
</uddi-selection>
</uddi-result>
<wss4jInfo>
<version>5</version>
<wssecurity>
<isSend>true</isSend>
<must>false</must>
</wssecurity>
<wsi-bsp>false</wsi-bsp>
</wss4jInfo>
<wsaInfo>
<useAddressing>false</useAddressing>
<must>false</must>
<to/>
<toOverride>false</toOverride>
<toDefault>true</toDefault>
<from/>
<fromOverride>false</fromOverride>
<fromDefault>true</fromDefault>
<action/>
<actionOverride>false</actionOverride>
<actionDefault>true</actionDefault>
<msgid/>
<msgidOverride>false</msgidOverride>
<msgidDefault>true</msgidDefault>
<replyTo/>
<replyToOverride>false</replyToOverride>
<faultTo/>
<faulttToOverride>false</faulttToOverride>
<addressingVersion>http://www.w3.org/2005/08/addressing</addressingVersion>
</wsaInfo>
<customHTTPHeaderInfo>
</customHTTPHeaderInfo>
<attachments>
<attachment-type>MIME</attachment-type>
</attachments>
<mtom-paths>
</mtom-paths>
    </Node>


    <Node log="" name="Load Request4" next="Parse Request4" quiet="true" think="500-1S" type="com.itko.lisa.test.FileNode" uid="844FCD6D90A411EA8E6218DBF2466751" useFilters="true" version="1"> 

<Loc>{{LISA_RELATIVE_PROJ_ROOT}}/Data/Requests/GenerateCart/generateCartId_{{channelId}}_Request4.txt</Loc>
<charset>DEFAULT</charset>
<PropKey>Request4</PropKey>
<onFail>Fail Step</onFail>
    </Node>


    <Node log="" name="Parse Request4" next="Send Request 4" quiet="true" think="500-1S" type="com.itko.lisa.utils.ParseTextContentNode" uid="844FCD6E90A411EA8E6218DBF2466751" useFilters="true" version="1"> 

<text>e3tSZXF1ZXN0NH19</text>
<propKey>Request4</propKey>
    </Node>


    <Node log="" name="Send Request 4" next="Load Request5" quiet="false" think="500-1S" type="com.itko.lisa.ws.nx.NxWSStep" uid="844FCD6F90A411EA8E6218DBF2466751" useFilters="true" version="1"> 

<wsdl>{{LISA_RELATIVE_PROJ_URL}}/Data/WSDL/CartWSIL_Golv.wsdl</wsdl>
<endpoint>http://{{Erricson_Host}}/cwf/services/CartON?wsdl</endpoint>
<targetNamespace>http://services.tmobile.com/OrderManagement/CartWSIL/V1</targetNamespace>
<service>CartWSIL</service>
<port>CartWSIL</port>
<operation>updateCart</operation>
<onError>Fail Step</onError>
<maintainSession>true</maintainSession>
<clearSession>false</clearSession>
<request itko_enc="base64">e3tSZXF1ZXN0NH19</request>
<style>document</style>
<use>literal</use>
<soapAction>updateCart</soapAction>
<sslInfo>
<ssl-keystore-password-enc>l1b60b2c8203b2a00bd060146c75b26b87dfc5bc8e01c4158dfaeaa606f4d922b</ssl-keystore-password-enc>
<ssl-key-password-enc>le404eb3987123a7ce645da679b1f1053ce20f45616d42316281caba87e08dc49</ssl-key-password-enc>
</sslInfo>
<wsiInfo>
<wsi-check-wsdl>false</wsi-check-wsdl>
<wsi-check-msg>false</wsi-check-msg>
<wsi-display-type>onlyFailed</wsi-display-type>
<wsi-on-fail>continue</wsi-on-fail>
</wsiInfo>
<transport>
<soapVersionUri>1.1</soapVersionUri>
<httpVersionUri>1.1</httpVersionUri>
<webMethodUri>POST</webMethodUri>
<mtom>false</mtom>
<dontSendRequest>false</dontSendRequest>
<fault-as-error>true</fault-as-error>
<callTimeout>30000</callTimeout>
</transport>
<uddiActive>false</uddiActive>
<uddi-result>
<uddi-selection>
</uddi-selection>
</uddi-result>
<wss4jInfo>
<version>5</version>
<wssecurity>
<isSend>true</isSend>
<must>false</must>
</wssecurity>
<wsi-bsp>false</wsi-bsp>
</wss4jInfo>
<wsaInfo>
<useAddressing>false</useAddressing>
<must>false</must>
<to/>
<toOverride>false</toOverride>
<toDefault>true</toDefault>
<from/>
<fromOverride>false</fromOverride>
<fromDefault>true</fromDefault>
<action/>
<actionOverride>false</actionOverride>
<actionDefault>true</actionDefault>
<msgid/>
<msgidOverride>false</msgidOverride>
<msgidDefault>true</msgidDefault>
<replyTo/>
<replyToOverride>false</replyToOverride>
<faultTo/>
<faulttToOverride>false</faulttToOverride>
<addressingVersion>http://www.w3.org/2005/08/addressing</addressingVersion>
</wsaInfo>
<customHTTPHeaderInfo>
</customHTTPHeaderInfo>
<attachments>
<attachment-type>MIME</attachment-type>
</attachments>
<mtom-paths>
</mtom-paths>
    </Node>


    <Node log="" name="Load Request5" next="Parse Request5" quiet="true" think="500-1S" type="com.itko.lisa.test.FileNode" uid="844FCD7090A411EA8E6218DBF2466751" useFilters="true" version="1"> 

<Loc>{{LISA_RELATIVE_PROJ_ROOT}}/Data/Requests/GenerateCart/generateCartId_{{channelId}}_Request5.txt</Loc>
<charset>DEFAULT</charset>
<PropKey>Request5</PropKey>
<onFail>Fail Step</onFail>
    </Node>


    <Node log="" name="Parse Request5" next="Send Request 5" quiet="true" think="500-1S" type="com.itko.lisa.utils.ParseTextContentNode" uid="844FF48190A411EA8E6218DBF2466751" useFilters="true" version="1"> 

<text>e3tSZXF1ZXN0NX19</text>
<propKey>Request5</propKey>
    </Node>


    <Node log="" name="Send Request 5" next="Pass Step" quiet="false" think="500-1S" type="com.itko.lisa.ws.nx.NxWSStep" uid="844FF48290A411EA8E6218DBF2466751" useFilters="true" version="1"> 


      <!-- Assertions -->
<CheckResult assertTrue="true" name="check if RETAIL Channel" type="com.itko.lisa.test.CheckResultPropRegEx">
<log>Assertion name: check if RETAIL Channel checks for: true is of type: Property Value Expression.</log>
<then>Load Request6</then>
<valueToAssertKey/>
        <prop>channelId</prop>
        <param>RETAIL</param>
</CheckResult>

<wsdl>{{LISA_RELATIVE_PROJ_URL}}/Data/WSDL/CartWSIL_Golv.wsdl</wsdl>
<endpoint>http://{{Erricson_Host}}/cwf/services/CartON?wsdl</endpoint>
<targetNamespace>http://services.tmobile.com/OrderManagement/CartWSIL/V1</targetNamespace>
<service>CartWSIL</service>
<port>CartWSIL</port>
<operation>updateCart</operation>
<onError>Fail Step</onError>
<maintainSession>true</maintainSession>
<clearSession>false</clearSession>
<request itko_enc="base64">e3tSZXF1ZXN0NX19</request>
<style>document</style>
<use>literal</use>
<soapAction>updateCart</soapAction>
<sslInfo>
<ssl-keystore-password-enc>lb69e501fa62802b888f36682c7e8afd4804fdec61b27cc63787a52ef0efdc6ec</ssl-keystore-password-enc>
<ssl-key-password-enc>l096a84ff43ed96881476718f3282baf4308e3c3aa8fd60896f8b14dd980cb8c1</ssl-key-password-enc>
</sslInfo>
<wsiInfo>
<wsi-check-wsdl>false</wsi-check-wsdl>
<wsi-check-msg>false</wsi-check-msg>
<wsi-display-type>onlyFailed</wsi-display-type>
<wsi-on-fail>continue</wsi-on-fail>
</wsiInfo>
<transport>
<soapVersionUri>1.1</soapVersionUri>
<httpVersionUri>1.1</httpVersionUri>
<webMethodUri>POST</webMethodUri>
<mtom>false</mtom>
<dontSendRequest>false</dontSendRequest>
<fault-as-error>true</fault-as-error>
<callTimeout>30000</callTimeout>
</transport>
<uddiActive>false</uddiActive>
<uddi-result>
<uddi-selection>
</uddi-selection>
</uddi-result>
<wss4jInfo>
<version>5</version>
<wssecurity>
<isSend>true</isSend>
<must>false</must>
</wssecurity>
<wsi-bsp>false</wsi-bsp>
</wss4jInfo>
<wsaInfo>
<useAddressing>false</useAddressing>
<must>false</must>
<to/>
<toOverride>false</toOverride>
<toDefault>true</toDefault>
<from/>
<fromOverride>false</fromOverride>
<fromDefault>true</fromDefault>
<action/>
<actionOverride>false</actionOverride>
<actionDefault>true</actionDefault>
<msgid/>
<msgidOverride>false</msgidOverride>
<msgidDefault>true</msgidDefault>
<replyTo/>
<replyToOverride>false</replyToOverride>
<faultTo/>
<faulttToOverride>false</faulttToOverride>
<addressingVersion>http://www.w3.org/2005/08/addressing</addressingVersion>
</wsaInfo>
<customHTTPHeaderInfo>
</customHTTPHeaderInfo>
<attachments>
<attachment-type>MIME</attachment-type>
</attachments>
<mtom-paths>
</mtom-paths>
    </Node>


    <Node log="" name="Pass Step" next="end" quiet="false" think="500-1S" type="com.itko.lisa.test.UserScriptNode" uid="844FF48390A411EA8E6218DBF2466751" useFilters="true" version="1"> 


      <!-- Assertions -->
<CheckResult assertTrue="true" name="Any Exception Then Fail" type="com.itko.lisa.dynexec.CheckInvocationEx">
<log>Assertion name: Any Exception Then Fail checks for: true is of type: Assert on Invocation Exception.</log>
<then>fail</then>
<valueToAssertKey/>
        <param>.*</param>
</CheckResult>

<onerror>continue</onerror>
<language>BeanShell</language>
<copyProps>TestExecProps</copyProps>
<script>&#xd;
testExec.setStateValue("FailureDescription","");&#xd;
testExec.setStateValue("Test_Execution_Status","Passed");&#xd;
testExec.setStateValue("CartID","{{cartId}}");&#xd;
return true;</script>
    </Node>


    <Node log="" name="Fail Step" next="end" quiet="false" think="500-1S" type="com.itko.lisa.test.UserScriptNode" uid="844FF48490A411EA8E6218DBF2466751" useFilters="true" version="1"> 


      <!-- Assertions -->
<CheckResult assertTrue="true" name="Any Exception Then Fail" type="com.itko.lisa.dynexec.CheckInvocationEx">
<log>Assertion name: Any Exception Then Fail checks for: true is of type: Assert on Invocation Exception.</log>
<then>fail</then>
<valueToAssertKey/>
        <param>.*</param>
</CheckResult>

<onerror>continue</onerror>
<language>BeanShell</language>
<copyProps>TestExecProps</copyProps>
<script>&#xd;
testExec.setStateValue("FailureDescription","Cart Id is not generated");&#xd;
testExec.setStateValue("Test_Execution_Status","Failed");&#xd;
testExec.setStateValue("STEPNAME","Subprocess GenerateCartId");&#xd;
return true;</script>
    </Node>


    <Node log="" name="Load Request6" next="Parse Request6" quiet="true" think="500-1S" type="com.itko.lisa.test.FileNode" uid="844FF48590A411EA8E6218DBF2466751" useFilters="true" version="1"> 

<Loc>{{LISA_RELATIVE_PROJ_ROOT}}/Data/Requests/GenerateCart/generateCartId_{{ChannelId}}_Request6.txt</Loc>
<charset>DEFAULT</charset>
<PropKey>Request6</PropKey>
<onFail>Fail Step</onFail>
    </Node>


    <Node log="" name="Parse Request6" next="Send Request 6" quiet="true" think="500-1S" type="com.itko.lisa.utils.ParseTextContentNode" uid="844FF48690A411EA8E6218DBF2466751" useFilters="true" version="1"> 

<text>e3tSZXF1ZXN0Nn19</text>
<propKey>Request6</propKey>
    </Node>


    <Node log="" name="Send Request 6" next="Pass Step" quiet="false" think="500-1S" type="com.itko.lisa.ws.nx.NxWSStep" uid="844FF48790A411EA8E6218DBF2466751" useFilters="true" version="1"> 

<wsdl>{{LISA_RELATIVE_PROJ_URL}}/Data/WSDL/CartWSIL_Golv.wsdl</wsdl>
<endpoint>http://{{Ericson_Host}}/cwf/services/CartON?wsdl</endpoint>
<targetNamespace>http://services.tmobile.com/OrderManagement/CartWSIL/V1</targetNamespace>
<service>CartWSIL</service>
<port>CartWSIL</port>
<operation>updateCart</operation>
<onError>Fail Step</onError>
<maintainSession>true</maintainSession>
<clearSession>false</clearSession>
<request itko_enc="base64">e3tSZXF1ZXN0Nn19</request>
<style>document</style>
<use>literal</use>
<soapAction>updateCart</soapAction>
<sslInfo>
<ssl-keystore-password-enc>l73aee04dd5dcb0a45c1aa0c0c984f737cbcd1380dc68309ec90d6cef44752488</ssl-keystore-password-enc>
<ssl-key-password-enc>lab3f87ccea9677646fe7e0071a24becbd6a7b7d107f38518a05a67c4be75f609</ssl-key-password-enc>
</sslInfo>
<wsiInfo>
<wsi-check-wsdl>false</wsi-check-wsdl>
<wsi-check-msg>false</wsi-check-msg>
<wsi-display-type>onlyFailed</wsi-display-type>
<wsi-on-fail>continue</wsi-on-fail>
</wsiInfo>
<transport>
<soapVersionUri>1.1</soapVersionUri>
<httpVersionUri>1.1</httpVersionUri>
<webMethodUri>POST</webMethodUri>
<mtom>false</mtom>
<dontSendRequest>false</dontSendRequest>
<fault-as-error>true</fault-as-error>
<callTimeout>30000</callTimeout>
</transport>
<uddiActive>false</uddiActive>
<uddi-result>
<uddi-selection>
</uddi-selection>
</uddi-result>
<wss4jInfo>
<version>5</version>
<wssecurity>
<isSend>true</isSend>
<must>false</must>
</wssecurity>
<wsi-bsp>false</wsi-bsp>
</wss4jInfo>
<wsaInfo>
<useAddressing>false</useAddressing>
<must>false</must>
<to/>
<toOverride>false</toOverride>
<toDefault>true</toDefault>
<from/>
<fromOverride>false</fromOverride>
<fromDefault>true</fromDefault>
<action/>
<actionOverride>false</actionOverride>
<actionDefault>true</actionDefault>
<msgid/>
<msgidOverride>false</msgidOverride>
<msgidDefault>true</msgidDefault>
<replyTo/>
<replyToOverride>false</replyToOverride>
<faultTo/>
<faulttToOverride>false</faulttToOverride>
<addressingVersion>http://www.w3.org/2005/08/addressing</addressingVersion>
</wsaInfo>
<customHTTPHeaderInfo>
</customHTTPHeaderInfo>
<attachments>
<attachment-type>MIME</attachment-type>
</attachments>
<mtom-paths>
</mtom-paths>
    </Node>


    <Node log="" name="abort" next="" quiet="true" think="0h" type="com.itko.lisa.test.AbortStep" uid="844FF48890A411EA8E6218DBF2466751" useFilters="true" version="1"> 

    </Node>


    <Node log="" name="fail" next="abort" quiet="true" think="0h" type="com.itko.lisa.test.Abend" uid="844FF48990A411EA8E6218DBF2466751" useFilters="true" version="1"> 

    </Node>


    <Node log="" name="end" next="fail" quiet="true" think="0h" type="com.itko.lisa.test.NormalEnd" uid="844FF48A90A411EA8E6218DBF2466751" useFilters="true" version="1"> 

    </Node>


    <DataSet atend="" local="false" maxItemsToFetch="0" name="sessionId" random="false" type="com.itko.lisa.test.DataSetRandIDGenerator">
<sample>rO0ABXNyABFqYXZhLnV0aWwuSGFzaE1hcAUH2sHDFmDRAwACRgAKbG9hZEZhY3RvckkACXRocmVzaG9sZHhwP0AAAAAAAAB3CAAAABAAAAAAeA==</sample>
<type>Alphanumeric</type>
<length>20</length>
<prefix/>
    </DataSet>

    <DataSet atend="" local="false" maxItemsToFetch="0" name="interactionId" random="false" type="com.itko.lisa.test.DataSetRandIDGenerator">
<sample>rO0ABXNyABFqYXZhLnV0aWwuSGFzaE1hcAUH2sHDFmDRAwACRgAKbG9hZEZhY3RvckkACXRocmVzaG9sZHhwP0AAAAAAAAB3CAAAABAAAAAAeA==</sample>
<type>Alphanumeric</type>
<length>18</length>
<prefix/>
    </DataSet>

</TestCase>