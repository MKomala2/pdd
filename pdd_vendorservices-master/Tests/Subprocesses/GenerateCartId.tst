<?xml version="1.0" ?>

<TestCase name="GenerateCartId" version="5">

<meta>
   <create version="10.3.0" buildNumber="10.3.0.297" author="asanka4" date="04/27/2020" host="LP-BW4VPF2" />
   <lastEdited version="10.3.0" buildNumber="10.3.0.297" author="asanka4" date="05/26/2020" host="LP-BW4VPF2" />
</meta>

<id>B32A5E33889E11EA810D18DBF2466751</id>
<Documentation>Put documentation of the Test Case here.</Documentation>
<IsInProject>true</IsInProject>
<sig>ZWQ9NSZ0Y3Y9NSZsaXNhdj0xMC4zLjAgKDEwLjMuMC4yOTcpJm5vZGVzPTEzNTY5MzEwMw==</sig>
<subprocess>true</subprocess>

<initState>
    <Parameter>
    <key>EnvironmentTested</key>
    <value>{{EnvironmentTested}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>channelId</key>
    <value>{{channelId}}</value>
    <name>Referenced 1st in Load Request1</name>
    </Parameter>
    <Parameter>
    <key>sessionId</key>
    <value>{{sessionId}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>interactionId</key>
    <value>{{interactionId}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
</initState>

<resultState>
    <Parameter>
    <key>CartID</key>
    <value></value>
    <name>Set 1st in Send Request</name>
    </Parameter>
    <Parameter>
    <key>FailureDescription</key>
    <value></value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>Test_Execution_Status</key>
    <value></value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>applicationUserId</key>
    <value></value>
    <name>Set 1st in Send Request</name>
    </Parameter>
    <Parameter>
    <key>cartItemId</key>
    <value></value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>offerSubType</key>
    <value></value>
    <name>Set 1st in Send Request</name>
    </Parameter>
    <Parameter>
    <key>offerType</key>
    <value></value>
    <name>Set 1st in Send Request</name>
    </Parameter>
    <Parameter>
    <key>offeringVariantId</key>
    <value></value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>orderType</key>
    <value></value>
    <name>Set 1st in Send Request</name>
    </Parameter>
    <Parameter>
    <key>productOfferingId</key>
    <value></value>
    <name>Set 1st in Send Request</name>
    </Parameter>
    <Parameter>
    <key>productOfferingLabel</key>
    <value></value>
    <name>Set 1st in Send Request</name>
    </Parameter>
    <Parameter>
    <key>productSubType</key>
    <value></value>
    <name>Set 1st in Send Request</name>
    </Parameter>
    <Parameter>
    <key>productType</key>
    <value></value>
    <name>Set 1st in Send Request</name>
    </Parameter>
    <Parameter>
    <key>sku</key>
    <value></value>
    <name>Set 1st in Send Request</name>
    </Parameter>
    <Parameter>
    <key>tillNumber</key>
    <value></value>
    <name>Set 1st in Send Request</name>
    </Parameter>
    <Parameter>
    <key>totalChargeAmount</key>
    <value></value>
    <name>Set 1st in Send Request</name>
    </Parameter>
    <Parameter>
    <key>totalDueAmount</key>
    <value></value>
    <name>Set 1st in Send Request</name>
    </Parameter>
    <Parameter>
    <key>totalTaxAmount</key>
    <value></value>
    <name>Set 1st in Send Request</name>
    </Parameter>
</resultState>

<deletedProps>
    <key>lisa.Pass Step.rsp.time</key>
    <key>lisa.Parse Request3.rsp</key>
    <key>LocalPath</key>
    <key>lisa.Load Request.rsp.time</key>
    <key>lisa.Send Request 3.rsp</key>
    <key>lisa.Load Request2.rsp.time</key>
    <key>lisa.Pass Step.rsp</key>
    <key>productSubType</key>
    <key>lisa.Parse Request2.rsp</key>
    <key>lisa.Send Request 4.rsp</key>
    <key>lisa.Execute script (JSR-223).rsp</key>
    <key>lisa.Send Request 1.rsp</key>
    <key>ExecutionMode</key>
    <key>lisa.Parse Request1.rsp</key>
    <key>lisa.Parse Request4.rsp.time</key>
    <key>lisa.Send Request 5.rsp.time</key>
    <key>lisa.Load Request6.rsp</key>
    <key>lisa.Send Request 4.rsp.time</key>
    <key>lisa.Load Request.rsp</key>
    <key>lisa.Parse Request3.rsp.time</key>
    <key>lisa.Send Request 2.rsp</key>
    <key>lisa.Execute script (JSR-223).rsp.time</key>
    <key>HandlerName</key>
    <key>lisa.Parse Request.rsp</key>
    <key>lisa.Load Request5.rsp</key>
    <key>lisa.Send Request 6.rsp.time</key>
    <key>lisa.Load Request3.rsp.time</key>
    <key>Request2</key>
    <key>Request1</key>
    <key>Request4</key>
    <key>lisa.Load Request4.rsp</key>
    <key>Request3</key>
    <key>lisa.Load Request6.rsp.time</key>
    <key>Request5</key>
    <key>lisa.Fail Step.rsp.time</key>
    <key>lisa.Parse Request2.rsp.time</key>
    <key>lisa.Parse Request5.rsp.time</key>
    <key>lisa.Send Request 3.rsp.time</key>
    <key>offeringVariantId</key>
    <key>lisa.Load Request3.rsp</key>
    <key>productType</key>
    <key>lisa.Load Request4.rsp.time</key>
    <key>lisa.Load Request1.rsp.time</key>
    <key>ResponseCode</key>
    <key>LOCDIR</key>
    <key>lisa.Parse Request1.rsp.time</key>
    <key>lisa.Send Request 2.rsp.time</key>
    <key>lisa.Parse Request5.rsp</key>
    <key>lisa.Fail Step.rsp</key>
    <key>Request</key>
    <key>rootItemId</key>
    <key>lisa.Send Request.rsp.time</key>
    <key>lisa.Send Request.rsp</key>
    <key>lisa.Parse Request.rsp.time</key>
    <key>lisa.Load Request2.rsp</key>
    <key>lisa.Send Request 5.rsp</key>
    <key>lisa.Send Request 1.rsp.time</key>
    <key>lisa.Parse Request4.rsp</key>
    <key>lisa.Load Request5.rsp.time</key>
    <key>ENDPOINT</key>
    <key>lisa.Load Request1.rsp</key>
    <key>lisa.Send Request 6.rsp</key>
</deletedProps>

    <Node name="Execute script (JSR-223)" log=""
          type="com.itko.lisa.test.UserScriptNode" 
          version="1" 
          uid="C14C3ABB889E11EA810D18DBF2466751" 
          think="500-1S" 
          useFilters="true" 
          quiet="false" 
          next="Load Request" > 


      <!-- Assertions -->
<CheckResult assertTrue="true" name="Any Exception Then Fail" type="com.itko.lisa.dynexec.CheckInvocationEx">
<log>Assertion name: Any Exception Then Fail checks for: true is of type: Assert on Invocation Exception.</log>
<then>Fail Step</then>
<valueToAssertKey></valueToAssertKey>
        <param>.*</param>
</CheckResult>

<onerror>Fail Step</onerror>
<language>BeanShell</language>
<copyProps>TestExecProps</copyProps>
<script>&#13;&#10;String envselector = testExec.getStateValue(&quot;EnvironmentTested&quot;);&#13;&#10;String channel = testExec.getStateValue(&quot;channelId&quot;);&#13;&#10;&#13;&#10;//testExec.setStateValue(&quot;channelId&quot;, &quot;WEB&quot;);&#13;&#10;&#13;&#10;//Set Host&#13;&#10;if(envselector.equals(&quot;QLAB02&quot;)){&#13;&#10;  erricsonhost = &quot;10.253.0.105:8300&quot;;&#13;&#10;}&#13;&#10;if(envselector.equals(&quot;QLAB06&quot;)){&#13;&#10;  erricsonhost = &quot;10.253.74.87:8080&quot;;&#13;&#10;}&#13;&#10;if(envselector.equals(&quot;QLAB03&quot;)){&#13;&#10;  erricsonhost = &quot;10.253.0.105:8200&quot;;&#13;&#10;}&#13;&#10;if(envselector.equals(&quot;QLAB07&quot;)){&#13;&#10;  erricsonhost = &quot;10.253.9.178:8080&quot;;&#13;&#10;}&#13;&#10;&#13;&#10;testExec.setStateValue(&quot;Erricson_Host&quot;,erricsonhost);&#13;&#10;&#13;&#10;&#13;&#10;//Set variables&#13;&#10;/*if(channel.equals(&quot;RETAIL&quot;)){&#13;&#10;&#13;&#10;  senderId = &quot;TMO&quot;;&#13;&#10;  applicationId = &quot;REBELLION&quot;;&#13;&#10;  segChannelId = &quot;In-Person&quot;;&#13;&#10;  segSubChannelId = &quot;Full Service&quot;;&#13;&#10;  segCategoryName = &quot;T-Mobile Retail&quot;;&#13;&#10;  paymentChannel = &quot;REBELLION&quot;;&#13;&#10;}&#13;&#10;if(channel.equals(&quot;CARE&quot;)){&#13;&#10;&#13;&#10;  senderId = &quot;CARE&quot;;&#13;&#10;  applicationId = &quot;SAP-CARE&quot;;&#13;&#10;  segChannelId = &quot;Over the Phone&quot;;&#13;&#10;  segSubChannelId = &quot;Care&quot;;&#13;&#10;  segCategoryName = &quot;Frontline&quot;;&#13;&#10;  paymentChannel = &quot;SAPCARE&quot;;&#13;&#10;}&#13;&#10;if(channel.equals(&quot;WEB&quot;)){&#13;&#10;&#13;&#10;  senderId = &quot;TMO&quot;;&#13;&#10;  applicationId = &quot;REBELLION&quot;;&#13;&#10;  segChannelId = &quot;Unassisted&quot;;&#13;&#10;  segSubChannelId = &quot;Self Service Anonymous&quot;;&#13;&#10;  segCategoryName = &quot;General Anonymous&quot;;&#13;&#10;  paymentChannel = &quot;REBELLION&quot;;&#13;&#10;}&#13;&#10;&#13;&#10;testExec.setStateValue(&quot;SenderId&quot;,senderId);&#13;&#10;testExec.setStateValue(&quot;ApplicationId&quot;,applicationId);&#13;&#10;testExec.setStateValue(&quot;SegChannelId&quot;,segChannelId);&#13;&#10;testExec.setStateValue(&quot;SegSubChannelId&quot;,segSubChannelId);&#13;&#10;testExec.setStateValue(&quot;SegCategoryName&quot;,segCategoryName);&#13;&#10;testExec.setStateValue(&quot;PaymentChannel&quot;,paymentChannel);*/&#13;&#10;return true;&#13;&#10;&#13;&#10;&#13;&#10;&#13;&#10;</script>
    </Node>


    <Node name="Load Request" log=""
          type="com.itko.lisa.test.FileNode" 
          version="1" 
          uid="5320F17088B411EA810D18DBF2466751" 
          think="500-1S" 
          useFilters="true" 
          quiet="true" 
          next="Parse Request" > 

<Loc>{{LISA_RELATIVE_PROJ_ROOT}}/Data/Requests/GenerateCart/generateCartId_{{channelId}}_Request.txt</Loc>
<charset>DEFAULT</charset>
<PropKey>Request</PropKey>
<onFail>Fail Step</onFail>
    </Node>


    <Node name="Parse Request" log=""
          type="com.itko.lisa.utils.ParseTextContentNode" 
          version="1" 
          uid="B98A96CD8BCF11EA816418DBF2466751" 
          think="500-1S" 
          useFilters="true" 
          quiet="true" 
          next="Send Request" > 

<text>e3tSZXF1ZXN0fX0=</text>
<propKey>Request</propKey>
    </Node>


    <Node name="Send Request" log=""
          type="com.itko.lisa.ws.nx.NxWSStep" 
          version="1" 
          uid="45F931FB88A211EA810D18DBF2466751" 
          think="500-1S" 
          useFilters="true" 
          quiet="false" 
          next="Pass Step" > 


      <!-- Filters -->
      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.Send Request.rsp</valueToFilterKey>
<prop>CartID</prop>
<xpathq>/*[local-name()=&apos;Envelope&apos;]/*[local-name()=&apos;Body&apos;]/*[local-name()=&apos;updateCartResponse&apos;]/*[local-name()=&apos;cart&apos;]/@orderId</xpathq>
      </Filter>

      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.Send Request.rsp</valueToFilterKey>
<prop>ResponseCode</prop>
<xpathq>/*[local-name()=&apos;Envelope&apos;]/*[local-name()=&apos;Body&apos;]/*[local-name()=&apos;updateCartResponse&apos;]/*[local-name()=&apos;responseStatus&apos;]/@code</xpathq>
      </Filter>

      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.Send Request.rsp</valueToFilterKey>
<prop>applicationUserId</prop>
<xpathq>/*[local-name()=&apos;Envelope&apos;]/*[local-name()=&apos;Body&apos;]/*[local-name()=&apos;updateCartResponse&apos;]/*[local-name()=&apos;header&apos;]/*[local-name()=&apos;sender&apos;]/*[local-name()=&apos;applicationUserId&apos;]/text()</xpathq>
      </Filter>

      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.Send Request.rsp</valueToFilterKey>
<prop>tillNumber</prop>
<xpathq>/*[local-name()=&apos;Envelope&apos;]/*[local-name()=&apos;Body&apos;]/*[local-name()=&apos;updateCartResponse&apos;]/*[local-name()=&apos;cart&apos;]/*[local-name()=&apos;orderLocation&apos;]/*[local-name()=&apos;tillNumber&apos;]/text()</xpathq>
      </Filter>

      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.Send Request.rsp</valueToFilterKey>
<prop>totalDueAmount</prop>
<xpathq>/*[local-name()=&apos;Envelope&apos;]/*[local-name()=&apos;Body&apos;]/*[local-name()=&apos;updateCartResponse&apos;]/*[local-name()=&apos;cart&apos;]/*[local-name()=&apos;totalAmounts&apos;]/*[local-name()=&apos;totalDueNowAmount&apos;]/*[local-name()=&apos;amount&apos;]/text()</xpathq>
      </Filter>

      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.Send Request.rsp</valueToFilterKey>
<prop>totalTaxAmount</prop>
<xpathq>/*[local-name()=&apos;Envelope&apos;]/*[local-name()=&apos;Body&apos;]/*[local-name()=&apos;updateCartResponse&apos;]/*[local-name()=&apos;cart&apos;]/*[local-name()=&apos;totalAmounts&apos;]/*[local-name()=&apos;totalTaxAmount&apos;]/*[local-name()=&apos;amount&apos;]/text()</xpathq>
      </Filter>

      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.Send Request.rsp</valueToFilterKey>
<prop>orderType</prop>
<xpathq>/*[local-name()=&apos;Envelope&apos;]/*[local-name()=&apos;Body&apos;]/*[local-name()=&apos;updateCartResponse&apos;]/*[local-name()=&apos;cart&apos;]/*[local-name()=&apos;orderType&apos;]/text()</xpathq>
      </Filter>

      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.Send Request.rsp</valueToFilterKey>
<prop>cartItemId</prop>
<xpathq>/*[local-name()=&apos;Envelope&apos;]/*[local-name()=&apos;Body&apos;]/*[local-name()=&apos;updateCartResponse&apos;]/*[local-name()=&apos;cart&apos;]/*[local-name()=&apos;cartItemList&apos;]/*[local-name()=&apos;cartItem&apos;]/*[local-name()=&apos;cartItemId&apos;]/text()</xpathq>
      </Filter>

      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.Send Request.rsp</valueToFilterKey>
<prop>productOfferingId</prop>
<xpathq>/*[local-name()=&apos;Envelope&apos;]/*[local-name()=&apos;Body&apos;]/*[local-name()=&apos;updateCartResponse&apos;]/*[local-name()=&apos;cart&apos;]/*[local-name()=&apos;cartItemList&apos;]/*[local-name()=&apos;cartItem&apos;]/*[local-name()=&apos;productOffering&apos;]/@productOfferingId</xpathq>
      </Filter>

      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.Send Request.rsp</valueToFilterKey>
<prop>productOfferingLabel</prop>
<xpathq>/*[local-name()=&apos;Envelope&apos;]/*[local-name()=&apos;Body&apos;]/*[local-name()=&apos;updateCartResponse&apos;]/*[local-name()=&apos;cart&apos;]/*[local-name()=&apos;cartItemList&apos;]/*[local-name()=&apos;cartItem&apos;]/*[local-name()=&apos;productOffering&apos;]/*[local-name()=&apos;label&apos;]/text()</xpathq>
      </Filter>

      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.Send Request.rsp</valueToFilterKey>
<prop>totalChargeAmount</prop>
<xpathq>/*[local-name()=&apos;Envelope&apos;]/*[local-name()=&apos;Body&apos;]/*[local-name()=&apos;updateCartResponse&apos;]/*[local-name()=&apos;cart&apos;]/*[local-name()=&apos;totalAmounts&apos;]/*[local-name()=&apos;totalChargeAmount&apos;]/*[local-name()=&apos;amount&apos;]/text()</xpathq>
      </Filter>

      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.Send Request.rsp</valueToFilterKey>
<prop>productType</prop>
<xpathq>/*[local-name()=&apos;Envelope&apos;]/*[local-name()=&apos;Body&apos;]/*[local-name()=&apos;updateCartResponse&apos;]/*[local-name()=&apos;cart&apos;]/*[local-name()=&apos;cartItemList&apos;]/*[local-name()=&apos;cartItem&apos;]/*[local-name()=&apos;productOffering&apos;]/*[local-name()=&apos;attribute&apos;][@name=&quot;productType&quot;]/*[local-name()=&apos;value&apos;]/text()</xpathq>
      </Filter>

      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.Send Request.rsp</valueToFilterKey>
<prop>offerType</prop>
<xpathq>/*[local-name()=&apos;Envelope&apos;]/*[local-name()=&apos;Body&apos;]/*[local-name()=&apos;updateCartResponse&apos;]/*[local-name()=&apos;cart&apos;]/*[local-name()=&apos;cartItemList&apos;]/*[local-name()=&apos;cartItem&apos;]/*[local-name()=&apos;productOffering&apos;]/*[local-name()=&apos;attribute&apos;][@name=&quot;serviceType&quot;]/*[local-name()=&apos;value&apos;]/text()</xpathq>
      </Filter>

      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.Send Request.rsp</valueToFilterKey>
<prop>offerSubType</prop>
<xpathq>/*[local-name()=&apos;Envelope&apos;]/*[local-name()=&apos;Body&apos;]/*[local-name()=&apos;updateCartResponse&apos;]/*[local-name()=&apos;cart&apos;]/*[local-name()=&apos;cartItemList&apos;]/*[local-name()=&apos;cartItem&apos;]/*[local-name()=&apos;productOffering&apos;]/*[local-name()=&apos;attribute&apos;][@name=&quot;offerSubType&quot;]/*[local-name()=&apos;value&apos;]/text()</xpathq>
      </Filter>

      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.Send Request.rsp</valueToFilterKey>
<prop>productSubType</prop>
<xpathq>/*[local-name()=&apos;Envelope&apos;]/*[local-name()=&apos;Body&apos;]/*[local-name()=&apos;updateCartResponse&apos;]/*[local-name()=&apos;cart&apos;]/*[local-name()=&apos;cartItemList&apos;]/*[local-name()=&apos;cartItem&apos;]/*[local-name()=&apos;productOffering&apos;]/*[local-name()=&apos;attribute&apos;][@name=&quot;productSubType&quot;]/*[local-name()=&apos;value&apos;]/text()</xpathq>
      </Filter>

      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.Send Request.rsp</valueToFilterKey>
<prop>offeringVariantId</prop>
<xpathq>/*[local-name()=&apos;Envelope&apos;]/*[local-name()=&apos;Body&apos;]/*[local-name()=&apos;updateCartResponse&apos;]/*[local-name()=&apos;cart&apos;]/*[local-name()=&apos;cartItemList&apos;]/*[local-name()=&apos;cartItem&apos;]/*[local-name()=&apos;productOffering&apos;]/*[local-name()=&apos;sku&apos;]/@code</xpathq>
      </Filter>

      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.Send Request.rsp</valueToFilterKey>
<prop>sku</prop>
<xpathq>/*[local-name()=&apos;Envelope&apos;]/*[local-name()=&apos;Body&apos;]/*[local-name()=&apos;updateCartResponse&apos;]/*[local-name()=&apos;cart&apos;]/*[local-name()=&apos;cartItemList&apos;]/*[local-name()=&apos;cartItem&apos;]/*[local-name()=&apos;productOffering&apos;]/*[local-name()=&apos;sku&apos;]/*[local-name()=&apos;attribute&apos;][@name=&apos;sku&apos;]/*[local-name()=&apos;value&apos;]/text()</xpathq>
      </Filter>


      <!-- Assertions -->
<CheckResult assertTrue="false" name="Check Response Code" type="com.itko.lisa.test.CheckResultPropRegEx">
<log>Assertion name: Check Response Code checks for: false is of type: Property Value Expression.</log>
<then>Fail Step</then>
<valueToAssertKey></valueToAssertKey>
        <prop>ResponseCode</prop>
        <param>100</param>
</CheckResult>

<wsdl>{{LISA_RELATIVE_PROJ_URL}}/Data/WSDL/CartWSIL_Golv.wsdl</wsdl>
<endpoint>http://{{Erricson_Host}}/cwf/services/CartON?wsdl</endpoint>
<targetNamespace>http://services.tmobile.com/OrderManagement/CartWSIL/V1</targetNamespace>
<service>CartWSIL</service>
<port>CartWSIL</port>
<operation>updateCart</operation>
<onError>Fail Step</onError>
<maintainSession>true</maintainSession>
<clearSession>false</clearSession>
<request itko_enc="base64">e3tSZXF1ZXN0fX0=</request>
<style>document</style>
<use>literal</use>
<soapAction>updateCart</soapAction>
<sslInfo>
<ssl-keystore-file></ssl-keystore-file>
<ssl-keystore-password-enc>l161dfc6631b360e46df38f376d0478e528f4dfc60b9cc33fd6ea1b386a416e78</ssl-keystore-password-enc>
<ssl-alias></ssl-alias>
<ssl-key-password-enc>l2a7b9b8c7336440126116469da6221f1eea899d1569f16977b3f657c49732aef</ssl-key-password-enc>
</sslInfo>
<wsiInfo>
<wsi-check-wsdl>false</wsi-check-wsdl>
<wsi-check-msg>false</wsi-check-msg>
<wsi-display-type>onlyFailed</wsi-display-type>
<wsi-on-fail>continue</wsi-on-fail>
</wsiInfo>
<transport>
<soapVersionUri>1.1</soapVersionUri>
<httpVersionUri>1.1</httpVersionUri>
<webMethodUri>POST</webMethodUri>
<mtom>false</mtom>
<dontSendRequest>false</dontSendRequest>
<fault-as-error>true</fault-as-error>
<callTimeout>30000</callTimeout>
</transport>
<uddiActive>false</uddiActive>
<uddi-result>
<uddi-selection>
</uddi-selection>
</uddi-result>
<wss4jInfo>
<version>5</version>
<wssecurity>
<isSend>true</isSend>
<must>false</must>
<role></role>
</wssecurity>
<wsi-bsp>false</wsi-bsp>
</wss4jInfo>
<wsaInfo>
<useAddressing>false</useAddressing>
<must>false</must>
<to></to>
<toOverride>false</toOverride>
<toDefault>true</toDefault>
<from></from>
<fromOverride>false</fromOverride>
<fromDefault>true</fromDefault>
<action></action>
<actionOverride>false</actionOverride>
<actionDefault>true</actionDefault>
<msgid></msgid>
<msgidOverride>false</msgidOverride>
<msgidDefault>true</msgidDefault>
<replyTo></replyTo>
<replyToOverride>false</replyToOverride>
<faultTo></faultTo>
<faulttToOverride>false</faulttToOverride>
<addressingVersion>http://www.w3.org/2005/08/addressing</addressingVersion>
</wsaInfo>
<customHTTPHeaderInfo>
</customHTTPHeaderInfo>
<attachments>
<attachment-type>MIME</attachment-type>
</attachments>
<mtom-paths>
</mtom-paths>
    </Node>


    <Node name="Pass Step" log=""
          type="com.itko.lisa.test.UserScriptNode" 
          version="1" 
          uid="AAD717B288C111EA810D18DBF2466751" 
          think="500-1S" 
          useFilters="true" 
          quiet="false" 
          next="end" > 


      <!-- Assertions -->
<CheckResult assertTrue="true" name="Any Exception Then Fail" type="com.itko.lisa.dynexec.CheckInvocationEx">
<log>Assertion name: Any Exception Then Fail checks for: true is of type: Assert on Invocation Exception.</log>
<then>continue</then>
<valueToAssertKey></valueToAssertKey>
        <param>.*</param>
</CheckResult>

<onerror>continue</onerror>
<language>BeanShell</language>
<copyProps>TestExecProps</copyProps>
<script>&#13;&#10;testExec.setStateValue(&quot;FailureDescription&quot;,&quot;&quot;);&#13;&#10;testExec.setStateValue(&quot;Test_Execution_Status&quot;,&quot;Passed&quot;);&#13;&#10;return true;</script>
    </Node>


    <Node name="Fail Step" log=""
          type="com.itko.lisa.test.UserScriptNode" 
          version="1" 
          uid="E8FA3A7C88C111EA810D18DBF2466751" 
          think="500-1S" 
          useFilters="true" 
          quiet="false" 
          next="end" > 


      <!-- Assertions -->
<CheckResult assertTrue="true" name="Any Exception Then Fail" type="com.itko.lisa.dynexec.CheckInvocationEx">
<log>Assertion name: Any Exception Then Fail checks for: true is of type: Assert on Invocation Exception.</log>
<then>continue</then>
<valueToAssertKey></valueToAssertKey>
        <param>.*</param>
</CheckResult>

<onerror>continue</onerror>
<language>BeanShell</language>
<copyProps>TestExecProps</copyProps>
<script>&#13;&#10;testExec.setStateValue(&quot;FailureDescription&quot;,&quot;Cart Id is not generated correctly.&quot;);&#13;&#10;testExec.setStateValue(&quot;Test_Execution_Status&quot;,&quot;Failed&quot;);&#13;&#10;testExec.setStateValue(&quot;STEPNAME&quot;,&quot;Subprocess GenerateCartId&quot;);&#13;&#10;return true;</script>
    </Node>


    <Node name="Load Request2" log=""
          type="com.itko.lisa.test.FileNode" 
          version="1" 
          uid="93FDDD2688B411EA810D18DBF2466751" 
          think="500-1S" 
          useFilters="true" 
          quiet="true" 
          next="Parse Request2" > 

<Loc>{{LISA_RELATIVE_PROJ_ROOT}}/Data/Requests/GenerateCart/generateCartId_{{channelId}}_Request2.txt</Loc>
<charset>DEFAULT</charset>
<PropKey>Request2</PropKey>
<onFail>Fail Step</onFail>
    </Node>


    <Node name="Parse Request2" log=""
          type="com.itko.lisa.utils.ParseTextContentNode" 
          version="1" 
          uid="D5BA78948BCF11EA816418DBF2466751" 
          think="500-1S" 
          useFilters="true" 
          quiet="true" 
          next="Send Request 2" > 

<text>e3tSZXF1ZXN0Mn19</text>
<propKey>Request2</propKey>
    </Node>


    <Node name="Send Request 2" log=""
          type="com.itko.lisa.ws.nx.NxWSStep" 
          version="1" 
          uid="5F8F82E688B511EA810D18DBF2466751" 
          think="500-1S" 
          useFilters="true" 
          quiet="false" 
          next="Load Request3" > 

<wsdl>{{LISA_RELATIVE_PROJ_URL}}/Data/WSDL/CartWSIL_Golv.wsdl</wsdl>
<endpoint>http://{{Erricson_Host}}/cwf/services/CartON?wsdl</endpoint>
<targetNamespace>http://services.tmobile.com/OrderManagement/CartWSIL/V1</targetNamespace>
<service>CartWSIL</service>
<port>CartWSIL</port>
<operation>updateCart</operation>
<onError>Fail Step</onError>
<maintainSession>true</maintainSession>
<clearSession>false</clearSession>
<request itko_enc="base64">e3tSZXF1ZXN0Mn19</request>
<style>document</style>
<use>literal</use>
<soapAction>updateCart</soapAction>
<sslInfo>
<ssl-keystore-password-enc>l62cb1359369805cdbb5df1e912deaeb526da78b766b443ecfcbf52f45d365afa</ssl-keystore-password-enc>
<ssl-key-password-enc>ldff0ede69904e7e84d277cc867e9361cd802364326d374d6834b4659617cf80b</ssl-key-password-enc>
</sslInfo>
<wsiInfo>
<wsi-check-wsdl>false</wsi-check-wsdl>
<wsi-check-msg>false</wsi-check-msg>
<wsi-display-type>onlyFailed</wsi-display-type>
<wsi-on-fail>continue</wsi-on-fail>
</wsiInfo>
<transport>
<soapVersionUri>1.1</soapVersionUri>
<httpVersionUri>1.1</httpVersionUri>
<webMethodUri>POST</webMethodUri>
<mtom>false</mtom>
<dontSendRequest>false</dontSendRequest>
<fault-as-error>true</fault-as-error>
<callTimeout>30000</callTimeout>
</transport>
<uddiActive>false</uddiActive>
<uddi-result>
<uddi-selection>
</uddi-selection>
</uddi-result>
<wss4jInfo>
<version>5</version>
<wssecurity>
<isSend>true</isSend>
<must>false</must>
</wssecurity>
<wsi-bsp>false</wsi-bsp>
</wss4jInfo>
<wsaInfo>
<useAddressing>false</useAddressing>
<must>false</must>
<to></to>
<toOverride>false</toOverride>
<toDefault>true</toDefault>
<from></from>
<fromOverride>false</fromOverride>
<fromDefault>true</fromDefault>
<action></action>
<actionOverride>false</actionOverride>
<actionDefault>true</actionDefault>
<msgid></msgid>
<msgidOverride>false</msgidOverride>
<msgidDefault>true</msgidDefault>
<replyTo></replyTo>
<replyToOverride>false</replyToOverride>
<faultTo></faultTo>
<faulttToOverride>false</faulttToOverride>
<addressingVersion>http://www.w3.org/2005/08/addressing</addressingVersion>
</wsaInfo>
<customHTTPHeaderInfo>
</customHTTPHeaderInfo>
<attachments>
<attachment-type>MIME</attachment-type>
</attachments>
<mtom-paths>
</mtom-paths>
    </Node>


    <Node name="Load Request3" log=""
          type="com.itko.lisa.test.FileNode" 
          version="1" 
          uid="9584562188B411EA810D18DBF2466751" 
          think="500-1S" 
          useFilters="true" 
          quiet="true" 
          next="Parse Request3" > 

<Loc>{{LISA_RELATIVE_PROJ_ROOT}}/Data/Requests/GenerateCart/generateCartId_{{channelId}}_Request3.txt</Loc>
<charset>DEFAULT</charset>
<PropKey>Request3</PropKey>
<onFail>Fail Step</onFail>
    </Node>


    <Node name="Parse Request3" log=""
          type="com.itko.lisa.utils.ParseTextContentNode" 
          version="1" 
          uid="E64083588BCF11EA816418DBF2466751" 
          think="500-1S" 
          useFilters="true" 
          quiet="true" 
          next="Send Request 3" > 

<text>e3tSZXF1ZXN0M319</text>
<propKey>Request3</propKey>
    </Node>


    <Node name="Send Request 3" log=""
          type="com.itko.lisa.ws.nx.NxWSStep" 
          version="1" 
          uid="6C800AD188B511EA810D18DBF2466751" 
          think="500-1S" 
          useFilters="true" 
          quiet="false" 
          next="Load Request4" > 

<wsdl>{{LISA_RELATIVE_PROJ_URL}}/Data/WSDL/CartWSIL_Golv.wsdl</wsdl>
<endpoint>http://{{Erricson_Host}}/cwf/services/CartON?wsdl</endpoint>
<targetNamespace>http://services.tmobile.com/OrderManagement/CartWSIL/V1</targetNamespace>
<service>CartWSIL</service>
<port>CartWSIL</port>
<operation>updateCart</operation>
<onError>Fail Step</onError>
<maintainSession>true</maintainSession>
<clearSession>false</clearSession>
<request itko_enc="base64">e3tSZXF1ZXN0M319</request>
<style>document</style>
<use>literal</use>
<soapAction>updateCart</soapAction>
<sslInfo>
<ssl-keystore-password-enc>l5c8a82ea5992a09252ef7d80aa10748884d6831a2e0aa3f0de29e8c487c530d4</ssl-keystore-password-enc>
<ssl-key-password-enc>l0ea331c0897fec72ff46adeb509dacf288f29d48258a638867dec89e8c238164</ssl-key-password-enc>
</sslInfo>
<wsiInfo>
<wsi-check-wsdl>false</wsi-check-wsdl>
<wsi-check-msg>false</wsi-check-msg>
<wsi-display-type>onlyFailed</wsi-display-type>
<wsi-on-fail>continue</wsi-on-fail>
</wsiInfo>
<transport>
<soapVersionUri>1.1</soapVersionUri>
<httpVersionUri>1.1</httpVersionUri>
<webMethodUri>POST</webMethodUri>
<mtom>false</mtom>
<dontSendRequest>false</dontSendRequest>
<fault-as-error>true</fault-as-error>
<callTimeout>30000</callTimeout>
</transport>
<uddiActive>false</uddiActive>
<uddi-result>
<uddi-selection>
</uddi-selection>
</uddi-result>
<wss4jInfo>
<version>5</version>
<wssecurity>
<isSend>true</isSend>
<must>false</must>
</wssecurity>
<wsi-bsp>false</wsi-bsp>
</wss4jInfo>
<wsaInfo>
<useAddressing>false</useAddressing>
<must>false</must>
<to></to>
<toOverride>false</toOverride>
<toDefault>true</toDefault>
<from></from>
<fromOverride>false</fromOverride>
<fromDefault>true</fromDefault>
<action></action>
<actionOverride>false</actionOverride>
<actionDefault>true</actionDefault>
<msgid></msgid>
<msgidOverride>false</msgidOverride>
<msgidDefault>true</msgidDefault>
<replyTo></replyTo>
<replyToOverride>false</replyToOverride>
<faultTo></faultTo>
<faulttToOverride>false</faulttToOverride>
<addressingVersion>http://www.w3.org/2005/08/addressing</addressingVersion>
</wsaInfo>
<customHTTPHeaderInfo>
</customHTTPHeaderInfo>
<attachments>
<attachment-type>MIME</attachment-type>
</attachments>
<mtom-paths>
</mtom-paths>
    </Node>


    <Node name="Load Request4" log=""
          type="com.itko.lisa.test.FileNode" 
          version="1" 
          uid="96C53A9C88B411EA810D18DBF2466751" 
          think="500-1S" 
          useFilters="true" 
          quiet="true" 
          next="Parse Request4" > 

<Loc>{{LISA_RELATIVE_PROJ_ROOT}}/Data/Requests/GenerateCart/generateCartId_{{channelId}}_Request4.txt</Loc>
<charset>DEFAULT</charset>
<PropKey>Request4</PropKey>
<onFail>Fail Step</onFail>
    </Node>


    <Node name="Parse Request4" log=""
          type="com.itko.lisa.utils.ParseTextContentNode" 
          version="1" 
          uid="F305DC918BCF11EA816418DBF2466751" 
          think="500-1S" 
          useFilters="true" 
          quiet="true" 
          next="Send Request 4" > 

<text>e3tSZXF1ZXN0NH19</text>
<propKey>Request4</propKey>
    </Node>


    <Node name="Send Request 4" log=""
          type="com.itko.lisa.ws.nx.NxWSStep" 
          version="1" 
          uid="767C09DC88B511EA810D18DBF2466751" 
          think="500-1S" 
          useFilters="true" 
          quiet="false" 
          next="Load Request5" > 

<wsdl>{{LISA_RELATIVE_PROJ_URL}}/Data/WSDL/CartWSIL_Golv.wsdl</wsdl>
<endpoint>http://{{Erricson_Host}}/cwf/services/CartON?wsdl</endpoint>
<targetNamespace>http://services.tmobile.com/OrderManagement/CartWSIL/V1</targetNamespace>
<service>CartWSIL</service>
<port>CartWSIL</port>
<operation>updateCart</operation>
<onError>Fail Step</onError>
<maintainSession>true</maintainSession>
<clearSession>false</clearSession>
<request itko_enc="base64">e3tSZXF1ZXN0NH19</request>
<style>document</style>
<use>literal</use>
<soapAction>updateCart</soapAction>
<sslInfo>
<ssl-keystore-password-enc>ldd2334aad03e999baa1599cedb081810494c907cd7fb8eb395007bdc291703ff</ssl-keystore-password-enc>
<ssl-key-password-enc>l56b271cd3124622ba0210f487dd909be65efeb99e349915468695404b765c871</ssl-key-password-enc>
</sslInfo>
<wsiInfo>
<wsi-check-wsdl>false</wsi-check-wsdl>
<wsi-check-msg>false</wsi-check-msg>
<wsi-display-type>onlyFailed</wsi-display-type>
<wsi-on-fail>continue</wsi-on-fail>
</wsiInfo>
<transport>
<soapVersionUri>1.1</soapVersionUri>
<httpVersionUri>1.1</httpVersionUri>
<webMethodUri>POST</webMethodUri>
<mtom>false</mtom>
<dontSendRequest>false</dontSendRequest>
<fault-as-error>true</fault-as-error>
<callTimeout>30000</callTimeout>
</transport>
<uddiActive>false</uddiActive>
<uddi-result>
<uddi-selection>
</uddi-selection>
</uddi-result>
<wss4jInfo>
<version>5</version>
<wssecurity>
<isSend>true</isSend>
<must>false</must>
</wssecurity>
<wsi-bsp>false</wsi-bsp>
</wss4jInfo>
<wsaInfo>
<useAddressing>false</useAddressing>
<must>false</must>
<to></to>
<toOverride>false</toOverride>
<toDefault>true</toDefault>
<from></from>
<fromOverride>false</fromOverride>
<fromDefault>true</fromDefault>
<action></action>
<actionOverride>false</actionOverride>
<actionDefault>true</actionDefault>
<msgid></msgid>
<msgidOverride>false</msgidOverride>
<msgidDefault>true</msgidDefault>
<replyTo></replyTo>
<replyToOverride>false</replyToOverride>
<faultTo></faultTo>
<faulttToOverride>false</faulttToOverride>
<addressingVersion>http://www.w3.org/2005/08/addressing</addressingVersion>
</wsaInfo>
<customHTTPHeaderInfo>
</customHTTPHeaderInfo>
<attachments>
<attachment-type>MIME</attachment-type>
</attachments>
<mtom-paths>
</mtom-paths>
    </Node>


    <Node name="Load Request5" log=""
          type="com.itko.lisa.test.FileNode" 
          version="1" 
          uid="984D604A88B411EA810D18DBF2466751" 
          think="500-1S" 
          useFilters="true" 
          quiet="true" 
          next="Parse Request5" > 

<Loc>{{LISA_RELATIVE_PROJ_ROOT}}/Data/Requests/GenerateCart/generateCartId_{{channelId}}_Request5.txt</Loc>
<charset>DEFAULT</charset>
<PropKey>Request5</PropKey>
<onFail>Fail Step</onFail>
    </Node>


    <Node name="Parse Request5" log=""
          type="com.itko.lisa.utils.ParseTextContentNode" 
          version="1" 
          uid="43F80E9B8BD011EA816418DBF2466751" 
          think="500-1S" 
          useFilters="true" 
          quiet="true" 
          next="Send Request 5" > 

<text>e3tSZXF1ZXN0NX19</text>
<propKey>Request5</propKey>
    </Node>


    <Node name="Send Request 5" log=""
          type="com.itko.lisa.ws.nx.NxWSStep" 
          version="1" 
          uid="826B4E6588B511EA810D18DBF2466751" 
          think="500-1S" 
          useFilters="true" 
          quiet="false" 
          next="Pass Step" > 


      <!-- Assertions -->
<CheckResult assertTrue="true" name="check if RETAIL Channel" type="com.itko.lisa.test.CheckResultPropRegEx">
<log>Assertion name: check if RETAIL Channel checks for: true is of type: Property Value Expression.</log>
<then>Load Request6</then>
<valueToAssertKey></valueToAssertKey>
        <prop>channelId</prop>
        <param>RETAIL</param>
</CheckResult>

<wsdl>{{LISA_RELATIVE_PROJ_URL}}/Data/WSDL/CartWSIL_Golv.wsdl</wsdl>
<endpoint>http://{{Erricson_Host}}/cwf/services/CartON?wsdl</endpoint>
<targetNamespace>http://services.tmobile.com/OrderManagement/CartWSIL/V1</targetNamespace>
<service>CartWSIL</service>
<port>CartWSIL</port>
<operation>updateCart</operation>
<onError>Fail Step</onError>
<maintainSession>true</maintainSession>
<clearSession>false</clearSession>
<request itko_enc="base64">e3tSZXF1ZXN0NX19</request>
<style>document</style>
<use>literal</use>
<soapAction>updateCart</soapAction>
<sslInfo>
<ssl-keystore-password-enc>lc9afb08a573acefca1d6c1548db15192f339457a9ebd851ec58af2171d09a025</ssl-keystore-password-enc>
<ssl-key-password-enc>l4cdfe6c62a5d379bca67decdecce8179b9fe4f88e3c3e55fe050e062e3b7d594</ssl-key-password-enc>
</sslInfo>
<wsiInfo>
<wsi-check-wsdl>false</wsi-check-wsdl>
<wsi-check-msg>false</wsi-check-msg>
<wsi-display-type>onlyFailed</wsi-display-type>
<wsi-on-fail>continue</wsi-on-fail>
</wsiInfo>
<transport>
<soapVersionUri>1.1</soapVersionUri>
<httpVersionUri>1.1</httpVersionUri>
<webMethodUri>POST</webMethodUri>
<mtom>false</mtom>
<dontSendRequest>false</dontSendRequest>
<fault-as-error>true</fault-as-error>
<callTimeout>30000</callTimeout>
</transport>
<uddiActive>false</uddiActive>
<uddi-result>
<uddi-selection>
</uddi-selection>
</uddi-result>
<wss4jInfo>
<version>5</version>
<wssecurity>
<isSend>true</isSend>
<must>false</must>
</wssecurity>
<wsi-bsp>false</wsi-bsp>
</wss4jInfo>
<wsaInfo>
<useAddressing>false</useAddressing>
<must>false</must>
<to></to>
<toOverride>false</toOverride>
<toDefault>true</toDefault>
<from></from>
<fromOverride>false</fromOverride>
<fromDefault>true</fromDefault>
<action></action>
<actionOverride>false</actionOverride>
<actionDefault>true</actionDefault>
<msgid></msgid>
<msgidOverride>false</msgidOverride>
<msgidDefault>true</msgidDefault>
<replyTo></replyTo>
<replyToOverride>false</replyToOverride>
<faultTo></faultTo>
<faulttToOverride>false</faulttToOverride>
<addressingVersion>http://www.w3.org/2005/08/addressing</addressingVersion>
</wsaInfo>
<customHTTPHeaderInfo>
</customHTTPHeaderInfo>
<attachments>
<attachment-type>MIME</attachment-type>
</attachments>
<mtom-paths>
</mtom-paths>
    </Node>


    <Node name="Load Request6" log=""
          type="com.itko.lisa.test.FileNode" 
          version="1" 
          uid="99D9578588B411EA810D18DBF2466751" 
          think="500-1S" 
          useFilters="true" 
          quiet="true" 
          next="Parse Request6" > 

<Loc>{{LISA_RELATIVE_PROJ_ROOT}}/Data/Requests/GenerateCart/generateCartId_{{ChannelId}}_Request6.txt</Loc>
<charset>DEFAULT</charset>
<PropKey>Request6</PropKey>
<onFail>Fail Step</onFail>
    </Node>


    <Node name="Parse Request6" log=""
          type="com.itko.lisa.utils.ParseTextContentNode" 
          version="1" 
          uid="5005D7A48BD011EA816418DBF2466751" 
          think="500-1S" 
          useFilters="true" 
          quiet="true" 
          next="Send Request 6" > 

<text>e3tSZXF1ZXN0Nn19</text>
<propKey>Request6</propKey>
    </Node>


    <Node name="Send Request 6" log=""
          type="com.itko.lisa.ws.nx.NxWSStep" 
          version="1" 
          uid="8E2997DF88B511EA810D18DBF2466751" 
          think="500-1S" 
          useFilters="true" 
          quiet="false" 
          next="Pass Step" > 

<wsdl>{{LISA_RELATIVE_PROJ_URL}}/Data/WSDL/CartWSIL_Golv.wsdl</wsdl>
<endpoint>http://{{Ericson_Host}}/cwf/services/CartON?wsdl</endpoint>
<targetNamespace>http://services.tmobile.com/OrderManagement/CartWSIL/V1</targetNamespace>
<service>CartWSIL</service>
<port>CartWSIL</port>
<operation>updateCart</operation>
<onError>Fail Step</onError>
<maintainSession>true</maintainSession>
<clearSession>false</clearSession>
<request itko_enc="base64">e3tSZXF1ZXN0Nn19</request>
<style>document</style>
<use>literal</use>
<soapAction>updateCart</soapAction>
<sslInfo>
<ssl-keystore-password-enc>la0dd44abc448929941f5fc7d4fb3794298bdd20420728d372542bc2db2390c34</ssl-keystore-password-enc>
<ssl-key-password-enc>l10d91413ae162d94bf930ab9bb3888b0fe8e37e496e9e27db8d0487aa220ce8c</ssl-key-password-enc>
</sslInfo>
<wsiInfo>
<wsi-check-wsdl>false</wsi-check-wsdl>
<wsi-check-msg>false</wsi-check-msg>
<wsi-display-type>onlyFailed</wsi-display-type>
<wsi-on-fail>continue</wsi-on-fail>
</wsiInfo>
<transport>
<soapVersionUri>1.1</soapVersionUri>
<httpVersionUri>1.1</httpVersionUri>
<webMethodUri>POST</webMethodUri>
<mtom>false</mtom>
<dontSendRequest>false</dontSendRequest>
<fault-as-error>true</fault-as-error>
<callTimeout>30000</callTimeout>
</transport>
<uddiActive>false</uddiActive>
<uddi-result>
<uddi-selection>
</uddi-selection>
</uddi-result>
<wss4jInfo>
<version>5</version>
<wssecurity>
<isSend>true</isSend>
<must>false</must>
</wssecurity>
<wsi-bsp>false</wsi-bsp>
</wss4jInfo>
<wsaInfo>
<useAddressing>false</useAddressing>
<must>false</must>
<to></to>
<toOverride>false</toOverride>
<toDefault>true</toDefault>
<from></from>
<fromOverride>false</fromOverride>
<fromDefault>true</fromDefault>
<action></action>
<actionOverride>false</actionOverride>
<actionDefault>true</actionDefault>
<msgid></msgid>
<msgidOverride>false</msgidOverride>
<msgidDefault>true</msgidDefault>
<replyTo></replyTo>
<replyToOverride>false</replyToOverride>
<faultTo></faultTo>
<faulttToOverride>false</faulttToOverride>
<addressingVersion>http://www.w3.org/2005/08/addressing</addressingVersion>
</wsaInfo>
<customHTTPHeaderInfo>
</customHTTPHeaderInfo>
<attachments>
<attachment-type>MIME</attachment-type>
</attachments>
<mtom-paths>
</mtom-paths>
    </Node>


    <Node name="end" log=""
          type="com.itko.lisa.test.NormalEnd" 
          version="1" 
          uid="B32A5E39889E11EA810D18DBF2466751" 
          think="0h" 
          useFilters="true" 
          quiet="true" 
          next="fail" > 

    </Node>


    <Node name="fail" log=""
          type="com.itko.lisa.test.Abend" 
          version="1" 
          uid="B32A5E37889E11EA810D18DBF2466751" 
          think="0h" 
          useFilters="true" 
          quiet="true" 
          next="abort" > 

    </Node>


    <Node name="abort" log=""
          type="com.itko.lisa.test.AbortStep" 
          version="1" 
          uid="B32A5E35889E11EA810D18DBF2466751" 
          think="0h" 
          useFilters="true" 
          quiet="true" 
          next="" > 

    </Node>


</TestCase>
