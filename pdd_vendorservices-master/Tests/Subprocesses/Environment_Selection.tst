<?xml version="1.0" ?>

<TestCase name="Environment_Selection" version="5">

<meta>
   <create version="10.3.0" buildNumber="10.3.0.297" author="asanka3" date="10/19/2018" host="AMUSLT17532" />
   <lastEdited version="10.3.0" buildNumber="10.3.0.297" author="MKomala2" date="07/27/2020" host="LP-5CD7355fj7" />
</meta>

<id>7E4D85C4D3AE11E8B39F2AC220524153</id>
<Documentation>Put documentation of the Test Case here.</Documentation>
<IsInProject>true</IsInProject>
<sig>ZWQ9NSZ0Y3Y9NSZsaXNhdj0xMC4zLjAgKDEwLjMuMC4yOTcpJm5vZGVzPS0xNjU2MzQ3NjA0</sig>
<subprocess>true</subprocess>

<initState>
    <Parameter>
    <key>EnvironmentTested</key>
    <value>{{EnvironmentTested}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
</initState>

<resultState>
    <Parameter>
    <key>BW_Host</key>
    <value></value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>CE_Host</key>
    <value></value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>OMSURL</key>
    <value></value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
</resultState>

<deletedProps>
    <key>DC02DBUser</key>
    <key>ProdJDBCDriver</key>
    <key>ENDPOINT1</key>
    <key>LocalPath</key>
    <key>StgConnectString</key>
    <key>OMNISCHEMA</key>
    <key>WSPORT1</key>
    <key>JenkinsJobName</key>
    <key>lisa.Execute script (JSR-223).rsp</key>
    <key>html_report_path</key>
    <key>WSSERVER1</key>
    <key>WSSERVER2</key>
    <key>EnvironmentTestedE2E</key>
    <key>ODCAppTestJenkinsJobPath</key>
    <key>WCSURI</key>
    <key>ExecutionMode</key>
    <key>GETLOAN_WAIT</key>
    <key>EnvironmentTested</key>
    <key>OFSLL_WAIT</key>
    <key>ProdDBPwd</key>
    <key>OMNIJDBCDriver</key>
    <key>Tdm_Env</key>
    <key>lisa.Execute script (JSR-223).rsp.time</key>
    <key>HandlerName</key>
    <key>ODCSmokeTestJenkinsJobPath</key>
    <key>StgJDBCDriver</key>
    <key>StgDBUser</key>
    <key>OMSJDBCDriver</key>
    <key>OMNIDBID</key>
    <key>Result_Path</key>
    <key>ProdDBSchema</key>
    <key>OMNIDBPass</key>
    <key>URIFlag</key>
    <key>DC02ConnectString</key>
    <key>WSSERVER</key>
    <key>TestSetId</key>
    <key>DC02JDBCDriver</key>
    <key>LOCDIR</key>
    <key>JenkinsJobPath</key>
    <key>JenkinsJobPath2</key>
    <key>JenkinsJobPath3</key>
    <key>OMNIConnectionstring</key>
    <key>JenkinsJobPath1</key>
    <key>OMSDBPass</key>
    <key>OMSDBID</key>
    <key>WSPORT</key>
    <key>Key1</key>
    <key>Type</key>
    <key>ProdDBUser</key>
    <key>ProdConnectString</key>
    <key>ENDPOINT</key>
    <key>OMSConnectionString</key>
    <key>lisa.vse.execution.mode</key>
</deletedProps>

    <Node name="Execute script (JSR-223)" log=""
          type="com.itko.lisa.test.UserScriptNode" 
          version="1" 
          uid="961A1E2ED3DC11E8A88A8E5720524153" 
          think="500-1S" 
          useFilters="true" 
          quiet="false" 
          next="end" > 


      <!-- Assertions -->
<CheckResult assertTrue="true" name="Any Exception Then Fail" type="com.itko.lisa.dynexec.CheckInvocationEx">
<log>Assertion name: Any Exception Then Fail checks for: true is of type: Assert on Invocation Exception.</log>
<then>fail</then>
<valueToAssertKey></valueToAssertKey>
        <param>.*</param>
</CheckResult>

<onerror>continue</onerror>
<language>BeanShell</language>
<copyProps>TestExecProps</copyProps>
<script>envselector = testExec.getStateValue(&quot;EnvironmentTested&quot;);&#13;&#10;&#13;&#10;if(envselector.equals(&quot;QLAB02&quot;)){&#13;&#10;    bw_host=&quot;qatenbw132.unix.gsm1900.org:9971&quot;;&#13;&#10;    ce_host1=&quot;vendorservices-qlab02-duti-blue.test.px-npe02b.cf.t-mobile.com&quot;;&#13;&#10;    ce_host=&quot;vendorservices-qlab02-duti-green.test.px-npe02b.cf.t-mobile.com&quot;;&#13;&#10;    OMSURL_VAL=&quot;https://u2oms-q2.px-npe1102.pks.t-mobile.com/yantrawebservices/services/YIFWebService&quot;;&#13;&#10;}&#13;&#10;&#13;&#10;if(envselector.equals(&quot;QLAB03&quot;)){&#13;&#10;    bw_host=&quot;qlab03-duti-blue.test.px-npe02b.cf.t-mobile.com&quot;;&#13;&#10;    ce_host=&quot;qlab03-duti-green.test.px-npe02b.cf.t-mobile.com&quot;;&#13;&#10;    OMSURL_VAL=&quot;https://u2oms-q3.px-npe1102.pks.t-mobile.com/yantrawebservices/services/YIFWebService&quot;;&#13;&#10;&#13;&#10;}&#13;&#10;if(envselector.equals(&quot;QLAB06&quot;)){&#13;&#10;    bw_host=&quot;qatenbw162.unix.gsm1900.org:9971&quot;;&#13;&#10;    ce_host=&quot;qlab06-duti.test.px-npe02b.cf.t-mobile.com&quot;;&#13;&#10;    OMSURL_VAL=&quot;https://u2oms-q6.px-npe1102.pks.t-mobile.com/yantrawebservices/services/YIFWebService&quot;;&#13;&#10;}&#13;&#10;&#13;&#10;if(envselector.equals(&quot;QLAB07&quot;)){&#13;&#10;    bw_host=&quot;qatenbw172.unix.gsm1900.org:9971&quot;;&#13;&#10;    ce_host=&quot;vendorservices-qlab07-duti-blue.test.px-npe02b.cf.t-mobile.com&quot;;&#13;&#10;    ce_host1=&quot;vendorservices-qlab07-duti-green.test.px-npe02b.cf.t-mobile.com&quot;;&#13;&#10;&#9;OMSURL_VAL=&quot;https://u2oms-q7.px-npe1102.pks.t-mobile.com/yantrawebservices/services/YIFWebService&quot;;&#13;&#10;}&#13;&#10;&#13;&#10;&#13;&#10;testExec.setStateValue(&quot;BW_Host&quot;, bw_host);&#13;&#10;testExec.setStateValue(&quot;CE_Host&quot;, ce_host);&#13;&#10;testExec.setStateValue(&quot;OMSURL&quot;, OMSURL_VAL);</script>
    </Node>


    <Node name="abort" log=""
          type="com.itko.lisa.test.AbortStep" 
          version="1" 
          uid="7E4D85C6D3AE11E8B39F2AC220524153" 
          think="0h" 
          useFilters="true" 
          quiet="true" 
          next="" > 

    </Node>


    <Node name="fail" log=""
          type="com.itko.lisa.test.Abend" 
          version="1" 
          uid="7E4D85C8D3AE11E8B39F2AC220524153" 
          think="0h" 
          useFilters="true" 
          quiet="true" 
          next="abort" > 

    </Node>


    <Node name="end" log=""
          type="com.itko.lisa.test.NormalEnd" 
          version="1" 
          uid="7E4D85CAD3AE11E8B39F2AC220524153" 
          think="0h" 
          useFilters="true" 
          quiet="true" 
          next="fail" > 

    </Node>


</TestCase>
