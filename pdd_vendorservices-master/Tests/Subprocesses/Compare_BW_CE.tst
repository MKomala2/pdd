<?xml version="1.0" ?>

<TestCase name="Compare_BW_CE" version="5">

<meta>
   <create version="10.3.0" buildNumber="10.3.0.297" author="asanka4" date="04/13/2020" host="LP-BW4VPF2" />
   <lastEdited version="10.3.0" buildNumber="10.3.0.297" author="asanka4" date="06/05/2020" host="LP-BW4VPF2" />
</meta>

<id>2E0507A7A69911EA976618DBF2466751</id>
<Documentation>Put documentation of the Test Case here.</Documentation>
<IsInProject>true</IsInProject>
<sig>ZWQ9NSZ0Y3Y9NSZsaXNhdj0xMC4zLjAgKDEwLjMuMC4yOTcpJm5vZGVzPS0xNTk3NTE4Mjkz</sig>
<subprocess>true</subprocess>

<initState>
    <Parameter>
    <key>CE_Response</key>
    <value>{{CE_Response}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>BW_Response</key>
    <value>{{BW_Response}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>TC_Id</key>
    <value>{{TC_Id}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>TestCase_Description</key>
    <value>{{TestCase_Description}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>Operation</key>
    <value>{{Operation}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>CE_Code</key>
    <value>{{CE_Code}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>BW_Code</key>
    <value>{{BW_Code}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>IgnoreAttributes</key>
    <value>{{IgnoreAttributes}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
</initState>

<resultState>
    <Parameter>
    <key>ExecutionMode</key>
    <value></value>
    <name>Set in project.config configuration</name>
    </Parameter>
    <Parameter>
    <key>FailureDescription</key>
    <value></value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>LOCDIR</key>
    <value></value>
    <name>Set in project.config configuration</name>
    </Parameter>
    <Parameter>
    <key>Test_Execution_Status</key>
    <value></value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>lisa.Format BW File.rsp</key>
    <value></value>
    <name>Set 1st in Format BW File</name>
    </Parameter>
    <Parameter>
    <key>lisa.Format BW File.rsp.time</key>
    <value></value>
    <name>Set 1st in Format BW File</name>
    </Parameter>
    <Parameter>
    <key>lisa.Format CE File.rsp</key>
    <value></value>
    <name>Set 1st in Format CE File</name>
    </Parameter>
    <Parameter>
    <key>lisa.Format CE File.rsp.time</key>
    <value></value>
    <name>Set 1st in Format CE File</name>
    </Parameter>
    <Parameter>
    <key>lisa.Format.rsp</key>
    <value></value>
    <name>Set 1st in Format</name>
    </Parameter>
    <Parameter>
    <key>lisa.Format.rsp.time</key>
    <value></value>
    <name>Set 1st in Format</name>
    </Parameter>
</resultState>

<deletedProps>
    <key>WSSERVER1</key>
    <key>STEPNAME</key>
    <key>EnvironmentTestedE2E</key>
    <key>ODCAppTestJenkinsJobPath</key>
    <key>lisa.Set Ignore Attributes.rsp.time</key>
    <key>lisa.Compare CE and BW.rsp.time</key>
    <key>ENDPOINT1</key>
    <key>ENDPOINT2</key>
    <key>LocalPath</key>
    <key>lisa.Set Ignore Attributes.rsp</key>
    <key>EnvironmentTested</key>
    <key>lisa.continue (quiet).rsp</key>
    <key>WSPORT</key>
    <key>lisa.Compare CE and BW.rsp</key>
    <key>Type</key>
    <key>WSPORT1</key>
    <key>ENDPOINT</key>
    <key>lisa.vse.execution.mode</key>
    <key>lisa.Execute script (JSR-223).rsp.time</key>
    <key>lisa.continue (quiet).rsp.time</key>
    <key>HandlerName</key>
    <key>ODCSmokeTestJenkinsJobPath</key>
    <key>lisa.Execute script (JSR-223).rsp</key>
    <key>WSSERVER</key>
</deletedProps>

    <Node name="Set Ignore Attributes" log=""
          type="com.itko.lisa.test.UserScriptNode" 
          version="1" 
          uid="2E052EB8A69911EA976618DBF2466751" 
          think="500-1S" 
          useFilters="true" 
          quiet="false" 
          next="Format BW File" > 


      <!-- Assertions -->
<CheckResult assertTrue="true" name="Any Exception Then Fail" type="com.itko.lisa.dynexec.CheckInvocationEx">
<log>Assertion name: Any Exception Then Fail checks for: true is of type: Assert on Invocation Exception.</log>
<then>fail</then>
<valueToAssertKey></valueToAssertKey>
        <param>.*</param>
</CheckResult>

<onerror>continue</onerror>
<language>BeanShell</language>
<copyProps>TestExecProps</copyProps>
<script>&#13;&#10;String ignoreAttributes = testExec.getStateValue(&quot;IgnoreAttributes&quot;);&#13;&#10;String [] ignoreAttributesArray = ignoreAttributes.split(&quot;,&quot;);&#13;&#10;&#13;&#10;testExec.log(&quot;ignoreAttributeArray length&quot;, ignoreAttributesArray.length.toString());&#13;&#10;testExec.setStateValue(&quot;IgnoreAttributes&quot;, ignoreAttributesArray);&#13;&#10;return true;</script>
    </Node>


    <Node name="Format BW File" log=""
          type="com.itko.lisa.test.UserScriptNode" 
          version="1" 
          uid="6DC6BEE1A69911EA976618DBF2466751" 
          think="500-1S" 
          useFilters="true" 
          quiet="false" 
          next="Format CE File" > 


      <!-- Assertions -->
<CheckResult assertTrue="true" name="Any Exception Then Fail" type="com.itko.lisa.dynexec.CheckInvocationEx">
<log>Assertion name: Any Exception Then Fail checks for: true is of type: Assert on Invocation Exception.</log>
<then>fail</then>
<valueToAssertKey></valueToAssertKey>
        <param>.*</param>
</CheckResult>

<onerror>continue</onerror>
<language>BeanShell</language>
<copyProps>TestExecProps</copyProps>
<script>import java.io.BufferedReader;&#13;&#10;import java.io.BufferedWriter;&#13;&#10;import java.io.File;&#13;&#10;import java.io.FileReader;&#13;&#10;import java.io.FileWriter;&#13;&#10;import java.io.IOException;&#13;&#10;import java.io.StringWriter;&#13;&#10;import java.io.Writer;&#13;&#10;import javax.xml.parsers.DocumentBuilder;&#13;&#10;import javax.xml.parsers.DocumentBuilderFactory;&#13;&#10;import javax.xml.parsers.ParserConfigurationException;&#13;&#10;import javax.xml.transform.OutputKeys;&#13;&#10;import javax.xml.transform.Transformer;&#13;&#10;import javax.xml.transform.TransformerConfigurationException;&#13;&#10;import javax.xml.transform.TransformerException;&#13;&#10;import javax.xml.transform.TransformerFactory;&#13;&#10;import javax.xml.transform.TransformerFactoryConfigurationError;&#13;&#10;import javax.xml.transform.dom.DOMSource;&#13;&#10;import javax.xml.transform.stream.StreamResult;&#13;&#10;import org.w3c.dom.Document;&#13;&#10;import org.xml.sax.SAXException;&#13;&#10;&#13;&#10;&#13;&#10;File f1 = new File(&quot;{{LOCDIR}}/Data/OutputFiles/{{TC_Id}}_{{TestCase_Description}}_BW.txt&quot;);&#13;&#10;File f1_modified = new File(&quot;{{LOCDIR}}/Data/OutputFiles/{{TC_Id}}_{{TestCase_Description}}_BW_modified.txt&quot;);&#13;&#10;&#13;&#10;//Parsing the XML&#13;&#10;DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();&#13;&#10;DocumentBuilder db = dbf.newDocumentBuilder();&#13;&#10;Document doc = db.parse(f1);&#13;&#10;&#13;&#10;//Transforming to Pretty XML&#13;&#10;Transformer tf = TransformerFactory.newInstance().newTransformer();&#13;&#10;tf.setOutputProperty(OutputKeys.ENCODING, &quot;UTF-8&quot;);&#13;&#10;tf.setOutputProperty(OutputKeys.INDENT, &quot;yes&quot;);&#13;&#10;Writer out = new StringWriter();&#13;&#10;tf.transform(new DOMSource(doc), new StreamResult(out));&#13;&#10;String str = out.toString();&#13;&#10;          &#13;&#10;//Writing to same file&#13;&#10;BufferedWriter bw = new BufferedWriter(new FileWriter(f1));&#13;&#10;bw.write(str);&#13;&#10;bw.close();&#13;&#10;&#13;&#10;&#13;&#10;//Read the modified file and write to a new file with changes&#13;&#10;BufferedReader br = new BufferedReader(new FileReader(f1));&#13;&#10;BufferedWriter bw1 = new BufferedWriter(new FileWriter(f1_modified));&#13;&#10;String st;&#13;&#10;while ((st = br.readLine()) != null) {&#13;&#10;&#9;if(st.contains(&quot;/&gt;&quot;) &amp;&amp; !st.contains(&quot;\&quot;&quot;)){&#13;&#10;&#9;&#9;continue;&#9;&#13;&#10;&#9;}&#13;&#10;    else if(st.contains(&quot;:Header&quot;)){&#13;&#10;        continue;&#13;&#10;    }&#13;&#10;&#9;else{&#13;&#10;        bw1.write(st);&#13;&#10;        bw1.newLine();&#13;&#10;&#9;}&#13;&#10;}&#13;&#10;&#13;&#10;bw1.close();&#13;&#10;br.close();&#13;&#10;</script>
    </Node>


    <Node name="Format CE File" log=""
          type="com.itko.lisa.test.UserScriptNode" 
          version="1" 
          uid="5DB03AD0A6A011EA976618DBF2466751" 
          think="500-1S" 
          useFilters="true" 
          quiet="false" 
          next="Compare CE and BW" > 


      <!-- Assertions -->
<CheckResult assertTrue="true" name="Any Exception Then Fail" type="com.itko.lisa.dynexec.CheckInvocationEx">
<log>Assertion name: Any Exception Then Fail checks for: true is of type: Assert on Invocation Exception.</log>
<then>fail</then>
<valueToAssertKey></valueToAssertKey>
        <param>.*</param>
</CheckResult>

<onerror>continue</onerror>
<language>BeanShell</language>
<copyProps>TestExecProps</copyProps>
<script>import java.io.BufferedReader;&#13;&#10;import java.io.BufferedWriter;&#13;&#10;import java.io.File;&#13;&#10;import java.io.FileReader;&#13;&#10;import java.io.FileWriter;&#13;&#10;import java.io.IOException;&#13;&#10;import java.io.StringWriter;&#13;&#10;import java.io.Writer;&#13;&#10;import javax.xml.parsers.DocumentBuilder;&#13;&#10;import javax.xml.parsers.DocumentBuilderFactory;&#13;&#10;import javax.xml.parsers.ParserConfigurationException;&#13;&#10;import javax.xml.transform.OutputKeys;&#13;&#10;import javax.xml.transform.Transformer;&#13;&#10;import javax.xml.transform.TransformerConfigurationException;&#13;&#10;import javax.xml.transform.TransformerException;&#13;&#10;import javax.xml.transform.TransformerFactory;&#13;&#10;import javax.xml.transform.TransformerFactoryConfigurationError;&#13;&#10;import javax.xml.transform.dom.DOMSource;&#13;&#10;import javax.xml.transform.stream.StreamResult;&#13;&#10;import org.w3c.dom.Document;&#13;&#10;import org.xml.sax.SAXException;&#13;&#10;&#13;&#10;&#13;&#10;File f1 = new File(&quot;{{LOCDIR}}/Data/OutputFiles/{{TC_Id}}_{{TestCase_Description}}_CE.txt&quot;);&#13;&#10;File f1_modified = new File(&quot;{{LOCDIR}}/Data/OutputFiles/{{TC_Id}}_{{TestCase_Description}}_CE_modified.txt&quot;);&#13;&#10;&#13;&#10;//Parsing the XML&#13;&#10;DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();&#13;&#10;DocumentBuilder db = dbf.newDocumentBuilder();&#13;&#10;Document doc = db.parse(f1);&#13;&#10;&#13;&#10;//Transforming to Pretty XML&#13;&#10;Transformer tf = TransformerFactory.newInstance().newTransformer();&#13;&#10;tf.setOutputProperty(OutputKeys.ENCODING, &quot;UTF-8&quot;);&#13;&#10;tf.setOutputProperty(OutputKeys.INDENT, &quot;yes&quot;);&#13;&#10;Writer out = new StringWriter();&#13;&#10;tf.transform(new DOMSource(doc), new StreamResult(out));&#13;&#10;String str = out.toString();&#13;&#10;          &#13;&#10;//Writing to same file&#13;&#10;BufferedWriter bw = new BufferedWriter(new FileWriter(f1));&#13;&#10;bw.write(str);&#13;&#10;bw.close();&#13;&#10;&#13;&#10;&#13;&#10;//Read the modified file and write to a new file with changes&#13;&#10;BufferedReader br = new BufferedReader(new FileReader(f1));&#13;&#10;BufferedWriter bw1 = new BufferedWriter(new FileWriter(f1_modified));&#13;&#10;String st;&#13;&#10;while ((st = br.readLine()) != null) {&#13;&#10;&#9;if(st.contains(&quot;/&gt;&quot;) &amp;&amp; !st.contains(&quot;\&quot;&quot;)){&#13;&#10;&#9;&#9;continue;&#9;&#13;&#10;&#9;}&#13;&#10;    else if(st.contains(&quot;:Header&quot;)){&#13;&#10;        continue;&#13;&#10;    }&#13;&#10;&#9;else{&#13;&#10;        bw1.write(st);&#13;&#10;        bw1.newLine();&#13;&#10;&#9;}&#13;&#10;}&#13;&#10;&#13;&#10;bw1.close();&#13;&#10;br.close();&#13;&#10;</script>
    </Node>


    <Node name="Compare CE and BW" log=""
          type="com.itko.lisa.test.UserScriptNode" 
          version="1" 
          uid="2E052EB9A69911EA976618DBF2466751" 
          think="500-1S" 
          useFilters="true" 
          quiet="false" 
          next="end" > 


      <!-- Assertions -->
<CheckResult assertTrue="true" name="Any Exception Then Fail" type="com.itko.lisa.dynexec.CheckInvocationEx">
<log>Assertion name: Any Exception Then Fail checks for: true is of type: Assert on Invocation Exception.</log>
<then>fail</then>
<valueToAssertKey></valueToAssertKey>
        <param>.*</param>
</CheckResult>

<onerror>continue</onerror>
<language>BeanShell</language>
<copyProps>TestExecProps</copyProps>
<script>import java.io.BufferedReader;&#13;&#10;import java.io.FileInputStream;&#13;&#10;import java.io.FileNotFoundException;&#13;&#10;import java.io.IOException;&#13;&#10;import java.io.InputStreamReader;&#13;&#10;import java.util.List;&#13;&#10;import org.custommonkey.xmlunit.DetailedDiff;&#13;&#10;import org.custommonkey.xmlunit.Diff;&#13;&#10;import org.custommonkey.xmlunit.Difference;&#13;&#10;import org.custommonkey.xmlunit.XMLUnit;&#13;&#10;import org.xml.sax.SAXException;&#13;&#10;import javax.xml.parsers.DocumentBuilderFactory;&#13;&#10;import javax.xml.parsers.DocumentBuilder;&#13;&#10;import org.w3c.dom.Document;&#13;&#10;import java.io.StringReader;&#13;&#10;import org.xml.sax.InputSource;&#13;&#10;&#13;&#10;String errorMsg=&quot;&quot;;&#13;&#10;int count = 0;&#13;&#10;&#13;&#10;String ce_responsecode = testExec.getStateValue(&quot;CE_Code&quot;);&#13;&#10;String bw_responsecode = testExec.getStateValue(&quot;BW_Code&quot;);&#13;&#10;&#13;&#10;//Getting&#13;&#10;String [] ignoreAttributesArray = testExec.getStateValue(&quot;IgnoreAttributes&quot;);&#13;&#10;&#13;&#10;if(!ce_responsecode.equals(bw_responsecode)){&#13;&#10; errorMsg +=&quot;CE Response code:&quot;+ce_responsecode+&quot; and BW Response code:&quot;+bw_responsecode+&quot;.\r\n&quot;;&#13;&#10; count++;&#13;&#10;}&#13;&#10;else{&#13;&#10;String cefilePath = &quot;{{LOCDIR}}/Data/OutputFiles/{{TC_Id}}_{{TestCase_Description}}_CE_modified.txt&quot;;&#13;&#10;String bwfilePath = &quot;{{LOCDIR}}/Data/OutputFiles/{{TC_Id}}_{{TestCase_Description}}_BW_modified.txt&quot;;&#13;&#10;&#13;&#10;&#13;&#10;FileInputStream fis_ce = new FileInputStream(cefilePath);&#13;&#10;FileInputStream fis_bw = new FileInputStream(bwfilePath);&#13;&#10;&#13;&#10;BufferedReader ceReader = new BufferedReader(new InputStreamReader(fis_ce));&#13;&#10;BufferedReader bwReader = new BufferedReader(new InputStreamReader(fis_bw));&#13;&#10;&#13;&#10;XMLUnit.setIgnoreWhitespace(true);&#13;&#10;&#13;&#10;Diff xmlDiff = new Diff(bwReader, ceReader);&#13;&#10;//Diff xmlDiff = new Diff(ceDocument, bwDocument);&#13;&#10;DetailedDiff detailXmlDiff = new DetailedDiff(xmlDiff);&#13;&#10;&#13;&#10;&#13;&#10;List differences = detailXmlDiff.getAllDifferences();&#13;&#10;&#13;&#10;//Add valid attributes to new list if present. &#13;&#10;List&lt;Difference&gt; validAttributesList = new ArrayList&lt;Difference&gt;();&#13;&#10;for(int i=0;i&lt;ignoreAttributesArray.length;i++){&#13;&#10;  for(Difference difference: differences){&#13;&#10;       String str = difference.toString();&#13;&#10;       if(str.contains(ignoreAttributesArray[i]) &amp;&amp; (str.contains(&quot;text value&quot;)|| str.contains(&quot;attribute value&quot;))){&#13;&#10;         validAttributesList.add(difference);&#13;&#10;       }&#13;&#10;  }&#13;&#10;}&#13;&#10;&#13;&#10;// Ignore namespace errors&#13;&#10;for(Difference difference: differences){&#13;&#10;       String str = difference.toString();&#13;&#10;       if(str.contains(&quot;namespace prefix&quot;)){&#13;&#10;         validAttributesList.add(difference);&#13;&#10;       }&#13;&#10;       if(str.contains(&quot;.0&quot;) || str.contains(&quot;.00&quot;)){&#13;&#10;         validAttributesList.add(difference);&#13;&#10;       }&#13;&#10;  }&#13;&#10;&#13;&#10;//Remove Valid attributes from difference list&#13;&#10;for(int i=0;i&lt;validAttributesList.size();i++){&#13;&#10;   differences.remove(validAttributesList.get(i));&#13;&#10;}&#13;&#10;&#13;&#10;//Convert the remaining difference to error message&#13;&#10;for(Difference difference: differences){&#13;&#10;String str = difference.toString();&#13;&#10;int index1 = str.indexOf(&quot;comparing&quot;);&#13;&#10;int index2 = str.lastIndexOf(&quot; at &quot;);&#13;&#10;String err = str.substring(0, index1 - 3)+&quot; at /&quot;+str.substring(index2+3)+&quot;.\r\n&quot;;&#13;&#10;err= err.replaceAll(&quot;&apos;&quot;, &quot;\&quot;&quot;);&#9;&#13;&#10;errorMsg+= err;&#13;&#10;count++;&#13;&#10;}&#13;&#10;&#13;&#10;}&#13;&#10;if(count&gt;0){&#13;&#10;    testExec.setStateValue(&quot;Test_Execution_Status&quot;, &quot;Failed&quot;);&#13;&#10;    testExec.setStateValue(&quot;FailureDescription&quot;, errorMsg);&#13;&#10;}&#13;&#10;else{&#13;&#10;    testExec.setStateValue(&quot;Test_Execution_Status&quot;, &quot;Passed&quot;);&#13;&#10;    testExec.setStateValue(&quot;FailureDescription&quot;, &quot;&quot;);&#13;&#10;}&#13;&#10;return true;</script>
    </Node>


    <Node name="end" log=""
          type="com.itko.lisa.test.NormalEnd" 
          version="1" 
          uid="2E052EBAA69911EA976618DBF2466751" 
          think="0h" 
          useFilters="true" 
          quiet="true" 
          next="fail" > 

    </Node>


    <Node name="fail" log=""
          type="com.itko.lisa.test.Abend" 
          version="1" 
          uid="2E052EBBA69911EA976618DBF2466751" 
          think="0h" 
          useFilters="true" 
          quiet="true" 
          next="abort" > 

    </Node>


    <Node name="abort" log=""
          type="com.itko.lisa.test.AbortStep" 
          version="1" 
          uid="2E052EBCA69911EA976618DBF2466751" 
          think="0h" 
          useFilters="true" 
          quiet="true" 
          next="" > 

    </Node>


</TestCase>
