<?xml version="1.0" ?>

<TestCase name="SendEmail" version="5">

<meta>
   <create version="10.3.0" buildNumber="10.3.0.297" author="skodaka1" date="08/06/2018" host="LP-3G2WPF2" />
   <lastEdited version="10.3.0" buildNumber="10.3.0.297" author="MKomala2" date="07/28/2020" host="LP-5CD7355fj7" />
</meta>

<id>4A24E694D0BD11EA8A7D8AB111CBFB47</id>
<Documentation>Put documentation of the Test Case here.</Documentation>
<IsInProject>true</IsInProject>
<sig>ZWQ9NSZ0Y3Y9NSZsaXNhdj0xMC4zLjAgKDEwLjMuMC4yOTcpJm5vZGVzPS0xMzY4NTM5MDQ1</sig>
<subprocess>false</subprocess>

<initState>
</initState>

<resultState>
</resultState>

<deletedProps>
</deletedProps>

    <Node name="Read HTML REPORT" log=""
          type="com.itko.lisa.test.FileNode" 
          version="1" 
          uid="4A250DA5D0BD11EA8A7D8AB111CBFB47" 
          think="500-1S" 
          useFilters="true" 
          quiet="true" 
          next="Send Email Step" > 

<Loc>{{LOCDIR}}/Data/OutputFiles/{{EAR}}_ExecutionResults_{{EnvironmentTested}}.html</Loc>
<charset>DEFAULT</charset>
<PropKey>ReportContent</PropKey>
<onFail>continue</onFail>
    </Node>


    <Node name="Send Email Step" log=""
          type="com.itko.lisa.glass.SendEmailStep" 
          version="1" 
          uid="4A250DA6D0BD11EA8A7D8AB111CBFB47" 
          think="500-1S" 
          useFilters="true" 
          quiet="false" 
          next="end" > 

<EmailConnection>
<AssetInfo type="org.apache.commons.mail.HtmlEmail" hashcode="-2115258468"><Url>config://com.itko.lisa.glass.email.asset-872B8B2D80C211EA987818DBF2466751</Url>
</AssetInfo>
<Ref>
</Ref>
</EmailConnection>
<Attachments>{{LOCDIR}}/Data/OutputFiles/{{EAR}}_ExecutionResults_{{EnvironmentTested}}.html</Attachments>
<Bcc></Bcc>
<Cc></Cc>
<From>MAHABOOB.KOMALA2@T-Mobile.com</From>
<Message>&lt;html&gt;&lt;body&gt;&lt;h3&gt;Prepaid CE Migration - {{EAR}} Execution Report.&lt;/h3&gt;&#10;&lt;p&gt;Hi Team,&lt;br&gt;Please find attached report for the {{EAR}} execution.&#10;&lt;br&gt; --Environment: {{EnvironmentTested}}.&lt;br&gt;-Regards&lt;br&gt;Prepaid CE Migration Team&#10;{{ReportContent}}&#10;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</Message>
<Subject>Prepaid CE Migration - {{EAR}} Execution Report {{EnvironmentTested}} from LISA </Subject>
<To>MAHABOOB.KOMALA2@T-Mobile.com</To>
<onError>continue</onError>
    </Node>


    <Node name="end" log=""
          type="com.itko.lisa.test.NormalEnd" 
          version="1" 
          uid="4A250DA7D0BD11EA8A7D8AB111CBFB47" 
          think="0h" 
          useFilters="true" 
          quiet="true" 
          next="fail" > 

    </Node>


    <Node name="fail" log=""
          type="com.itko.lisa.test.Abend" 
          version="1" 
          uid="4A250DA8D0BD11EA8A7D8AB111CBFB47" 
          think="0h" 
          useFilters="true" 
          quiet="true" 
          next="abort" > 

    </Node>


    <Node name="abort" log=""
          type="com.itko.lisa.test.AbortStep" 
          version="1" 
          uid="4A250DA9D0BD11EA8A7D8AB111CBFB47" 
          think="0h" 
          useFilters="true" 
          quiet="true" 
          next="" > 

    </Node>


</TestCase>
